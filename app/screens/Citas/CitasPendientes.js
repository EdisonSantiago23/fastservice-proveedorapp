
import React, { Component } from 'react';
import { StyleSheet, ScrollView, TouchableOpacity, StatusBar, Image, Animated, View } from 'react-native';
import mecanicoApi from '../../api/mecanico';
import LoadingProgress from '../../common/LoadingProgress';
import { Block, Text, theme } from "galio-framework";
import AsyncStorage from '@react-native-community/async-storage';
import Constants from '../../common/Constants';
import { Header } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import {
    SCLAlert,
    SCLAlertButton
} from 'react-native-scl-alert'
var Moment = require('moment'); // require
import {showDangerMsg, showSuccesMsg} from '../../common/ShowMessages';

export default class CitasPendientes extends Component {
    constructor(props) {
        super(props);
        this.mecanicoApi = new mecanicoApi();

        this.state = {
            initSplash: true,
            loginSpl: true,
            loading: true,
            reservas: [],
            recerva: '',
            show: false,
            showconfig: false,
            showconfigfin: false,

        };
        this.RotateValueHolder = new Animated.Value(0);
        this.getReservas();
    }

    getReservas = async () => {
        this.mecanicoApi.getcitasproveedor(await AsyncStorage.getItem(Constants.STORE_ID_USER)).then(res => {
            if (res.status==='error'||res === undefined) {
                this.setState({ show: true, loading: false, })

            }else{
                this.setState({
                    reservas: res.reservas,loading: false,
                });
            }
           

        });
    }
    finalizarcita = async () => {
        try {
            this.setState({ loading: true });
            const cita = {
                idrecerva: this.state.recerva.idrecerva,
                estado: 1,
                idlocal: await AsyncStorage.getItem(Constants.STORE_ID_USER),
                tocken: this.state.recerva.tocken,
                nombre: this.state.recerva.nombrecliente,

            }
            this.mecanicoApi.respuestareserva(cita).then(res => {
                if (res) {
                    this.getReservas();
                } else {
                    if (res.status && res.status == 'error') {
                        showDangerMsg(res.message,'Error');
                        this.setState({ loading: false });
                    }
                }
            });

        } catch (err) {
            showDangerMsg(err.message,'Error');
        }
    }
    estado = async (cliente, estado) => {
        try {
            this.setState({ loading: true });
            cliente.estado = estado;
            if (estado == 3) {
                cliente.content = "EL cliente " + cliente.nombrecliente + " aceptó al mecánico " + cliente.nombremecanico;

            } else {
                cliente.content = "EL cliente " + cliente.nombrecliente + " Finalizó el servicio ";

            }
            this.mecanicoApi.respuestaCliente(cliente).then(res => {
                if (res) {


                    if (estado == 5) {
                        this.setState({ loading: false, showconfigfin: true });

                    } else {
                        this.setState({ loading: false, showconfig: true });
                        this.getReservas();

                    }

                } else {
                    if (res.status && res.status == 'error') {
                        showDangerMsg(res.message,'Error');
                        this.setState({ loading: false });
                    }
                }
            });

        } catch (err) {
            showDangerMsg(err.message,'Error');
        }
    }

    handleOpen = () => {
        this.setState({ show: true })
    }

    handleCloseinfofin = () => {
        this.setState({ showconfigfin: false })
        this.getReservas();

    }
    handleCloseinfofinSI = () => {
        this.setState({ showconfigfin: false })
        this.finalizarcita();
    }
    handleCloseinfo = () => {
        this.setState({ showconfig: false })

    }
    handleClose = () => {
        this.setState({ show: false })
        this.props.navigation.navigate('MenuCitas');

    }
    render() {
        if (this.state.loading) {
            return <LoadingProgress />;
        }
        if (!this.state.loading) {
            return (
                <View>
                    <Header
                        leftComponent={<TouchableOpacity onPress={() => {
                            this.props.navigation.navigate('MenuCitas');
                        }}>
                            <Icon name="arrow-left" type="Ionicons" size={30} color="#fff" />
                        </TouchableOpacity>}
                        centerComponent={{ text: 'MIS CITAS', style: { color: '#fff' } }}

                        rightComponent={<TouchableOpacity onPress={() => {
                            this.props.navigation.navigate('Menu');
                        }}>
                            <Icon name="home" type="Ionicons" size={30} color="#fff" />
                        </TouchableOpacity>}
      containerStyle={{
        justifyContent: 'space-around',
        backgroundColor: '#FF9100'
    }} />
                        <StatusBar backgroundColor='#FF9100' barStyle="light-content" />
                    <View style={{ flexDirection: 'row', alignItems: 'center', marginLeft: '30%' }}>
                        <View>
                            <Image
                                source={require('../../assets/png/lg.png')}
                                style={styles.imagen} />
                            <View>
                                <Text style={{ fontSize: 18, color: '#617792', fontWeight: 'bold' }}> MECÁNICA EXPRESS</Text>
                            </View>
                        </View>

                    </View>
                    <SCLAlert
                            theme="info"
                            show={this.state.show}
                            title="Citas"
                            onRequestClose={this.handleClose}
                            cancellable={true}

                            subtitle="Actualmente no tienes citas pendientes"
                        >
                            <SCLAlertButton theme="info" onPress={this.handleClose}>Atras</SCLAlertButton>
                        </SCLAlert>
                    <SCLAlert
                        theme="warning"
                        show={this.state.showconfigfin}
                        title="Auxlio Mecánico"
                        onRequestClose={this.handleCloseinfofin}
                        cancellable={true}

                        subtitle="Deseas agendar la cita la cita?"
                    >
                        <SCLAlertButton theme="danger" onPress={this.handleCloseinfofinSI}>Aceptar</SCLAlertButton>
                        <SCLAlertButton theme="info" onPress={this.handleCloseinfofin}>Cancelar</SCLAlertButton>

                    </SCLAlert>

                    <ScrollView style={{ height: '70%' }}>

                        <View style={styles.container}>
                            <View>
                                {this.state.reservas.map(novedad => (
                                    <View style={styles.contImage}>

                                        <Block flex style={styles.profileCard}>

                                            <Block middle style={styles.avatarContainer}>
                                                <Text style={{ fontSize: 20, color: '#617792', fontWeight: 'bold' }}>{novedad.nombremarca}</Text>

                                                {novedad.estado == 1 ?
                                                    (
                                                        <Text style={{ fontSize: 15, color: '#1F4997', fontWeight: 'bold', textAlign: 'center' }}>
                                                            AGENDADO </Text>
                                                    )
                                                    :
                                                    (
                                                        novedad.estado == 0 ?
                                                            (
                                                                <Text style={{ fontSize: 15, color: '#FF333C', fontWeight: 'bold', textAlign: 'center' }}>
                                                                    PENDIENTE </Text>

                                                            ) : (null)
                                                    )}
                                                <TouchableOpacity
                                                    style={styles.btnVerEnSitio}
                                                    onPress={_ => this.setState({ showconfigfin: true, recerva: novedad })
                                                    }>
                                                    <Text style={{ fontSize: 14, textAlign: 'center', color: '#1F4997' }}>
                                                        Aceptar Cita</Text>
                                                </TouchableOpacity>
                                                <Image
                                                    source={{ uri: novedad.url }}
                                                    style={styles.avatar}
                                                />
                                                <Text style={{ fontSize: 15, color: '#617792', fontWeight: 'bold' }}>{novedad.nombrelocal}</Text>
                                                <Block middle
                                                    row
                                                    space="evenly"
                                                    style={{ marginTop: 10, paddingBottom: 24 }}>

                                                </Block>
                                                <Block middle>
                                                    <Text
                                                        bold
                                                        color="#525F7F"
                                                        size={16}
                                                        style={{ marginBottom: 4 }}
                                                    >
                                                        {novedad.nombrecliente}

                                                    </Text>

                                                    <Text size={16}>Cliente</Text>
                                                </Block>
                                            </Block>

                                            <Block style={styles.info}>
                                                <Block middle
                                                    row
                                                    space="evenly"
                                                    style={{ marginTop: 10, paddingBottom: 24 }}>

                                                </Block>


                                                <Block row space="between">

                                                    <Block middle>
                                                        <Text bold
                                                            size={16}
                                                            color="#525F7F"
                                                            style={{ marginBottom: 4 }}
                                                        >
                                                            {Moment(novedad.fecha).format('MM-DD-YYYY')}
                                                        </Text>
                                                        <Text size={16}>Fecha</Text>
                                                    </Block>
                                                    <Block middle>
                                                        <Text
                                                            bold
                                                            color="#525F7F"
                                                            size={16}
                                                            style={{ marginBottom: 4 }}
                                                        >
                                                            {Moment(novedad.hora).format('hh:mm:ss')}

                                                        </Text>

                                                        <Text size={16}>Hora</Text>
                                                    </Block>

                                                    <Block middle>
                                                        <Text
                                                            bold
                                                            color="#525F7F"
                                                            size={16}
                                                            style={{ marginBottom: 4 }}
                                                        >
                                                            {novedad.celularcliente}

                                                        </Text>

                                                        <Text size={16}>Teléfono</Text>
                                                    </Block>

                                                </Block>
                                            </Block>

                                        </Block>

                                    </View>

                                ))}
                            </View>
                        </View>
                    </ScrollView>
                </View>
            );

        }
    }

}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
    },
    profile: {
        flex: 1
    },
    imagen: {
        width: 75,
        height: 75,
        marginLeft: '25%'

    },
    btnVerEnSitioL: {
        flexDirection: 'row',
        alignItems: 'center',
        alignContent: 'center',
        justifyContent: 'center',
        fontSize: 16,
        color: '#000',
        backgroundColor: '#32BEDC',
        paddingVertical: 10,
        paddingHorizontal: 15,
        textAlign: 'center',
        width: '50%',
        height: 50,
        marginVertical: 5,
    },
    contImage: {
        flex: 1,
        marginTop: 3,
        justifyContent: 'center',
        borderWidth: 2,
        width: 400,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
    },
    map: {
        ...StyleSheet.absoluteFillObject
    },
    headerContainer: {
        flexDirection: 'row',
        padding: 5,
        alignItems: 'flex-start',
    },
    btnVerEnSitio: {
        backgroundColor: '#F5F6F7',
        borderRadius: 20,
        width: '35%',
        padding: '3%',
        color: 'blue',
        borderWidth: 1,
        marginTop: '5%',
        borderColor: '#4982e6',
    },
    carousel: {
        position: 'absolute',
        bottom: 0,
        marginBottom: 48
    },
    cardContainer: {
        alignSelf: 'center',
        backgroundColor: '#617792',
        height: 600,
        width: 350,
        padding: 50,
        borderRadius: 24
    },
    cardImage: {
        height: 120,
        width: 300,
        bottom: 0,
        position: 'absolute',
        borderBottomLeftRadius: 24,
        borderBottomRightRadius: 24
    },
    button: {
        alignSelf: 'center',

        fontSize: 16,
        color: '#fff',
        backgroundColor: '#617792',
        paddingVertical: 10,
        paddingHorizontal: 15,
        textAlign: "center",
        height: 50,
        width: 350,
        padding: 50,
        borderRadius: 24
    },
    cardTitle: {
        color: 'white',
        fontSize: 22,
        alignSelf: 'center'
    },
    profile: {
        flex: 1
    },
    profileContainer: {
        width: '100%',
        height: '50%',
        color: '#617792',
        padding: 0,
        zIndex: 1
    },
    profileBackground: {
        width: '100%',
        height: 350 / 2
    },
    profileCard: {
        // position: "relative",
        padding: 1,
        marginHorizontal: 1,
        marginTop: 65,
        borderTopLeftRadius: 6,
        borderTopRightRadius: 6,
        backgroundColor: 1,
        shadowColor: "#617792",
        shadowOffset: { 200: 0, 200: 0 },
        shadowRadius: 8,
        shadowOpacity: 0.2,
        zIndex: 2
    },
    info: {
        paddingHorizontal: 30
    },
    avatarContainer: {
        position: "relative",
        marginTop: -60
    },
    avatar: {
        width: 124,
        height: 124,
        borderWidth: 0,
        marginTop: '5%'
    },
    nameInfo: {
        marginTop: -7
    },
    divider: {
        width: "90%",
        borderWidth: 1,
        borderColor: "#E9ECEF"
    },
    thumb: {
        borderRadius: 4,
        marginVertical: 4,
        alignSelf: "center",
        width: 300,
        height: 300
    }
});
