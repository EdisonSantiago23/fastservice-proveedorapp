import React, { Component } from 'react';
import { Text, StyleSheet, View, ScrollView, TouchableOpacity, ImageBackground, Image,StatusBar } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Header } from 'react-native-elements';
export default class MenuCitas extends Component {
    constructor(props) {
        super(props);
        this.state = {
            initSplash: true,
            loginSpl: true,
        };
    }
    render() {
        return (
            <View style={{ height: '100%', width: '100%' }} >
                <Header
                    leftComponent={<TouchableOpacity onPress={() => {
                        this.props.navigation.navigate('Menu');
                    }}>
                        <Icon name="arrow-left" type="Ionicons" size={30} color="#fff" />
                    </TouchableOpacity>}
                    centerComponent={{ text: 'NOTIFICACIONES', style: { color: '#fff' } }}

                    rightComponent={<TouchableOpacity onPress={() => {
                        this.props.navigation.navigate('Menu');

                    }}>
                        <Icon name="home" type="Ionicons" size={30} color="#fff" />
                    </TouchableOpacity>}
                    containerStyle={{
                        backgroundColor: '#3D6DCC',
                        justifyContent: 'space-around',
                        height: '8%'
                    }}
                    containerStyle={{
                        justifyContent: 'space-around',
                        backgroundColor: '#FF9100'
                    }} />
                                        <StatusBar backgroundColor='#FF9100' barStyle="light-content" />
                <View style={{ alignItems: 'center' }}>
                    <Image
                        source={require('../../assets/png/lg.png')}
                        style={styles.imagen}
                    />
                    <Text style={{ fontSize: 20, color: '#617792', fontWeight: 'bold', textAlign: 'center' }}>MECÁNICA EXPRESS</Text>

                </View>
                <View style={{width: '100%', height: '70%', alignItems: 'center'}}>

                <ScrollView style={{ height: '100%', width: '90%' }}>

                        <View style={styles.contImage}>
                            <ImageBackground source={require('../../assets/jpg/im5.jpeg')} imageStyle={{ borderRadius: 20 }} style={{ width: '100%', height: '100%' }}>
                                <TouchableOpacity
                                    onPress={() =>
                                        this.props.navigation.navigate('Citas')
                                    }
                                    style={styles.btnImage}
                                >
                                    <Text style={{ fontSize: 30, fontWeight: 'bold', color: '#FFF' }}>MIS CITAS</Text>
                                </TouchableOpacity>
                            </ImageBackground>
                        </View>
                        <View style={styles.contImage}>
                            <ImageBackground source={require('../../assets/jpg/im2.jpeg')} imageStyle={{ borderRadius: 20 }}
                                style={{ width: '100%', height: '100%', borderRadius: 25 }}>
                                <TouchableOpacity
                                    onPress={() =>
                                        this.props.navigation.navigate('CitasPendientes')
                                    }
                                    style={styles.btnImage}
                                >
                                    <Text style={{ fontSize: 30, fontWeight: 'bold', color: '#FFF' }}>CITAS PENDIENTES</Text>
                                </TouchableOpacity>
                            </ImageBackground>
                        </View>
                       
                 

                    </ScrollView>
                </View>

            </View >
        );
    }
    _renderItem = ({ item, dimensions }) => (
        <View>
            <Image
                style={styles.image}
                source={item.image} />
        </View>
    );
}


const styles = StyleSheet.create({
    image: {
        width: 400,
        height: 200,

    },
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',

    },
    contImage: {
        flex: 1,
        marginTop: 3,
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 2,
        height: 200,
        width: '100%',
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
    },
    btnImage: {
        flex: 1,
        justifyContent: 'center',
        paddingBottom: 10,
        alignItems: 'center'
    },
    imagen: {
        width: 100,
        height: 100,


    },
});
