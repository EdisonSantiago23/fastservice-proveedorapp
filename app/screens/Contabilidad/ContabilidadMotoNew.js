import React, {Component} from 'react';
import {
  StyleSheet,
  StatusBar,
  ScrollView,
  TouchableOpacity,
  Dimensions,
  Image,
  Animated,
  View,
} from 'react-native';
import mecanicoApi from '../../api/mecanico';
import LoadingProgress from '../../common/LoadingProgress';
import {Text} from 'galio-framework';
import AsyncStorage from '@react-native-community/async-storage';
import Constants from '../../common/Constants';
import Geolocation from '@react-native-community/geolocation';
import Icon from 'react-native-vector-icons/FontAwesome';
var {height, width} = Dimensions.get('window');
import LinearGradient from 'react-native-linear-gradient'; // Only if no expo
import {ListItem} from 'react-native-elements';
import {SCLAlert, SCLAlertButton} from 'react-native-scl-alert';
import {showDangerMsg, showSuccesMsg} from '../../common/ShowMessages';
import { Header} from 'react-native-elements';
export default class ContabilidadMotoNew extends Component {
  constructor(props) {
    super(props);
    this.mecanicoApi = new mecanicoApi();

    this.state = {
      initSplash: true,
      loginSpl: true,
      loading: true,
      reservas: [],
      estado: 'Acpetado',
      show: false,
      poi: {
        latitude: '',
        longitude: '',
      },
      region: {
        latitude: 0,
        longitude: 0,
        latitudeDelta: 0,
        longitudeDelta: 0.04,
      },
    };
    this.RotateValueHolder = new Animated.Value(0);
    this.getLocalidad();
  }
  getLocalidad = async () => {
    await Geolocation.getCurrentPosition(position => {
      const {latitude, longitude} = position.coords;
      this.state.poi.latitude = position.coords.latitude;
      this.state.poi.longitude = position.coords.longitude;
      this.setState({
        region: {
          longitude: longitude,
          latitude: latitude,
          latitudeDelta: 0.029213524352655895,
          longitudeDelta: (width / height) * 0.029213524352655895,
        },
      });
      this.getReservas();
    });
  };
  getReservas = async () => {
    this.mecanicoApi
      .getRecervaproveedorMotos(
        await AsyncStorage.getItem(Constants.STORE_ID_USER),
      )
      .then(res => {
        if (res.status) {
          this.setState({show: true, loading: false});
        }
        this.setState({
          reservas: res,
        });
        if (res != undefined) {
          this.setState({
            loading: false,
          });
        }
      });
  };

  estado = async (estado, cliente) => {
    try {
      this.setState({loading: true});
      cliente.estado = estado;
      if (estado == 2) {
        this.state.estado = 'Rechazado';
        cliente.content =
          'Mecánico ' + cliente.nombremecanico + ' no disponible';
      } else {
        this.state.estado = 'Acpetada';
        cliente.content =
          'El mecánico ' + cliente.nombremecanico + ' ha aceptado';
      }
      this.mecanicoApi.respuestaLocal(cliente).then(res => {
        if (res.idReservaMecanico) {
          this.getReservas();

          showSuccesMsg(this.state.estado, 'Emergencia');
          this.setState({loading: false});
        } else {
          if (res.status && res.status == 'error') {
            showDangerMsg(res.message, 'Error');
            this.setState({loading: false});
          }
        }
      });
    } catch (err) {
      showDangerMsg(err.message, 'Error');
    }
  };
  handleOpen = () => {
    this.setState({show: true});
  };

  handleClose = () => {
    this.setState({show: false});
    this.props.navigation.goBack();
  };
  render() {
    if (this.state.loading) {
      return <LoadingProgress />;
    }
    if (!this.state.loading) {
      if (!this.state.reservas.status) {
        return (
          <View>
            <Header
              leftComponent={
                <TouchableOpacity
                  onPress={() => {
                    this.props.navigation.navigate('MenuContabilidad');
                  }}>
                  <Icon
                    name="arrow-left"
                    type="Ionicons"
                    size={30}
                    color="#fff"
                  />
                </TouchableOpacity>
              }
              centerComponent={{
                text: 'SERVICIO AUTORIZADO',
                style: {color: '#fff'},
              }}
              rightComponent={
                <TouchableOpacity
                  onPress={() => {
                    this.props.navigation.navigate('Menu');
                  }}>
                  <Icon name="home" type="Ionicons" size={30} color="#fff" />
                </TouchableOpacity>
              }
              containerStyle={{
                justifyContent: 'space-around',
                backgroundColor: '#FF9100',
              }}
            />
            <StatusBar backgroundColor="#FF9100" barStyle="light-content" />

            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                marginLeft: '30%',
              }}>
              <View>
                <Image
                  source={require('../../assets/png/lg.png')}
                  style={styles.imagen}
                />
                <View>
                  <Text
                    style={{
                      fontSize: 18,
                      color: '#617792',
                      fontWeight: 'bold',
                    }}>
                    {' '}
                    MECÁNICA EXPRESS
                  </Text>
                </View>
              </View>
            </View>

            <ScrollView style={{height: '75%', width: '100%'}}>
              <View>
                {this.state.reservas.map(novedad => (
                  <ListItem
                    style={{
                      paddingTop: '1%',
                    }}
                    badge={{
                      value: '$' + novedad.precioemergencia,
                      textStyle: {
                        color: 'white',
                        fontWeight: 'bold',
                        fontSize: 15,
                      },
                      containerStyle: {color: 'orange'},
                    }}
                    friction={90} //
                    tension={100} // These props are passed to the parent component (here TouchableScale)
                    activeScale={0.95} //
                    linearGradientProps={{
                      colors: ['#BCBCCE', '#617792'],
                      start: {x: 1, y: 0},
                      end: {x: 0.2, y: 0},
                    }}
                    ViewComponent={LinearGradient} // Only if no expo
                    leftAvatar={{
                      rounded: true,
                      source: {uri: Constants.BASEURI + novedad.imgpequena},
                    }}
                    title={novedad.nombreemergencia + ' KM'}
                    titleStyle={{color: 'white', fontWeight: 'bold'}}
                    subtitle={novedad.nombremecanico}
                    subtitleStyle={{color: 'white'}}
                    bottomDivider
                    chevron={{color: '#617792'}}
                  />
                ))}
              </View>
            </ScrollView>
          </View>
        );
      } else {
        return (
          <View style={styles.container}>
            <SCLAlert
              theme="danger"
              show={this.state.show}
              title="Emergencias"
              onRequestClose={this.handleClose}
              cancellable={true}
              subtitle="Actualmente no tienes emergencias por atender">
              <SCLAlertButton theme="danger" onPress={this.handleClose}>
                Atras
              </SCLAlertButton>
            </SCLAlert>
          </View>
        );
      }
    }
  }
  renderBtn(cliente) {
    if (cliente.estado == 0) {
      return (
        <View style={styles.containerdtn}>
          <TouchableOpacity
            style={styles.btnVerEnSitioL}
            onPress={() => this.estado(1, cliente)}>
            <Text style={{textAlign: 'center'}}>ACEPTAR</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.btnVerEnSitioR}
            onPress={() => this.estado(2, cliente)}>
            <Text style={{textAlign: 'center'}}>RECHAZAR</Text>
          </TouchableOpacity>
        </View>
      );
    }
    if (cliente.estado == 1) {
      return (
        <View>
          <Text
            style={{
              fontSize: 15,
              color: '#35DC32',
              fontWeight: 'bold',
              textAlign: 'center',
            }}>
            {' '}
            En espera de confirmacion de {cliente.nommbreusuario}
          </Text>
        </View>
      );
    }
    if (cliente.estado == 3) {
      return (
        <View>
          <Text
            style={{
              fontSize: 15,
              color: '#3243DC',
              fontWeight: 'bold',
              textAlign: 'center',
            }}>
            {' '}
            {cliente.nommbreusuario} confirmo el mecánico, comparte el destio
          </Text>
          <Text
            style={{
              fontSize: 15,
              color: '#3243DC',
              fontWeight: 'bold',
              textAlign: 'center',
            }}
          />
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    width: '100%',
  },
  containerdtn: {
    bottom: 0,
    alignItems: 'center',
    flexDirection: 'row',
  },
  btnVerEnSitioL: {
    backgroundColor: '#32DC36',
    borderRadius: 20,
    alignItems: 'center',
    width: '50%',
    padding: '3%',
    color: 'blue',
    borderWidth: 1,
    marginTop: '5%',
    borderColor: '#4982e6',
  },
  btnVerEnSitioR: {
    backgroundColor: '#FF333C',
    borderRadius: 20,
    alignItems: 'center',
    width: '50%',
    padding: '3%',
    color: 'blue',
    borderWidth: 1,
    marginTop: '5%',
    borderColor: '#4982e6',
  },

  profile: {
    flex: 1,
  },
  imagen: {
    width: 75,
    height: 75,
    marginLeft: '20%',
  },
  contImage: {
    flex: 1,
    marginTop: 3,
    justifyContent: 'center',
    width: '90%',
    height: 560,
    borderWidth: 2,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
  },
  map: {
    width: '100%',
    marginTop: '15%',
    height: '65%',
  },
  headerContainer: {
    flexDirection: 'row',
    padding: 5,
    alignItems: 'flex-start',
  },
  btnVerEnSitio: {
    backgroundColor: '#F5F6F7',
    borderRadius: 20,
    width: '35%',
    padding: '3%',
    color: 'blue',
    borderWidth: 1,
    marginTop: '5%',
    borderColor: '#4982e6',
  },
  carousel: {
    position: 'absolute',
    bottom: 0,
    marginBottom: 48,
  },
  cardContainer: {
    alignSelf: 'center',
    backgroundColor: '#617792',
    height: 630,
    width: 350,
    padding: 50,
    borderRadius: 24,
  },
  cardImage: {
    height: 120,
    width: 300,
    bottom: 0,
    position: 'absolute',
    borderBottomLeftRadius: 24,
    borderBottomRightRadius: 24,
  },
  button: {
    alignSelf: 'center',

    fontSize: 16,
    color: '#fff',
    backgroundColor: '#617792',
    paddingVertical: 10,
    paddingHorizontal: 15,
    textAlign: 'center',
    height: 50,
    width: 350,
    padding: 50,
    borderRadius: 24,
  },
  cardTitle: {
    color: 'white',
    fontSize: 22,
    alignSelf: 'center',
  },
  profile: {
    flex: 1,
  },
  profileContainer: {
    width: '100%',
    height: '50%',
    color: '#617792',
    padding: 0,
    zIndex: 1,
  },
  profileBackground: {
    width: '100%',
    height: 350 / 2,
  },
  profileCard: {
    marginTop: 65,
    borderTopLeftRadius: 6,
    borderTopRightRadius: 6,
    backgroundColor: 1,
    shadowColor: 'black',
    shadowOffset: {200: 0, 200: 0},
    shadowRadius: 8,
    shadowOpacity: 0.2,
  },
  info: {},
  titulo: {
    marginTop: -70,
  },
  avatarContainer: {
    position: 'relative',
    marginTop: -25,
  },
  avatar: {
    width: 70,
    height: 70,
    borderRadius: 62,
    borderWidth: 0,
  },
  nameInfo: {
    marginTop: -50,
  },
  divider: {
    width: '90%',
    borderWidth: 1,
    borderColor: '#E9ECEF',
  },
  thumb: {
    borderRadius: 4,
    marginVertical: 4,
    alignSelf: 'center',
    width: 300,
    height: 300,
  },
});
