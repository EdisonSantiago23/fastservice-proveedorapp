import React, { Component } from 'react';
import { Text, SafeAreaView, Image, ScrollView, Alert, StyleSheet, TextInput, View, TouchableOpacity, StatusBar, ActivityIndicator } from 'react-native';
import ImagePicker from 'react-native-image-picker';
import NiftyText from '../../common/NiftyText';
import mecanicoApi from '../../api/mecanico.js';
import FeatherIcon from 'react-native-vector-icons/Feather';
import AntDesign from 'react-native-vector-icons/AntDesign';
import { CustomPicker } from 'react-native-custom-picker';
import {
    SCLAlert,
    SCLAlertButton
} from 'react-native-scl-alert'
import LoadingProgress from '../../common/LoadingProgress';

import { Overlay, Header } from 'react-native-elements';
export default class Novedadesmes extends Component {
    constructor(props) {
        super(props);
        this.mecanicoApi = new mecanicoApi();
        this.state = {
            login: true,
            registro: false,
            validado: false,
            provincias: [],
            ciudades: [],
            local: this.props.route.params.local,
            imageFileData: '',
            loadingBtn: false,
            loading: false,
            productFeatures: {
                id: 1,
                nombre: 'info',
                arrFotos: [],
            },
            show: false,

            arrFotos: [
                {
                    id: 0,
                    fileData: '',
                    nombre: '',
                    precio: '',
                    descuento: '',
                    stock: '',
                    local_idLocal: '',
                    tiponovedades_idtiponovedades: ''
                },
                {
                    id: 1,
                    fileData: '',
                    nombre: '',
                    precio: '',
                    descuento: '',
                    stock: '',
                    local_idLocal: '',
                    tiponovedades_idtiponovedades: ''
                },
                {
                    id: 2,
                    fileData: '',
                    nombre: '',
                    precio: '',
                    descuento: '',
                    stock: '',
                    local_idLocal: '',
                    tiponovedades_idtiponovedades: ''
                },
                {
                    id: 3,
                    fileData: '',
                    nombre: '',
                    precio: '',
                    descuento: '',
                    stock: '',
                    local_idLocal: '',
                    tiponovedades_idtiponovedades: ''
                },
                {
                    id: 4,
                    fileData: '',
                    nombre: '',
                    precio: '',
                    descuento: '',
                    stock: '',
                    local_idLocal: '',
                    tiponovedades_idtiponovedades: ''
                },
                {
                    id: 5,
                    fileData: '',
                    nombre: '',
                    precio: '',
                    descuento: '',
                    stock: '',
                    local_idLocal: '',
                    tiponovedades_idtiponovedades: ''
                }
            ],
            tipoNovedades: [],
        };
        this.getTipoNovedades();
    }
    getTipoNovedades() {
        this.mecanicoApi.getTipoNovedades().then(res => {
            this.setState({
                tipoNovedades: res,
            });
            if (res != undefined) {
                this.setState({
                    loading: false,
                });
            }
        });
    }

    handleOpen = () => {
        this.setState({ show: true })
    }

    handleClose = () => {
        this.setState({ show: false })
        this.props.navigation.replace('Menu');

    }
    updateStatusFeature(param, value, index) {
        let userFeatConst = this.state.arrFotos;
        userFeatConst[index]['local_idLocal'] = this.state.local.idLocal;
        userFeatConst[index][param] = value;
        this.setState({ userFeatures: userFeatConst });
    }

    AddTipo(diTipo, index) {
        let copy = this.state.arrFotos;
        copy[index].idtiponovedades = diTipo;
        this.setState({ arrFotos: copy });
    }



    validadorCampo(userFeatures) {
        if (userFeatures.fileData != ''
            && userFeatures.nombre != ''
            && userFeatures.precio != ''
            && userFeatures.descuento != ''
            && userFeatures.stock != ''
            && userFeatures.local_idLocal != ''
            && userFeatures.tiponovedades_idtiponovedades != '') {
            if (this.validarprecio(userFeatures.precio)) {
                if (userFeatures.descuento >= 100 || userFeatures.descuento < 0) {
                    this.setState({ validado: false, loading: false })
                    Alert.alert('Error', 'Ingresa un descuento válido(0% al 100%)', [{ text: 'Cerrar' }]);
                    return false;
                } else {
                    return true;
                }
            } else {
                this.setState({ validado: false, loading: false })
                return false;
            }
        } else {
            this.setState({ validado: false, loading: false })

            Alert.alert('Error', 'No debe existir campos vacios', [{ text: 'Cerrar' }]);
        }
    }

    createProductos = async () => {
        try {

            this.setState({ loading: true });
            let params = this.state.arrFotos;
            let fotosArray = [];
            params.forEach(element => {
                if (element.fileData != '') {
                    this.state.validado=this.validadorCampo(element);
                    this.setState({ validado:  this.validadorCampo(element) })                   
                    fotosArray.push(element);
                }
            });
            if (this.state.validado) {
                this.state.productFeatures.arrFotos = fotosArray;
                this.mecanicoApi.createNovedades(this.state.productFeatures).then(res => {
                    if (res) {
                        this.setState({ loading: false, show: true });

                    } else {
                        if (res.status && res.status == 'error') {
                            this.setState({ loading: false });
                            Alert.alert('Error', res.message, [{ text: 'Cerrar' }]);

                        }
                    }
                });
            }

        } catch (err) {
            this.setState({ loading: false });
            Alert.alert('Error', err.message, [{ text: 'Cerrar' }]);
        }
    };
    validarprecio(value) {
        var regex = /^[1-9]\d*(((,\d{3}){1})?(\.\d{0,2})?)$/;
        if (regex.test(value)) {
            return true;
        } else {
            Alert.alert('Error', 'Debes in gresar un precio válido', [{ text: 'Cerrar' }]);
            this.setState({ loading: false });

            return false;

        }
    }

    chooseImage(index) {
        let options = {
            title: 'Seleccionar imagen',
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
            cancelButtonTitle: 'Cancelar',
            takePhotoButtonTitle: 'Tomar Foto',
            chooseFromLibraryButtonTitle: 'Elegir Foto de la Galería',
            mediaType: 'photo',
            index: index,
        };
        ImagePicker.showImagePicker(options, response => {
            if (response.didCancel) {
            } else if (response.error) {
            } else if (response.customButton) {
                alert(response.customButton);
            } else {
                let ministatus = this.state.arrFotos;
                ministatus[options.index].fileData =
                    'data:image/jpg;base64,' + response.data;
                this.setState({
                    arrFotos: ministatus,
                });
            }
        });
    }


    renderFileData(item, index) {
        if (this.state.arrFotos[index].fileData) {
            return (
                <View>

                    <Image
                        source={{
                            uri: this.state.arrFotos[index].fileData,
                        }}
                        style={styles.images}
                    />
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <NiftyText others={rnpickerStyles.labelStyle}>
                            Novedad:</NiftyText>
                        <View style={rnpickerStyles.pickerContainer}>
                            <CustomPicker
                                placeholder={'Seleccionar un tipo'}
                                options={this.state.tipoNovedades}
                                getLabel={item => item.label}
                                onValueChange={val => {
                                    this.updateStatusFeature('tiponovedades_idtiponovedades', val.value, index);
                                }}
                            />
                        </View>
                    </View>
                    <TextInput
                        placeholder="Descripción"
                        style={styles.inputSearch}
                        onChangeText={val => this.updateStatusFeature('nombre', val, index)} />
                    <TextInput
                        placeholder="Precio"
                        style={styles.inputSearch}
                        keyboardType={'numeric'}
                        onChangeText={val => this.updateStatusFeature('precio', parseFloat(val), index)} />
                    <TextInput
                        placeholder="Descuento %"
                        onChangeText={val => { this.updateStatusFeature('descuento', parseFloat(val).toFixed(0), index) }}
                        keyboardType={'numeric'}
                        style={styles.inputSearch}
                    />

                    <TextInput
                        placeholder="Stock"
                        onChangeText={val => {
                            val == 0 ? (
                                this.updateStatusFeature('stock', 0)
                            ) : (
                                    this.updateStatusFeature('stock', parseFloat(val).toFixed(0),index)
                                )
                        }}
                        keyboardType={'numeric'}
                        style={styles.inputSearch}
                    />
                    <TouchableOpacity
                        style={{ position: 'absolute', top: 15, right: 5 }}
                        onPress={() => {
                            let copy = this.state.arrFotos;
                            copy[index].fileData = null;
                            this.setState({ arrFotos: copy });
                        }}>
                        <CloseIcon />
                    </TouchableOpacity>

                </View>
            );
        } else {
            return (
                <FeatherIcon
                    name="camera"
                    size={50}
                    style={{ margin: 50 }}
                    color="#000"
                />

            );
        }
    }



    render() {
        if (this.state.loading) {
            return <LoadingProgress />;
        }
        if (!this.state.loading) {

            return (
                <View style={{ height: '100%', width: '100%' }} >
                    <Header

                        centerComponent={{ text: 'REGISTRO DE NOVEDADES', style: { color: '#fff' } }}

                        containerStyle={{
                            justifyContent: 'space-around',
                            backgroundColor: '#FF9100'
                        }}
                    />
                    <StatusBar backgroundColor='#FF9100' barStyle="light-content" />
                    <ScrollView>
                        <SCLAlert
                            theme="success"
                            show={this.state.show}
                            title="MECÁNICA EXPRESS"
                            onRequestClose={this.handleClose}
                            cancellable={true}

                            subtitle="Registro con éxito"
                        >
                            <SCLAlertButton theme="success" onPress={this.handleClose}>Ok</SCLAlertButton>
                        </SCLAlert>
                        <View style={styles.container}>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <Image source={require('../../assets/png/lg.png')} style={styles.imagen} />
                                <View>
                                    <Text style={{ fontSize: 15, color: '#617792', fontWeight: 'bold', textAlign: 'center' }}>MECÁNICA EXPRESS</Text>
                                    <Text style={{ fontSize: 15, color: '#617792', fontWeight: 'bold', textAlign: 'center' }}>Registra 6 productos de promoción</Text>
                                </View>
                            </View>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <SafeAreaView style={styles.container}>
                                    <ScrollView horizontal={true}>
                                        {this.state.arrFotos.map((item, index) => (
                                            <TouchableOpacity
                                                keyExtractor={(item, index) => index.toString()}
                                                style={styles.fotoContainer}
                                                onPress={() => this.chooseImage(index)}>
                                                {this.renderFileData(item, index)}
                                            </TouchableOpacity>

                                        ))}
                                    </ScrollView>
                                </SafeAreaView>
                            </View>
                            <View style={styles.ellanoteama}>
                                {this.renderBtn()}
                            </View>
                        </View>
                    </ScrollView>
                </View>
            );
        }
    }
    renderBtn() {
        if (!this.state.loadingBtn) {
            return (
                <View>
                    <TouchableOpacity
                        onPress={() => {
                            this.createProductos();
                        }}
                        style={styles.btnStyle}>
                        <Text style={{ fontSize: 14, textAlign: 'center', color: '#1F4997' }}>
                            GUARDAR
                        </Text>
                    </TouchableOpacity>
                </View>
            );
        } else {
            return (
                <TouchableOpacity style={styles.btnStyle}>
                    <ActivityIndicator size="small" color={'#1F4997'} />
                </TouchableOpacity>
            );
        }
    }
}
const CloseIcon = () => {
    return (
        <AntDesign
            name={'close'}
            size={12}
            color={'#000'}
            style={{ backgroundColor: '#D4D7DD', borderRadius: 50 }}
        />
    );
};

const rnpickerStyles = StyleSheet.create({
    labelStyle: { textAlign: 'left', minWidth: 60 },
    labelStyle2: { textAlign: 'center', minWidth: 60 },
    pickerContainer: {
        borderBottomWidth: 1,
        borderBottomColor: '#FFFF',
        flex: 1,
    },
});

const styles = StyleSheet.create({
    btnVerEnSitio: {
        backgroundColor: '#F5F6F7',
        borderRadius: 20,
        alignItems: 'center',
        width: '70%',
        padding: '3%',
        color: 'blue',
        borderWidth: 1,
        marginTop: '5%',
        borderColor: '#4982e6',
    },
    btnVerEnSitio3: {
        backgroundColor: '#F5F6F7',
        borderRadius: 20,
        alignItems: 'center',
        width: '70%',
        padding: '3%',
        color: 'blue',
        borderWidth: 1,
        marginTop: '5%',
        borderColor: '#4982e6',
    },
    btnVerEnSitio2: {
        backgroundColor: '#F5F6F7',
        borderRadius: 20,
        alignItems: 'center',
        width: '70%',
        padding: '3%',
        color: 'blue',
        borderWidth: 1,
        marginTop: '-10%',
        borderColor: '#4982e6',
    },
    descriptionTag: {
        color: '#502A18',
    },
    btnTextStyle: {
        color: '#FFFFFF',
        fontSize: 22,
        alignItems: 'center',
        textAlign: 'center',
    },
    btnStyle: {
        alignItems: 'center',
        borderWidth: 1,
        borderColor: '#4982e6',
        borderRadius: 20,
        justifyContent: 'center',
        backgroundColor: '#F5F6F7',
        height: 50,
        marginTop: 20,
        marginBottom: 80,
    },
    images: {
        width: 180,
        height: 180,
        borderColor: 'black',
        borderWidth: 1,
    },
    fotoContainer: {
        borderColor: '#424244',
        borderWidth: 1,
        borderStyle: 'dotted',
        backgroundColor: '#F9FCFD',
    },
    container: {
        flex: 1,
        alignItems: 'center',
        padding: 15,
        backgroundColor: '#fff',
    },
    ellanoteama: {
        minWidth: '80%',
        backgroundColor: '#fff',
    },
    createAccount: {
        fontSize: 30,
        textAlign: 'left',
        marginVertical: 15,
    },
    inputSearch: {
        borderBottomWidth: 1,
    },
    imagen: {
        width: 100,
        height: 100,


    },
    imagen2: {
        width: 155,
        height: 130,


    },
});
