import React, { Component } from 'react';
import { StyleSheet, ImageBackground, ScrollView, TouchableOpacity, StatusBar, Image, Animated, View } from 'react-native';
import mecanicoApi from '../../api/mecanico.js';
import LoadingProgress from '../../common/LoadingProgress';
import Constants from '../../common/Constants';
import AsyncStorage from '@react-native-community/async-storage';
import Icon from 'react-native-vector-icons/FontAwesome';

import { Overlay, Header } from 'react-native-elements';

import { Block, Text, theme } from "galio-framework";

import {
    SCLAlert,
    SCLAlertButton
} from 'react-native-scl-alert'

export default class MisNovedades extends Component {
    constructor(props) {
        super(props);
        this.mecanicoApi = new mecanicoApi();

        this.state = {
            initSplash: true,
            loginSpl: true,
            loading: true,
            novedades: [],
            show: false,

        };
        this.GetNovedades();
    }

    GetNovedades = async () => {
        this.mecanicoApi.getMisNovedades(await AsyncStorage.getItem(Constants.STORE_ID_USER)).then(res => {
            if (res.status) {
                this.setState({ show: true, loading: false, })

            }
            this.setState({
                novedades: res,
            });
            if (res != undefined) {
                this.setState({
                    loading: false,
                });
            }
        });
    }
    handleOpen = () => {
        this.setState({ show: true })
    }

    handleClose = () => {
        this.setState({ show: false })

    }
    handleCloseSalir = () => {
        this.setState({ show: false })
        this.props.navigation.replace('Menu');

    }
    render() {
        if (this.state.loading) {
            return <LoadingProgress />;
        }
        if (!this.state.loading) {
            if (!this.state.novedades.status) {

                return (
                    <View>
                          <Header
                    leftComponent={<TouchableOpacity onPress={() => {
                        this.props.navigation.goBack();
                    }}>
                        <Icon name="arrow-left" type="Ionicons" size={30} color="#fff" />
                    </TouchableOpacity>}
                    centerComponent={{ text: 'MIS NOVEDADES', style: { color: '#fff' } }}

                    rightComponent={<TouchableOpacity onPress={() => {
                        this.props.navigation.goBack();

                    }}>
                        <Icon name="home" type="Ionicons" size={30} color="#fff" />
                    </TouchableOpacity>}
                    containerStyle={{
                        justifyContent: 'space-around',
                        backgroundColor: '#FF9100'
                    }}
                />
                        <View style={{ flexDirection: 'row', alignItems: 'center', marginLeft: '30%' }}>
                            <View>
                                <Image
                                    source={require('../../assets/png/lg.png')}
                                    style={styles.imagen} />
                                <View>
                                    <Text style={{ fontSize: 17, color: '#617792', fontWeight: 'bold' }}> MECÁNICA EXPRESS</Text>
                                </View>
                            </View>

                        </View>
                        
              
                        <ScrollView style={{ height: '80%' }}>

                            <View style={styles.container}>
                                {!this.state.novedades.status ? (
                                    <View>
                                        {this.state.novedades.map(novedad => (
                                            <View style={styles.contImage}>

                                                <Block flex style={styles.profileCard}>
                                                    <Block middle style={styles.avatarContainer}>
                                                        <Image
                                                            source={{ uri: Constants.BASEURI + novedad.url }}
                                                            style={styles.avatar}
                                                        />
                                                    </Block>
                                                    <Block style={styles.info}>
                                                        <Block middle
                                                            row
                                                            space="evenly"
                                                            style={{ marginTop: 20, paddingBottom: 24 }}>

                                                        </Block>
                                                        <Block middle style={styles.nameInfo}>
                                                            <Text bold size={28} color="#32325D">
                                                                {novedad.nombre}</Text>


                                                        </Block>

                                                        <Block row space="between">

                                                            <Block middle>
                                                                <Text bold
                                                                    size={16}
                                                                    color="#525F7F"
                                                                    style={{ marginBottom: 4 }}
                                                                >
                                                                    ${novedad.precio.toFixed(2)}
                                                                </Text>
                                                                <Text size={16}>Precio</Text>
                                                            </Block>
                                                            <Block middle>
                                                                <Text
                                                                    bold
                                                                    color="#525F7F"
                                                                    size={16}
                                                                    style={{ marginBottom: 4 }}
                                                                >
                                                                    {novedad.descuento}%

                                                            </Text>

                                                                <Text size={16}>Descuento</Text>
                                                            </Block>
                                                            <Block middle>
                                                                <Text
                                                                    bold
                                                                    color="#525F7F"
                                                                    size={16}
                                                                    style={{ marginBottom: 4 }}
                                                                >
                                                                    {novedad.stock} ud.

                                                            </Text>
                                                                <Text size={16}>Stock</Text>
                                                            </Block>
                                                        </Block>
                                                    </Block>

                                                </Block>
                                            </View>

                                        ))}
                                    </View>

                                ) : (
                                        <View>
                                            <Block middle style={styles.nameInfo}>
                                                <Text bold size={28} color="#32325D">
                                                    Sin Novedades</Text>


                                            </Block>
                                            <Block middle style={styles.nameInfo}>
                                                <Text bold size={28} color="#32325D">
                                                    Registrados</Text>


                                            </Block>
                                        </View>
                                    )}


                            </View>

                        </ScrollView>

                    </View>

                );
            } else {
                return (
                    <View style={styles.container}>
                        <SCLAlert
                            theme="warning"
                            show={this.state.show}
                            title="Mis novedades"
                            onRequestClose={this.handleClose}
                            cancellable={true}

                            subtitle="Actualmente no tienes novedades disponibles"
                        >
                            <SCLAlertButton theme="warning" onPress={this.handleClose}>Registrar</SCLAlertButton>
                            <SCLAlertButton theme="warning" onPress={this.handleCloseSalir}>Salir</SCLAlertButton>
                        </SCLAlert>
                    </View>
                );

            }
        }
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        padding: '3%',
    },
    profile: {
        // marginBottom: -HeaderHeight * 2,
        flex: 1
    },
    imagen: {
        width: 70,
        height: 70,
        marginLeft: '25%'

    },
    contImage: {
        flex: 1,
        marginTop: 3,
        justifyContent: 'center',
        borderWidth: 2,
        width: 400,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
    },
    map: {
        ...StyleSheet.absoluteFillObject
    },
    headerContainer: {
        flexDirection: 'row',
        padding: 5,
        alignItems: 'flex-start',
    },
    btnVerEnSitio: {
        backgroundColor: '#F5F6F7',
        borderRadius: 20,
        width: '35%',
        padding: '3%',
        color: 'blue',
        borderWidth: 1,
        marginTop: '5%',
        borderColor: '#4982e6',
    },
    carousel: {
        position: 'absolute',
        bottom: 0,
        marginBottom: 48
    },
    cardContainer: {
        alignSelf: 'center',
        backgroundColor: '#617792',
        height: 600,
        width: 350,
        padding: 50,
        borderRadius: 24
    },
    cardImage: {
        height: 120,
        width: 300,
        bottom: 0,
        position: 'absolute',
        borderBottomLeftRadius: 24,
        borderBottomRightRadius: 24
    },
    button: {
        alignSelf: 'center',

        fontSize: 16,
        color: '#fff',
        backgroundColor: '#617792',
        paddingVertical: 10,
        paddingHorizontal: 15,
        textAlign: "center",
        height: 50,
        width: 350,
        padding: 50,
        borderRadius: 24
    },
    cardTitle: {
        color: 'white',
        fontSize: 22,
        alignSelf: 'center'
    },
    profile: {
        flex: 1
    },
    profileContainer: {
        width: '100%',
        height: '50%',
        color: '#617792',
        padding: 0,
        zIndex: 1
    },
    profileBackground: {
        width: '100%',
        height: 350 / 2
    },
    profileCard: {
        // position: "relative",
        padding: 1,
        marginHorizontal: 1,
        marginTop: 65,
        borderTopLeftRadius: 6,
        borderTopRightRadius: 6,
        backgroundColor: 1,
        shadowColor: "black",
        shadowOffset: { 200: 0, 200: 0 },
        shadowRadius: 8,
        shadowOpacity: 0.2,
        zIndex: 2
    },
    info: {
        paddingHorizontal: 40
    },
    avatarContainer: {
        position: "relative",
        marginTop: -60
    },
    avatar: {
        width: 300,
        height: 300,
        borderWidth: 0
    },
    nameInfo: {
        marginTop: -15
    },
    divider: {
        width: "90%",
        borderWidth: 1,
        borderColor: "#E9ECEF"
    },
    thumb: {
        borderRadius: 4,
        marginVertical: 4,
        alignSelf: "center",
        width: 300,
        height: 300
    }
});
