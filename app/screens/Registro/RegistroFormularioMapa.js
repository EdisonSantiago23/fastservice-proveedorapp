
import React, { Component } from 'react';
import { Text, StatusBar, Image, ScrollView, StyleSheet, TextInput, View, TouchableOpacity,  ActivityIndicator } from 'react-native';
import NiftyText from '../../common/NiftyText';
import { CustomPicker } from 'react-native-custom-picker';
import mecanicoApi from '../../api/mecanico.js';
import LoadingProgress from '../../common/LoadingProgress';
import Constants from '../../common/Constants';
import {showDangerMsg,showSuccesMsg} from '../../common/ShowMessages';

import { CheckBox } from 'react-native-elements';

import AsyncStorage from '@react-native-community/async-storage';
import Icon from 'react-native-vector-icons/FontAwesome';

import { Overlay, Header } from 'react-native-elements';

export default class RegistroFormularioMapa extends Component {
    constructor(props) {
        super(props);

        this.mecanicoApi = new mecanicoApi();
        this.state = {
            provincias: [],
            userId: '',
            pushToken: '',

            ciudades: [],
            imageFileData: '',
            checked: false,
            loading: true,
            tiposlocal: [],
            tipo: this.props.route.params.tipo,
            data: this.props.route.params.data,
            arrFotos: [
            ],
            userFeatures: {
                idLocal: 0,
                nombre: '',
                calificacion: 1,
                latitud: 0,
                longitud: 0,
                direccion: '',
                ciudad_idCiudad: '',
                representante: '',
                telefono: '',
                correo: '',
                TipoLocal_idTipoLocal: '',
                estado: 0,
                password: '',
                cedula: '',
                url: '',
                localtype: '',
                tocken: '',

            },

        };
        this.getProvincia();
        this.getTiposLocal();

    }


    validarContrasena() {
        if (this.state.userFeatures.password.length >= 6) {
            return true;
        } else {
            showDangerMsg('La contraseña debe tener mínimo 6 caracteres','Campos erróneos',);
            return false;
        }
    }

    onChangeText = (key, val) => {
        this.setState({ [key]: val });
    };

    updateStatusFeature(param, value) {
        let userFeatConst = this.state.userFeatures;
        userFeatConst[param] = value;
        this.setState({ userFeatures: userFeatConst });
    }

    validador() {
        if (!this.state.tipo) {
            this.state.userFeatures.representante = this.state.userFeatures.nombre;
            this.state.userFeatures.TipoLocal_idTipoLocal = 6;

        }

        if (this.validateEmail(this.state.userFeatures.correo) && this.validarContrasena() && this.validateCedula(this.state.userFeatures.cedula) && this.validadorCampo()) {
            return true;
        } else {
            return false;
        }
    }
    validadorCampo() {
        if (this.state.userFeatures.nombre != '' && this.state.userFeatures.password != '' && this.state.userFeatures.representante != '' && this.state.userFeatures.TipoLocal_idTipoLocal != '' && this.state.userFeatures.ciudad_idCiudad != '') {
            return true;
        } else {
            showDangerMsg('No debe existir campos vacios','Campos erróneos');
            return false;
        }
    }

    createLocal = async () => {
        if (!this.state.checked) {
           
            showDangerMsg('Acepta los términos de uso','Error');

            return;
        }

        try {
            if (this.validador()) {
                this.setState({ loadingBtn: true });
                this.state.userFeatures.localtype = this.state.tipo;
                this.state.userFeatures.url = this.state.data.url;
                this.state.userFeatures.tocken = 0;

                if (this.state.tipo) {
                    this.state.userFeatures.localtype = 0;
                } else {
                    this.state.userFeatures.localtype = 1;
                }
                this.state.userFeatures.latitud = this.state.data.latitud;
                this.state.userFeatures.longitud = this.state.data.longitud;
                this.mecanicoApi.createLocal(this.state.userFeatures).then(res => {
                    if (res.idLocal) {
                        AsyncStorage.setItem(Constants.STORE_ID_USER, JSON.stringify(res.idLocal));
                        AsyncStorage.setItem(Constants.STORE_ID_TOKEN, JSON.stringify(res.tocken));
                        this.setState({ loadingBtn: false });
                        this.props.navigation.replace('Novedadesmes', { local: res })

                    } else {
                        if (res.status && res.status == 'error') {
                            showDangerMsg(res.message,'Error');

                            this.setState({ loadingBtn: false });
                        }
                    }
                });
            }
        } catch (err) {
            showDangerMsg(err.message,'Error');
        }
    };


    validateEmail = (email) => {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (!re.test(email)) {
            showDangerMsg('Ingresa un correo válido','Correo Erróneo',);
        }
        return re.test(email);
    };
    validateCedula = (cedula) => {
        var cedula = cedula;
        //Preguntamos si la cedula consta de 10 digitos
        if (cedula.length == 10) {

            //Obtenemos el digito de la region que sonlos dos primeros digitos
            var digito_region = cedula.substring(0, 2);

            //Pregunto si la region existe ecuador se divide en 24 regiones
            if (digito_region >= 1 && digito_region <= 24) {

                // Extraigo el ultimo digito
                var ultimo_digito = cedula.substring(9, 10);

                //Agrupo todos los pares y los sumo
                var pares = parseInt(cedula.substring(1, 2)) + parseInt(cedula.substring(3, 4)) + parseInt(cedula.substring(5, 6)) + parseInt(cedula.substring(7, 8));

                //Agrupo los impares, los multiplico por un factor de 2, si la resultante es > que 9 le restamos el 9 a la resultante
                var numero1 = cedula.substring(0, 1);
                var numero1 = (numero1 * 2);
                if (numero1 > 9) { var numero1 = (numero1 - 9); }

                var numero3 = cedula.substring(2, 3);
                var numero3 = (numero3 * 2);
                if (numero3 > 9) { var numero3 = (numero3 - 9); }

                var numero5 = cedula.substring(4, 5);
                var numero5 = (numero5 * 2);
                if (numero5 > 9) { var numero5 = (numero5 - 9); }

                var numero7 = cedula.substring(6, 7);
                var numero7 = (numero7 * 2);
                if (numero7 > 9) { var numero7 = (numero7 - 9); }

                var numero9 = cedula.substring(8, 9);
                var numero9 = (numero9 * 2);
                if (numero9 > 9) { var numero9 = (numero9 - 9); }

                var impares = numero1 + numero3 + numero5 + numero7 + numero9;

                //Suma total
                var suma_total = (pares + impares);

                //extraemos el primero digito
                var primer_digito_suma = String(suma_total).substring(0, 1);

                //Obtenemos la decena inmediata
                var decena = (parseInt(primer_digito_suma) + 1) * 10;

                //Obtenemos la resta de la decena inmediata - la suma_total esto nos da el digito validador
                var digito_validador = decena - suma_total;

                //Si el digito validador es = a 10 toma el valor de 0
                if (digito_validador == 10)
                    var digito_validador = 0;

                //Validamos que el digito validador sea igual al de la cedula
                if (digito_validador == ultimo_digito) {
                    return true;
                } else {
                    showDangerMsg( 'la cedula:' + cedula + ' es incorrecta','Cédula errónea',);

                    return false;

                }

            } else {
                // imprimimos en consola si la region no pertenece
                showDangerMsg( 'Esta cedula no pertenece a ninguna region','Cédula errónea',);

                return false;

            }
        } else {
            //imprimimos en consola si la cedula tiene mas o menos de 10 digitos
            showDangerMsg( 'Esta cedula tiene menos de 10 Digitos','Cédula errónea',);

            return false;

        }
    }
    terminos() {
        if (this.state.checked) {
            this.setState({ checked: !this.state.checked })

        } else {
            this.setState({ checked: !this.state.checked })

            this.props.navigation.navigate('Terminos')
        }

    }
    render() {
        if (this.state.loading) {
            return <LoadingProgress />;
        }
        if (!this.state.loading) {
            return (
                <View>
                    <Header
                        leftComponent={<TouchableOpacity onPress={() => {
                            this.props.navigation.goBack();
                        }}>
                            <Icon name="arrow-left" type="Ionicons" size={30} color="#fff" />
                        </TouchableOpacity>}
                        centerComponent={{ text: 'REGISTRO DE UBICACIÓN', style: { color: '#fff' } }}

                            containerStyle={{
                            justifyContent: 'space-around',
                            backgroundColor: '#FF9100'
                        }}
                    />
                    <StatusBar backgroundColor='#FF9100' barStyle="light-content" />
                    <ScrollView>
                        <View style={styles.container}>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <Image
                                    source={require('../../assets/png/lg.png')}
                                    style={styles.imagen} />
                                <View>
                                    <Text style={{ fontSize: 15, color: '#617792', fontWeight: 'bold', textAlign: 'center' }}>MECÁNICA EXPRESS</Text>
                                    <Text style={{ fontSize: 15, color: '#617792', fontWeight: 'bold', textAlign: 'center' }}>Registro</Text>
                                </View>
                            </View>
                            <View style={styles.ellanoteama}>
                                {this.state.tipo ? (
                                    <TextInput
                                        placeholder="Nombre de la empresa "
                                        onChangeText={val => this.updateStatusFeature('nombre', val)}
                                        style={styles.inputSearch}
                                        value={this.state.userFeatures.nombre}
                                    />
                                ) : (
                                        <TextInput
                                            placeholder="Nombre Freelance"
                                            onChangeText={val => this.updateStatusFeature('nombre', val)}
                                            style={styles.inputSearch}
                                            value={this.state.userFeatures.nombre}
                                        />

                                    )}

                                <TextInput
                                    placeholder="Cédula"
                                    onChangeText={val => this.updateStatusFeature('cedula', val)}
                                    style={styles.inputSearch}
                                    keyboardType={'phone-pad'}
                                    maxLength={10}
                                    value={this.state.userFeatures.cedula}
                                />
                                <TextInput
                                    placeholder="Dirección"
                                    onChangeText={val => this.updateStatusFeature('direccion', val)}
                                    style={styles.inputSearch}
                                    value={this.state.userFeatures.direccion}
                                />


                                <TextInput
                                    placeholder="Contraseña"
                                    secureTextEntry={true}
                                    onChangeText={val => this.updateStatusFeature('password', val)}
                                    style={styles.inputSearch}
                                    value={this.state.userFeatures.password}
                                />
                                <TextInput
                                    placeholder="Correo"
                                    style={styles.inputSearch}
                                    onChangeText={val => this.updateStatusFeature('correo', val.toLowerCase().trim())}
                                    value={this.state.userFeatures.correo}
                                />
                                <TextInput
                                    placeholder="Teléfono"
                                    secureTextEntry={true}
                                    style={styles.inputSearch}
                                    keyboardType={'phone-pad'}
                                    maxLength={10}
                                    onChangeText={val => this.updateStatusFeature('telefono', val)}
                                    value={this.state.userFeatures.telefono}
                                />
                                {this.state.tipo ? (
                                    <TextInput
                                        placeholder="Representante Legal"
                                        onChangeText={val => this.updateStatusFeature('representante', val)}
                                        style={styles.inputSearch}
                                        value={this.state.userFeatures.representante}
                                    />
                                ) : (
                                        <View></View>

                                    )}

                                {this.state.tipo ? (
                                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                        <NiftyText others={rnpickerStyles.labelStyle}>
                                            Local:</NiftyText>
                                        <View style={rnpickerStyles.pickerContainer}>
                                            <CustomPicker
                                                placeholder={'Seleccionar un tipo'}
                                                options={this.state.tiposlocal}
                                                getLabel={item => item.label}
                                                onValueChange={val => {
                                                    this.updateStatusFeature('TipoLocal_idTipoLocal', val ? val.value : null);
                                                }}
                                            />
                                        </View>
                                    </View>
                                ) : (
                                        <View></View>
                                    )}

                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                    <NiftyText others={rnpickerStyles.labelStyle}>
                                        Provincia:</NiftyText>
                                    <View style={rnpickerStyles.pickerContainer}>
                                        <CustomPicker
                                            placeholder={'Seleccionar una provincia'}
                                            options={this.state.provincias}
                                            getLabel={item => item.label}
                                            onValueChange={val => {
                                                this.getCiudades(val ? val.value : null);
                                            }}
                                        />
                                    </View>
                                </View>
                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                    <NiftyText others={rnpickerStyles.labelStyle}>Ciudad:</NiftyText>
                                    <View style={rnpickerStyles.pickerContainer}>
                                        <CustomPicker
                                            placeholder={'Seleccionar una ciudad'}
                                            options={this.state.ciudades}
                                            getLabel={item => item.label}
                                            onValueChange={val => {
                                                this.updateStatusFeature('ciudad_idCiudad', val ? val.value : null);
                                            }}
                                        />
                                    </View>
                                </View>
                                <View style={{ flexDirection: 'row' }}>

                                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Terminos')}>
                                        <Text style={{ alignItems: 'center', fontWeight: 'bold' }}>Acepto las Políticas de Privacidad{', '}</Text>
                                        <Text style={{ alignItems: 'center', fontWeight: 'bold' }}>Términos y Condiciones</Text>

                                    </TouchableOpacity>
                                    <CheckBox
                                        right
                                        checkedIcon="dot-circle-o"
                                        uncheckedIcon="circle-o"
                                        checkedColor="#502A18"
                                        checked={this.state.checked}
                                        onPress={() => this.terminos()}
                                    />
                                </View>

                                {this.renderBtn()}
                            </View>
                        </View>
                    </ScrollView>
                </View>

            );
        }

    }
    renderBtn() {
        if (!this.state.loadingBtn) {
            return (
                <View>
                    <TouchableOpacity
                        onPress={() => {
                            this.createLocal();
                        }}
                        style={styles.btnStyle}>
                        <Text style={{ fontSize: 14, textAlign: 'center', color: '#1F4997' }}>
                            GUARDAR
                        </Text>
                    </TouchableOpacity>

                </View>
            );
        } else {
            return (
                <TouchableOpacity style={styles.btnStyle}>
                    <ActivityIndicator size="small" color={'#1F4997'} />
                </TouchableOpacity>
            );
        }
    }
    getTiposLocal() {
        this.mecanicoApi.getTiposLocal().then(res => {
            this.setState({
                tiposlocal: res,
            });
            if (res != undefined) {
                this.setState({
                    loading: false,
                });
            }
        });
    }
    getProvincia() {
        this.mecanicoApi.getProvincias().then(res => {

            this.setState({
                provincias: res,
            });
            if (res != undefined) {
                this.setState({
                    loading: false,
                });
            }
        });
    }
    getCiudades(provinciaId) {
        this.mecanicoApi.getCiudades(provinciaId).then(res => {
            if (res.ciudades != undefined) {
                this.setState({
                    ciudades: res.ciudades,
                });
            } else {
                this.setState({
                    ciudades: [],
                });
            }
        });


    }
}

const rnpickerStyles = StyleSheet.create({
    labelStyle: { textAlign: 'left', minWidth: 60 },
    labelStyle2: { textAlign: 'center', minWidth: 60 },
    pickerContainer: {
        borderBottomWidth: 1,
        borderBottomColor: '#FFFF',
        flex: 1,
    },
});

const styles = StyleSheet.create({
    btnVerEnSitio: {
        backgroundColor: '#F5F6F7',
        borderRadius: 20,
        alignItems: 'center',
        width: '70%',
        padding: '3%',
        color: 'blue',
        borderWidth: 1,
        marginTop: '5%',
        borderColor: '#4982e6',
    },
    map: {
        width: 180,
        height: 180,
    },
    btnVerEnSitio3: {
        backgroundColor: '#F5F6F7',
        borderRadius: 20,
        alignItems: 'center',
        width: '90%',
        padding: '3%',
        color: 'blue',
        borderWidth: 1,
        marginTop: '5%',
        borderColor: '#4982e6',
    },
    btnVerEnSitio2: {
        backgroundColor: '#F5F6F7',
        borderRadius: 20,
        alignItems: 'center',
        width: '70%',
        padding: '3%',
        color: 'blue',
        borderWidth: 1,
        marginTop: '-10%',
        borderColor: '#4982e6',
    },
    descriptionTag: {
        color: '#502A18',
    },
    btnTextStyle: {
        color: '#FFFFFF',
        fontSize: 22,
        alignItems: 'center',
        textAlign: 'center',
    },
    btnStyle: {
        alignItems: 'center',
        borderWidth: 1,
        borderColor: '#4982e6',
        borderRadius: 20,
        justifyContent: 'center',
        backgroundColor: '#F5F6F7',
        height: 50,
        marginTop: 20,
        marginBottom: 80,

    },
    images: {
        width: 150,
        height: 150,
        borderColor: 'black',
        borderWidth: 1,
    },
    fotoContainer: {
        borderColor: '#424244',
        borderWidth: 1,
        borderStyle: 'dotted',
        backgroundColor: '#F9FCFD',
    },
    container: {
        flex: 1,
        alignItems: 'center',
        padding: 15,
        backgroundColor: '#fff',
    },
    ellanoteama: {
        minWidth: '80%',
        backgroundColor: '#fff',
    },
    createAccount: {
        fontSize: 30,
        textAlign: 'left',
        marginVertical: 15,
    },
    inputSearch: {
        borderBottomWidth: 1,
    },
    imagen: {
        width: 100,
        height: 100,

    },
    imagen2: {
        width: 155,
        height: 130,


    },
});
