import React, { Component } from 'react';
import { Text, Dimensions,StatusBar, Image, ScrollView, StyleSheet, TextInput, View, TouchableOpacity, ImageBackground, ActivityIndicator } from 'react-native';
import FeatherIcon from 'react-native-vector-icons/Feather';
import ImagePicker from 'react-native-image-picker';
import NiftyText from '../../common/NiftyText';
import { CustomPicker } from 'react-native-custom-picker';
import mecanicoApi from '../../api/mecanico.js';
import Icon from 'react-native-vector-icons/FontAwesome';

import {  Header } from 'react-native-elements';
import {showDangerMsg,showSuccesMsg} from '../../common/ShowMessages';

import Geolocation from '@react-native-community/geolocation';
import MapView from 'react-native-maps';

const { width, height } = Dimensions.get('window');

const ASPECT_RATIO = width / height;
const LATITUDE = -0.9752939;
const LONGITUDE = -78.70039;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO * 100;

export default class Productos extends Component {
    constructor(props) {
        super(props);
        this.mecanicoApi = new mecanicoApi();

        this.state = {
            login: true,
            registro: false,
            provincias: [],
            ciudades: [],
            imageFileData: '',
            loadingBtn: false,

            region: {
                latitude: LATITUDE,
                longitude: LONGITUDE,
                latitudeDelta: LATITUDE_DELTA,
                longitudeDelta: LONGITUDE_DELTA,
            },

            tiposlocal: [],
            arrFotos: [
            ],
            userFeatures: {
                idLocal: 0,
                nombre: '',
                calificacion: 1,
                latitud: 0,
                longitud: 0,
                ciudad_idCiudad: '',
                representante: '',
                telefono: '',
                correo: '',
                TipoLocal_idTipoLocal: '',
                estado: 0,
                password: '',
                cedula: '',
                url: '',

            },

        };
        this.getProvincia();
        this.getTiposLocal();


    }
    componentDidUpdate() {
    }

    getLocalidad() {
        Geolocation.getCurrentPosition(position => {
            this.updateStatusFeature('latitud', position.coords.latitude);
            this.updateStatusFeature('longitud', position.coords.longitude)
        });

    }

    onChangeText = (key, val) => {
        this.setState({ [key]: val });
    };

    updateStatusFeature(param, value) {
        let userFeatConst = this.state.userFeatures;
        userFeatConst[param] = value;
        this.setState({ userFeatures: userFeatConst });
    }
    loguin() {

    }
    registro() {
        this.setState({ login: false, registro: true });

    }
    home() {
    }
    createLocal = async () => {
        try {
            this.setState({ loadingBtn: true });
            this.state.userFeatures.url = this.state.imageFileData;
            this.mecanicoApi.createLocal(this.state.userFeatures).then(res => {
                if (res.idLocal) {
                    showSuccesMsg('Local creado','Bienvenido',);
                    this.setState({ loadingBtn: false });
                    this.props.navigation.navigate('Productos')
                } else {
                    if (res.status && res.status == 'error') {
                        showDangerMsg(res.message,'Error',);

                        this.setState({ loadingBtn: false });
                        this.setState({ login: true, registro: false });
                    }
                }
            });

        } catch (err) {
            showDangerMsg(err.message,'Error',);
        }
    };

    chooseImage(index) {
        let options = {
            title: 'Seleccionar imagen',
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
            cancelButtonTitle: 'Cancelar',
            takePhotoButtonTitle: 'Tomar Foto',
            chooseFromLibraryButtonTitle: 'Elegir Foto de la Galería',
            mediaType: 'photo',
            index: index,
        };
        ImagePicker.showImagePicker(options, response => {
            if (response.didCancel) {
            } else if (response.error) {
            } else if (response.customButton) {
            } else {
                let ministatus = 'data:image/jpg;base64,' + response.data;
                this.setState({
                    imageFileData: ministatus,
                });
            }
        });
    }



    renderFileData() {
        if (this.state.userFeatures.path) {
            return (
                <Image
                    source={{ uri: Constants.BASEURI + this.state.userFeatures.path }}
                    style={styles.images}
                />
            );
        }
        if (this.state.imageFileData) {
            return (
                <Image
                    source={{
                        uri: this.state.imageFileData,
                    }}
                    style={styles.images}
                />
            );
        } else {
            return (
                <FeatherIcon
                    name="camera"
                    size={50}
                    style={{ margin: 50 }}
                    color="#000"
                />
            );
        }
    }

    getTiposLocal() {
        this.mecanicoApi.getTiposLocal().then(res => {
            this.setState({
                tiposlocal: res,
            });
            if (res != undefined) {
                this.setState({
                    loading: false,
                });
            }
        });
    }
    getProvincia() {
        this.mecanicoApi.getProvincias().then(res => {

            this.setState({
                provincias: res,
            });
            if (res != undefined) {
                this.setState({
                    loading: false,
                });
            }
        });
    }
    getCiudades(provinciaId) {
        this.mecanicoApi.getCiudades(provinciaId).then(res => {
            if (res.ciudades != undefined) {
                this.setState({
                    ciudades: res.ciudades,
                });
            } else {
                this.setState({
                    ciudades: [],
                });
            }
        });
    }

    render() {
        if (this.state.login) {
            return (
                <View style={{ height: '100%', width: '100%' }} >
 <Header
                        leftComponent={<TouchableOpacity onPress={() => {
                            this.props.navigation.goBack();
                        }}>
                            <Icon name="arrow-left" type="Ionicons" size={30} color="#fff" />
                        </TouchableOpacity>}
                        centerComponent={{ text: 'REGISTRO DE NOVEDADES DEL MES', style: { color: '#fff' } }}

                                    containerStyle={{
                                    justifyContent: 'space-around',
                                    backgroundColor: '#FF9100'
                                }}
                    />
                    <StatusBar backgroundColor='#FF9100' barStyle="light-content" />
                <View style={styles.container}>
                    <View style={styles.contImage}>
                        <ImageBackground source={require('../../assets/png/lg.png')} style={{
                            width: 170,
                            height: 170,
                            marginTop: '10%',
                            borderWidth: 0
                        }}>
                            <TouchableOpacity
                                onPress={() =>
                                    this.props.navigation.navigate('MenuAuxilioMotos')
                                }
                                style={styles.btnImage}
                            >

                            </TouchableOpacity>
                        </ImageBackground>
                    </View>
                    <View>
                        <Text bold size={50} color="#32325D" style={{ textAlign: "center" }}>
                            MECÁNICA EXPRESS
                    </Text>
                        <Text bold size={50} color="#32325D" style={{ textAlign: "center" }}>
                            Proveedor
                    </Text>
                        <TextInput
                            placeholder="Nombre"
                            onChangeText={val => this.updateStatusFeature('nombre', val)}
                            style={styles.inputSearch}
                            value={this.state.userFeatures.nombre}
                        />
                        <TextInput
                            placeholder="Contraseña: Mínimo 6 caracteres"
                            secureTextEntry={true}
                            style={styles.inputSearch}
                            onChangeText={val => this.updateStatusFeature('password', val)}
                            value={this.state.userFeatures.password}
                        />

                    </View>
                    <TouchableOpacity style={styles.btnVerEnSitio}
                        onPress={() => this.loguin()}>
                        <Text style={{ fontSize: 14, textAlign: 'center', color: '#1F4997' }}>
                            Ingresar
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.btnVerEnSitio}
                        onPress={() => this.registro()}>
                        <Text style={{ fontSize: 14, textAlign: 'center', color: '#1F4997' }}>
                            Registrar
                        </Text>
                    </TouchableOpacity>
                </View >
                </View>

            );
        } if (this.state.registro) {
            return (
                <ScrollView>
                    <View style={styles.container}>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Image
                                source={require('../../assets/png/lg.png')}
                                style={styles.imagen}
                            />
                            <View>
                                <NiftyText others={rnpickerStyles.labelStyle}>
                                    MECÁNICA EXPRESS
              </NiftyText>
                                <NiftyText others={rnpickerStyles.labelStyle2}>
                                    Registro
              </NiftyText>
                            </View>
                        </View>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <View>
                                <TouchableOpacity
                                    style={[!this.state.imageFileData ? styles.fotoContainer : {}]}
                                    onPress={() => this.chooseImage()}>
                                    {this.renderFileData()}
                                </TouchableOpacity>
                                <NiftyText others={rnpickerStyles.labelStyle}>
                                Recuerda que la foto debe</NiftyText>
                                <NiftyText others={rnpickerStyles.labelStyle}>
                                ser panorámica, visible.</NiftyText>
                            </View>
                            <View>

                                <View style={styles.container}>

                                    <MapView
                                        style={styles.map}
                                        mapType={"standard"}
                                        followsUserLocation={true}
                                        showsUserLocation={true}
                                        showsMyLocationButton={true}
                                        initialRegion={{
                                            latitude: LATITUDE,
                                            longitude: LONGITUDE,
                                            latitudeDelta: LATITUDE_DELTA,
                                            longitudeDelta: LONGITUDE_DELTA,
                                        }}
                                        zoomEnabled={true}
                                    >
                                    </MapView>
                                    <NiftyText others={rnpickerStyles.labelStyle}>
                                Ubicacion de tu local</NiftyText>
                                <NiftyText others={rnpickerStyles.labelStyle}>
                                Mapa</NiftyText>
                                </View>
                            </View>


                        </View>


                        <View style={styles.ellanoteama}>

                            <TextInput
                                placeholder="Nombre de la empresa "
                                onChangeText={val => this.updateStatusFeature('nombre', val)}
                                style={styles.inputSearch}
                                value={this.state.userFeatures.nombre}
                            />
                            <TextInput
                                placeholder="Cédula o Ruc"
                                onChangeText={val => this.updateStatusFeature('cedula', val)}
                                style={styles.inputSearch}
                                keyboardType={'phone-pad'}
                                value={this.state.userFeatures.cedula}
                            />
                            <TextInput
                                placeholder="Contraseña"
                                onChangeText={val => this.updateStatusFeature('password', val)}
                                style={styles.inputSearch}
                                value={this.state.userFeatures.password}
                            />
                            <TextInput
                                placeholder="Correo"
                                style={styles.inputSearch}
                                onChangeText={val => this.updateStatusFeature('correo', val)}
                                value={this.state.userFeatures.correo}
                            />
                            <TextInput
                                placeholder="Teléfono"
                                secureTextEntry={true}
                                style={styles.inputSearch}
                                keyboardType={'phone-pad'}
                                onChangeText={val => this.updateStatusFeature('telefono', val)}
                                value={this.state.userFeatures.telefono}
                            />

                            <TextInput
                                placeholder="Representante Legal"
                                onChangeText={val => this.updateStatusFeature('representante', val)}
                                style={styles.inputSearch}
                                value={this.state.userFeatures.representante}
                            />


                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <NiftyText others={rnpickerStyles.labelStyle}>
                                    Tipos de Local:</NiftyText>
                                <View style={rnpickerStyles.pickerContainer}>
                                    <CustomPicker
                                        placeholder={'Seleccionar un tipo'}
                                        options={this.state.tiposlocal}
                                        getLabel={item => item.label}
                                        onValueChange={val => {
                                            this.updateStatusFeature('TipoLocal_idTipoLocal', val ? val.value : null);

                                        }}
                                    />


                                </View>
                            </View>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <NiftyText others={rnpickerStyles.labelStyle}>
                                    Provincia:</NiftyText>
                                <View style={rnpickerStyles.pickerContainer}>
                                    <CustomPicker
                                        placeholder={'Seleccionar una provincia'}
                                        options={this.state.provincias}
                                        getLabel={item => item.label}
                                        onValueChange={val => {
                                            this.getCiudades(val ? val.value : null);
                                        }}
                                    />
                                </View>
                            </View>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <NiftyText others={rnpickerStyles.labelStyle}>Ciudad:</NiftyText>
                                <View style={rnpickerStyles.pickerContainer}>
                                    <CustomPicker
                                        placeholder={'Seleccionar una ciudad'}
                                        options={this.state.ciudades}
                                        getLabel={item => item.label}
                                        onValueChange={val => {
                                            this.updateStatusFeature('ciudad_idCiudad', val ? val.value : null);
                                        }}
                                    />
                                </View>
                            </View>
                            {this.renderBtn()}


                        </View>
                    </View>
                </ScrollView>

            );
        }

    }
    renderBtn() {
        if (!this.state.loadingBtn) {
            return (
                <View>
                    <TouchableOpacity
                        onPress={() => {
                            this.createLocal();
                        }}
                        style={styles.btnStyle}>
                        <NiftyText others={styles.btnTextStyle}>GUARDAR</NiftyText>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => {
                        }}
                        style={styles.btnStyle}>
                        <NiftyText others={styles.btnTextStyle}>LOGIN</NiftyText>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.btnVerEnSitio3}
                        onPress={() => this.props.navigation.navigate('Productos', {
                            idlocal: 12,
                        })
                        }>
                    </TouchableOpacity>
                </View>
            );
        } else {
            return (
                <TouchableOpacity style={styles.btnStyle}>
                    <ActivityIndicator size="small" color={'#fff'} />
                </TouchableOpacity>
            );
        }
    }

}

const rnpickerStyles = StyleSheet.create({
    labelStyle: { textAlign: 'left', minWidth: 60 },
    labelStyle2: { textAlign: 'center', minWidth: 60 },
    pickerContainer: {
        borderBottomWidth: 1,
        borderBottomColor: '#FFFF',
        flex: 1,
    },
});

const styles = StyleSheet.create({
    btnVerEnSitio: {
        backgroundColor: '#F5F6F7',
        borderRadius: 20,
        alignItems: 'center',
        width: '70%',
        padding: '3%',
        color: 'blue',
        borderWidth: 1,
        marginTop: '5%',
        borderColor: '#4982e6',
    },
    map: {
        width: 180,
        height: 180,
    },
    btnVerEnSitio3: {
        backgroundColor: '#F5F6F7',
        borderRadius: 20,
        alignItems: 'center',
        width: '90%',
        padding: '3%',
        color: 'blue',
        borderWidth: 1,
        marginTop: '5%',
        borderColor: '#4982e6',
    },
    btnVerEnSitio2: {
        backgroundColor: '#F5F6F7',
        borderRadius: 20,
        alignItems: 'center',
        width: '70%',
        padding: '3%',
        color: 'blue',
        borderWidth: 1,
        marginTop: '-10%',
        borderColor: '#4982e6',
    },
    descriptionTag: {
        color: '#502A18',
    },
    btnTextStyle: {
        color: '#FFFFFF',
        fontSize: 22,
        alignItems: 'center',
        textAlign: 'center',
    },
    btnStyle: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#1F4997',
        height: 50,
        marginTop: 20,
        marginBottom: 80,
    },
    images: {
        width: 150,
        height: 150,
        borderColor: 'black',
        borderWidth: 1,
    },
    fotoContainer: {
        borderColor: '#424244',
        borderWidth: 1,
        borderStyle: 'dotted',
        backgroundColor: '#F9FCFD',
    },
    container: {
        flex: 1,
        alignItems: 'center',
        padding: 15,
        backgroundColor: '#fff',
    },
    ellanoteama: {
        minWidth: '80%',
        backgroundColor: '#fff',
    },
    createAccount: {
        fontSize: 30,
        textAlign: 'left',
        marginVertical: 15,
    },
    inputSearch: {
        borderBottomWidth: 1,
    },
    imagen: {
        width: 100,
        height: 100,


    },
    imagen2: {
        width: 155,
        height: 130,


    },
});
