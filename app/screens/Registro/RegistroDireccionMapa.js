import React, { Component } from 'react';
import { Text, StatusBar, Dimensions, Image,  StyleSheet, View, TouchableOpacity } from 'react-native';
import ImagePicker from 'react-native-image-picker';
import mecanicoApi from '../../api/mecanico.js';
import LoadingProgress from '../../common/LoadingProgress';
import Icon from 'react-native-vector-icons/FontAwesome';
import {showDangerMsg,showSuccesMsg} from '../../common/ShowMessages';

import {  Header } from 'react-native-elements';
var { height, width } = Dimensions.get('window');

import Geolocation from '@react-native-community/geolocation';
import MapView, { Callout, Marker, ProviderPropType } from 'react-native-maps';

export default class RegistroDireccionMapa extends Component {
    constructor(props) {
        super(props);
        this.mecanicoApi = new mecanicoApi();

        this.state = {
            loading: true,

            imageFileData: '',
            loadingBtn: false,
            tipo: this.props.route.params.tipo,
            poi: {
                latitude: '',
                longitude: '',
            },
            region: {
                latitude: '',
                longitude: '',
                latitudeDelta: 0,
                longitudeDelta: 0.005,
            },
            userFeatures: {
                latitud: 0,
                longitud: 0,
                url: '',
            },

        };
        this.onPoiClick = this.onPoiClick.bind(this);
        this.getLocationUser();

    }
    onPoiClick(e) {
        const poi = e.nativeEvent.coordinate;
        this.updateStatusFeature('latitud', poi.latitude);
        this.updateStatusFeature('longitud', poi.longitude);
        this.setState({
            poi,
        });
    }
    getLocationUser = async () => {
        await Geolocation.getCurrentPosition(
            position => {
                const { latitude, longitude } = position.coords;
                this.state.poi.latitude = position.coords.latitude;
                this.state.poi.longitude = position.coords.longitude;
                this.setState({
                    region: {
                        longitude: longitude,
                        latitude: latitude,
                        latitudeDelta: 0.029213524352655895,
                        longitudeDelta: (width / height) * 0.029213524352655895,
                    },
                });
                this.setState({ loading: false });
            },
        );
    };



    onChangeText = (key, val) => {
        this.setState({ [key]: val });
    };

    updateStatusFeature(param, value) {
        let userFeatConst = this.state.userFeatures;
        userFeatConst[param] = value;
        this.setState({ userFeatures: userFeatConst });
    }

    chooseImage(index) {
        let options = {
            title: 'Seleccionar imagen',
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
            cancelButtonTitle: 'Cancelar',
            takePhotoButtonTitle: 'Tomar Foto',
            chooseFromLibraryButtonTitle: 'Elegir Foto de la Galería',
            mediaType: 'photo',
            index: index,
        };
        ImagePicker.showImagePicker(options, response => {
            if (response.didCancel) {
            } else if (response.error) {
            } else if (response.customButton) {
            } else {
                let ministatus = 'data:image/jpg;base64,' + response.data;
                this.setState({
                    imageFileData: ministatus,
                });
            }
        });
    }


    renderFileData() {
        if (this.state.userFeatures.path) {
            return (
                <Image
                    source={{ uri: Constants.BASEURI + this.state.userFeatures.path }}
                    style={[!this.state.tipo ? styles.imagesfrelance : styles.imagesfrelance]}

                />
            );
        }
        if (this.state.imageFileData) {
            return (
                <Image
                    source={{
                        uri: this.state.imageFileData,
                    }}
                    style={[!this.state.tipo ? styles.imagesfrelance : styles.imageslocal]}
                />
            );
        } else {
            return (
                <View>
                    {this.state.tipo ? (
                        <Image
                            source={require('../../assets/png/camera.png')}
                            style={{
                                width: 50,
                                marginLeft: '45%',
                                marginTop: '10%',

                                height: 50
                            }} />
                    ) : (
                            <Image
                                source={require('../../assets/png/camera.png')}
                                style={{
                                    width: 50,
                                    marginLeft: '30%',
                                    marginTop: '25%',

                                    height: 50
                                }} />)}
                </View>
            );
        }
    }

    gotFormulacio() {
        if (this.validadorCampo()) {
            if (this.state.poi) {
                this.updateStatusFeature('latitud', this.state.poi.latitude);
                this.updateStatusFeature('longitud', this.state.poi.longitude)
            }
            this.state.userFeatures.url = this.state.imageFileData;
            this.props.navigation.replace('RegistroFormularioMapa', { tipo: this.state.tipo, data: this.state.userFeatures })
        }


    };
    validadorCampo() {
        if (this.state.imageFileData != '') {
            return true;
        } else {
            showDangerMsg('Debes ingresar una fotografía','Fotografía',);
            return false;
        }
    }
    render() {
        if (this.state.loading) {
            return <LoadingProgress />;
        }
        if (!this.state.loading) {
            return (
                <View style={{ height: '100%', width: '100%' }} >
                    <Header
                        leftComponent={<TouchableOpacity onPress={() => {
                            this.props.navigation.goBack();
                        }}>
                            <Icon name="arrow-left" type="Ionicons" size={30} color="#fff" />
                        </TouchableOpacity>}
                        centerComponent={{ text: 'REGISTRO DE UBICACIÓN', style: { color: '#fff' } }}

                                    containerStyle={{
                                    justifyContent: 'space-around',
                                    backgroundColor: '#FF9100'
                                }}
                    />
                    <StatusBar backgroundColor='#FF9100' barStyle="light-content" />

                    <View style={styles.container}>

                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <View>
                                <Image
                                    source={require('../../assets/png/lg.png')}
                                    style={styles.imagen} />
                                <View>
                                    <Text style={{ fontSize: 15, color: '#617792', fontWeight: 'bold', textAlign: 'center' }}> MECÁNICA EXPRESS</Text>
                                </View>
                            </View>

                            <TouchableOpacity style={styles.btnVerEnSitio}
                                onPress={() => this.gotFormulacio()}>
                                <Text style={{ fontSize: 14, textAlign: 'center', color: '#1F4997' }}>
                                    CONTINUAR
                        </Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <View style={[!this.state.tipo ? styles.container : styles.container2]}>
                                <Text style={{ fontSize: 15, color: '#617792', fontWeight: 'bold', textAlign: 'center' }}>REGISTRO</Text>
                                <TouchableOpacity
                                    style={[!this.state.tipo ? styles.fotoContainerfree : styles.fotoContainerlocal]}
                                    onPress={() => this.chooseImage()}>
                                    {this.renderFileData()}
                                </TouchableOpacity>
                                {this.state.tipo ? (
                                    <Text style={{ fontSize: 17, color: '#617792', fontWeight: 'bold', textAlign: 'center' }}>Recuerda que la foto debe ser panorámica y visible.</Text>) : (
                                        <Text style={{ fontSize: 17, color: '#617792', fontWeight: 'bold', textAlign: 'center' }}>Foto de tu rostro</Text>
                                    )}

                            </View>
                        </View>

                        <View style={{
                            width: '100%',
                            height: '60%',
                        }}>
                            <MapView
                                style={styles.map}
                                mapType={"standard"}
                                zoomEnabled={true}
                                pitchEnabled={true}
                                showsUserLocation={true}
                                followsUserLocation={true}
                                showsCompass={true}
                                showsBuildings={true}
                                showsTraffic={true}
                                showsIndoors={true}
                                onPress={this.onPoiClick}
                                initialRegion={this.state.region}
                                zoomEnabled={true}
                            >
                                <Marker coordinate={this.state.poi}>
                                    <Callout>
                                        <View>
                                            <Text>Mi local</Text>
                                        </View>
                                    </Callout>
                                </Marker>
                            </MapView>
                            <Text style={{ fontSize: 15, color: '#617792', fontWeight: 'bold', textAlign: 'center' }}>Ubicación exacta de tu sede.</Text>

                        </View>

                    </View>
                </View>

            );
        }
    }

}

RegistroDireccionMapa.propTypes = {
    provider: ProviderPropType,
};


const styles = StyleSheet.create({
    btnVerEnSitio: {
        backgroundColor: '#F5F6F7',
        borderRadius: 20,
        alignItems: 'center',
        width: '35%',
        padding: '3%',
        color: 'blue',
        borderWidth: 1,
        marginTop: '5%',
        borderColor: '#4982e6',
    },
    map: {
        width: '100%',
        height: '70%',
    },
    btnVerEnSitio3: {
        backgroundColor: '#F5F6F7',
        borderRadius: 20,
        alignItems: 'center',
        width: '90%',
        padding: '3%',
        color: 'blue',
        borderWidth: 1,
        marginTop: '5%',
        borderColor: '#4982e6',
    },
    btnVerEnSitio2: {
        backgroundColor: '#F5F6F7',
        borderRadius: 20,
        alignItems: 'center',
        width: '70%',
        padding: '3%',
        color: 'blue',
        borderWidth: 1,
        marginTop: '-10%',
        borderColor: '#4982e6',
    },
    descriptionTag: {
        color: '#502A18',
    },
    btnTextStyle: {
        color: '#FFFFFF',
        fontSize: 22,
        alignItems: 'center',
        textAlign: 'center',
    },
    btnStyle: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#1F4997',
        height: 50,
        marginTop: 20,
        marginBottom: 80,
    },
    imagesfrelance: {
        width: 130,
        height: 130,
        borderColor: 'black',
        borderWidth: 1,

    },
    imageslocal: {
        width: '100%',
        height: 130,
        borderColor: 'black',
        borderWidth: 1,
    },
    fotoContainerlocal: {
        borderColor: '#424244',
        borderWidth: 1,
        width: '100%',
        height: 130,

        borderStyle: 'dotted',
        backgroundColor: '#F9FCFD',
    },
    fotoContainerfree: {
        width: 130,
        height: 130,
        borderColor: 'black',
        borderWidth: 1,

    },
    container: {
        flex: 1,
        alignItems: 'center',
        height: '100%',
        padding: '3%',
        backgroundColor: '#fff',
    },
    container2: {
        flex: 1,
        backgroundColor: '#fff',
    },
    ellanoteama: {
        minWidth: '80%',
        backgroundColor: '#fff',
    },
    createAccount: {
        fontSize: 30,
        textAlign: 'left',
        marginVertical: 15,
    },
    inputSearch: {
        borderBottomWidth: 1,
    },
    imagen: {
        width: 75,
        height: 75,
        marginLeft: '30%'


    },
    imagen2: {
        width: 155,
        height: 130,


    },
});
