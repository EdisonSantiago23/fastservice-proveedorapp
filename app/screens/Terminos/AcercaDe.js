import React, {Component} from 'react';
import {ActivityIndicator, StyleSheet, View} from 'react-native';
import {WebView} from 'react-native-webview';
import NiftyText from '../../common/NiftyText';
import HeaderProfile from '../../common/HeaderProfile';

export default class Contact extends Component {
  static navigationOptions = ({navigation}) => {
    title: 'Politicas';
  };

  constructor(props) {
    super(props);
    this.state = {
      loading: true,
    };
    // YellowBox.ignoreWarnings(['Encountered an error loading page']);
  }

  componentwillunmount() {
    this.setState({
      loading: true,
    });
  }

  ActivityIndicatorLoadingView = () => {
    return (
      <View
        style={{
          flex: 1,
          position: 'absolute',
          left: 0,
          right: 0,
          top: 0,
          bottom: 0,
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: 'rgba(52, 52, 52, 0.8)',
        }}>
        <ActivityIndicator color="#fff" />
        <NiftyText others={{color: '#fff'}}>
          Cargando contenido, espere...
        </NiftyText>
      </View>
    );
  };

  errorInternet = () => {
    this.setState({
      networkError: true,
    });
  };

  render() {
    if (this.state.networkError) {
      return (
        <View>
          <HeaderProfile
            back={false}
            tittle={this.props.route.params.title}
            navigation={this.props.navigation}
          />
          <View
            style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
            <NiftyText others={{margin: 20}}>
              Esta aplicacion necesita conexión a internet, por favor verifique
              e intente de nuevo.
            </NiftyText>
          </View>
        </View>
      );
    } else {
      return (
        <View style={{flex: 1}}>
          <HeaderProfile
            back={true}
            tittle={this.props.route.params.title}
            navigation={this.props.navigation}
          />
          <WebView
            onError={() => this.errorInternet()}
            style={{marginTop: 2}}
            source={{uri: this.props.route.params.link}}
            javaScriptEnabled={true}
            domStorageEnabled={true}
            renderLoading={this.ActivityIndicatorLoadingView}
            startInLoadingState={true}
          />
        </View>
      );
    }
  }
}

const style = StyleSheet.create({
  iconContainer: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    width: 80,
  },
  touchable: {
    flex: 1,
    flexDirection: 'row',
  },
});
