
import React, { Component,Fragment } from 'react';
import { StyleSheet, Alert,Header, ScrollView,SafeAreaView,StatusBar, TouchableOpacity, Dimensions, Image, Animated, View } from 'react-native';
import mecanicoApi from '../../../api/mecanico';
import LoadingProgress from '../../../common/LoadingProgress';
import { Block, Text, theme } from "galio-framework";
import AsyncStorage from '@react-native-community/async-storage';
import Constants from '../../../common/Constants';
import Geolocation from '@react-native-community/geolocation';
import MapView, { Callout, Marker, ProviderPropType } from 'react-native-maps';
var { height, width } = Dimensions.get('window');
import { Icon, Overlay } from 'react-native-elements'
import { TextInput } from 'react-native-paper'
import {
    SCLAlert,
    SCLAlertButton
} from 'react-native-scl-alert'
import Share from 'react-native-share';
import Feather from 'react-native-vector-icons/Feather';

export default class DetalleCotizar extends Component {
    constructor(props) {
        super(props);
        this.mecanicoApi = new mecanicoApi();

        this.state = {
            initSplash: true,
            loginSpl: true,
            loading: true,
            reservas: '',
            estado: 'Acpetado',
            detalle: this.props.route.params.detalle,
            visible:false,
            showfailid: false,

            poi: {
                latitude: '',
                longitude: '',
            },
            region: {
                latitude: '',
                longitude: '',
                latitudeDelta: 0,
                longitudeDelta: 0.005,
            },

        };
        this.RotateValueHolder = new Animated.Value(0);
        this.getLocalidad();
    }
    getLocalidad() {

        Geolocation.getCurrentPosition(position => {
            this.state.region.latitude = position.coords.latitude;
            this.state.region.longitude = position.coords.longitude;
            this.getReservas(position.coords.latitude, position.coords.longitude);

        });
    }
    getReservas = async (latitude, longitude) => {

        this.setState({
            reservas: this.state.detalle.acpetadas[0],
        });
        this.setState({ loading: false });

    }

    finalizarrepuesto () {

        
        try {
            this.setState({ loading: true });
            this.mecanicoApi.finalizarrepuesto(this.state.detalle.idReservaRepuesto).then(res => {
                if (res.idReservaRepuesto) {
                    this.setState({ loading: false });
                    Alert.alert('Repuesto','Finalizado', [{ text: 'Cerrar' }]);
                    this.props.navigation.navigate('MenuNotificaciones');

                } else {
                    if (res.status && res.status == 'error') {
                        Alert.alert('Error', res.message, [{ text: 'Cerrar' }]);
                        this.setState({ loading: false });
                    }
                }
            });

        } catch (err) {
            Alert.alert('Error', err.message, [{ text: 'Cerrar' }]);
        }

    }
    updateStatusFeature(param, value) {
        let userFeatConst = this.state.reservas;
        userFeatConst[param] = value;
        this.setState({ reservas: userFeatConst });
    }
    validarprecio(value) {
        var regex = /^[1-9]\d*(((,\d{3}){1})?(\.\d{0,2})?)$/;
           if(regex.test(value)){
            return true ;

        }else{
            this.setState({ showfailid: true,message:'Debes in gresar un precio válido' })

            return false ;

        }
    }
    validadorCampo() {
            if(this.validarprecio(this.state.reservas.precio)){
                if(this.state.reservas.descuento>=100||this.state.reservas.descuento<0){
                    this.setState({ showfailid: true,message:'Ingresa un descuento válido(0% al 100%)' })
                    return false;

                }else{
                    return true;

                }

            }else{
                return false;

            }
    }
       
    handleOpenFaild = () => {
        this.setState({ showfailid: true })
    }

    handleCloseFaild = () => {
        this.setState({ showfailid: false })

    }
    cambiarPass = async () => {
        try {
            if (this.validadorCampo()) {
                this.setState({ loading: true,visible: !this.state.visible });
                this.mecanicoApi.recotizar(this.state.reservas).then(res => {

                    if (res) {
                        this.setState({ show: true, menss: res.menssOk, loading: false, });
                        this.props.navigation.replace('Menu');


                    } else {
                        this.setState({ menss: res.message, loading: false, showerror: true })
                    }
                });
            }
        } catch (err) {
            this.setState({ menss: res.message, loading: false, showerror: true })
        }
    };
    estado = async (estado, cliente) => {

        try {
            this.setState({ loading: true });
            cliente.estado = estado;
            if (estado == 2) {
                this.state.estado = "Rechazado";
                cliente.content = "Mecánico " + cliente.nombremecanico + " no disponible";

            } else {
                this.state.estado = "Acpetada";
                cliente.content = "El mecánico " + cliente.nombremecanico + " ha aceptado";

            }
            this.mecanicoApi.respuestaLocal(cliente).then(res => {
                if (res.idReservaMecanico) {
                    this.getReservas();

                    Alert.alert('Emergencia', this.state.estado, [{ text: 'Cerrar' }]);
                    this.setState({ loading: false });

                } else {
                    if (res.status && res.status == 'error') {
                        Alert.alert('Error', res.message, [{ text: 'Cerrar' }]);
                        this.setState({ loading: false });
                    }
                }
            });

        } catch (err) {
            Alert.alert('Error', err.message, [{ text: 'Cerrar' }]);
        }


    }

    render() {
        if (this.state.loading) {
            return <LoadingProgress />;
        }
        if (!this.state.loading) {
            return (
                <View>
                         
                    <View style={{ flexDirection: 'row', alignItems: 'center', marginLeft: '30%' }}>
                        <View>
                            <Image
                                source={require('../../../assets/png/lg.png')}
                                style={styles.imagen} />
                            <View>
                                <Text style={{ fontSize: 18, color: '#617792', fontWeight: 'bold' }}> MECÁNICA EXPRESS</Text>
                            </View>
                        </View>

                    </View>
                    <SCLAlert
                    theme="danger"
                    show={this.state.showfailid}
                    title="Cotización"
                    onRequestClose={this.handleCloseFaild}
                    cancellable={true}
                    subtitle={this.state.message}>
                    <SCLAlertButton theme="danger" onPress={this.handleCloseFaild}>OK</SCLAlertButton>
                </SCLAlert>           
                    <View style={{ alignItems: 'center' }}>

                        <Overlay isVisible={this.state.visible}>
                            <View style={styles.container3}>
                                <Text style={styles.categoriesName2}>Recotiza tu oferta</Text>
                                <Text style={{ fontSize: 15, color: 'red', textAlign: 'center' }}>Ingresa los nuevos datos del repuesto</Text>

                                <View >
                                    <Fragment>
                                        <SafeAreaView>
                                            <View style={styles.container3}>
              
                                                <TextInput mode="outlined"
                                                    ref='zipCode'
                                                    label="Precio"
                                                    theme={{
                                                        colors: {
                                                            primary: 'purple',
                                                            background: 'white'
                                                        }
                                                    }}
                                                    value={this.state.reservas.precio.toString()}
                                                    onChangeText={val => this.updateStatusFeature('precio', parseFloat(val))}
                                                    keyboardType={'numeric'}

                                                />
                                                <TextInput mode="outlined"
                                                    ref='zipCode'
                                                    label="Stock"
                                                    theme={{
                                                        colors: {
                                                            primary: 'purple',
                                                            background: 'white'
                                                        }
                                                    }}
                                                    value={this.state.reservas.stock.toString()}
                                                    onChangeText={val => this.updateStatusFeature('stock', parseFloat(val).toFixed(0))}
                                                    keyboardType={'numeric'}

                                                    returnKeyType='done'
                                                />
                                                <TextInput mode="outlined"
                                                    ref='zipCode'
                                                    label="Descuento"
                                                    theme={{
                                                        colors: {
                                                            primary: 'purple',
                                                            background: 'white'
                                                        }
                                                    }}
                                                    value={this.state.reservas.descuento.toString()}
                                                    onChangeText={val => this.updateStatusFeature('descuento',parseFloat(val).toFixed(0))}
                                                    keyboardType={'numeric'}

                                                    returnKeyType='done'
                                                />
                                            </View>
                                        </SafeAreaView>
                                    </Fragment>
                                </View>

                            </View>

                            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <Icon
                                    raised
                                    name='times'
                                    type='font-awesome'
                                    color='#f50'
                                    onPress={() => this.setState({ visible: !this.state.visible })} />
                                <Icon
                                    raised
                                    name='check'
                                    type='font-awesome'
                                    color='green'
                                    onPress={() => this.cambiarPass()} />
                       
                            </View>
                        </Overlay>
                    </View>

                    <ScrollView style={{ height: '80%',width:'100%' }}>

                        <View style={styles.container}>
                                <View>
                                        <View style={styles.contImage}>
                                            <Block flex style={styles.profileCard}>

                                                <Block middle style={styles.titulo}>
                                                    <Text style={{ fontSize: 20, color: '#617792', fontWeight: 'bold' }}>{this.state.reservas.nombrerespuesta}</Text>
                                                </Block>
                                                {this.renderBtn(this.state.reservas)}
                                                <Block middle >
                                                    <Image
                                                        source={{ uri:Constants.BASEURI + this.state.reservas.url }}
                                                        style={styles.avatar}
                                                    />
                                                    
                                                </Block>

                                                <Block middle style={styles.titulo}>

                                                    <MapView
                                                       style={styles.map}
                                                       mapType={"standard"}
                                                       initialCamera={{
                                                           center: {
                                                               latitude: this.state.detalle.poi[0].latitude,
                                                               longitude: this.state.detalle.poi[0].longitude,
                                                           },
                                                           pitch: 45,
                                                           heading: 90,
                                                           altitude: 1000,
                                                           zoom: 17,
                                                       }}
                                                       loadingEnabled
                                                       loadingIndicatorColor="#666666"
                                                       loadingBackgroundColor="#eeeeee"
                                                       showsIndoors
                                                       showsIndoorLevelPicker
                                                       zoomEnabled={true}
                                                       pitchEnabled={true}
                                                       showsUserLocation={true}
                                                       followsUserLocation={true}
                                                       showsCompass={true}
                                                       showsBuildings={true}
                                                       showsTraffic={true}
                                                       showsIndoors={true}
                                                       initialRegion={this.state.region}
                                                       zoomEnabled={true}>

                                                        <Marker coordinate={this.state.detalle.poi[0]}>
                                                            <Callout>
                                                                <View>
                                                                    <Text>Cliente</Text>
                                                                </View>
                                                            </Callout>
                                                        </Marker>
                                                    </MapView>
                                                    <Text
                                                            bold
                                                            color="#525F7F"
                                                            size={16}
                                                            style={{ marginBottom: 4 }}
                                                        >
                                                            {this.state.reservas.direccionenvio}

                                                        </Text>

                                                </Block>


                                                <Block style={styles.info}>
                                      


                                                <Block row space="between">
                                                <Block middle>
                                                {this.state.reservas.trasporte == 1 ? (
                                                              <Text
                                                              bold
                                                              color="#525F7F"
                                                              size={16}
                                                              style={{ marginBottom: 4 }}
                                                          >
                                                              SI
  
                                                          </Text>
                                                    ):(
                                                        <Text
                                                        bold
                                                        color="#525F7F"
                                                        size={16}
                                                        style={{ marginBottom: 4 }}
                                                    >
NO
                                                    </Text>
                                                    )}
                                                        <Text size={16}>Uber</Text>
                                                    </Block>
                                                    <Block middle>
                                                        <Text bold
                                                            size={16}
                                                            color="#525F7F"
                                                            style={{ marginBottom: 4 }}
                                                        >
                                                            {this.state.reservas.cantidad}
                                                        </Text>
                                                        <Text size={16}>Cantidad</Text>
                                                    </Block>
                                     
                                                    <Block middle>
                                                        <Text
                                                            bold
                                                            color="#525F7F"
                                                            size={16}
                                                            style={{ marginBottom: 4 }}
                                                        >
                                                            {this.state.reservas.descuento}%

                                                        </Text>

                                                        <Text size={16}>Descuento</Text>
                                                    </Block>
                                                    <Block middle>
                                                        <Text
                                                            bold
                                                            color="#525F7F"
                                                            size={16}
                                                            style={{ marginBottom: 4 }}
                                                        >
                                                            ${(this.state.reservas.precio-(this.state.reservas.precio*this.state.reservas.descuento/100))*(this.state.reservas.cantidad)}

                                                        </Text>

                                                        <Text size={16}>Total</Text>
                                                    </Block>
                                      
                                              
                                                </Block>
                                            </Block>

                                  
                         
                                            </Block>

                                        </View>
                             
                                </View>
                        </View>

                    </ScrollView>

                </View>

            );
        }
    }

        


    renderBtn(cliente) {
        if (cliente.estado == 0) {
            if (cliente.cotizado == 0) {
                return (
                    <View style={styles.containerdtn}>
                                    <TouchableOpacity style={styles.btnVerEnSitio}
                                                                            onPress={() =>this.setState({ visible: !this.state.visible })                                                                           }>
                                                                            <Text style={{ fontSize: 14, textAlign: 'center', color: '#1F4997' }}>RE COTIZAR PRODUCTO </Text></TouchableOpacity>
                    </View>
                );
            }
         
           
        }
        if (cliente.estado == 1) {
            if(cliente.trasporte==1){
                return (
                    <View style={styles.containerdtn}>
                                  <TouchableOpacity style={styles.btnVerEnSitioL2}
                         onPress={() => {
                             const shareOptions = {
                                 title: 'Compartir producto vía',
                                 message: 'El cliente:' + this.state.detalle.nommbreusuario + ' se encuentra en ',
                                 url:
                                     'https://maps.google.com/?q=' + this.state.detalle.poi[0].latitude + ',' + this.state.detalle.poi[0].longitude
                             };
                             Share.open(shareOptions);
                         }}>
                         <Feather
                             name="share-2"
                             size={20}/>
                         <Text>Compartir Ubicación</Text>
                     </TouchableOpacity>
                    </View>
                );
            }

        }
    }
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        width: '100%'
    },
    containerdtn: {
        bottom: 0,
        alignItems: 'center',
        flexDirection: 'row',
    },
    btnVerEnSitioL: {
        backgroundColor: '#32DC36',
        borderRadius: 20,
        alignItems: 'center',
        width: '50%',
        padding: '3%',
        color: 'blue',
        borderWidth: 1,
        marginTop: '5%',
        borderColor: '#4982e6',

    },
    container3: {
        width:'100%',
        flexDirection: 'column',
        justifyContent: 'space-around',
        alignItems: 'stretch',
      },
    title: {
        fontFamily: 'vagron',
        fontSize: 24,
        color: '#1F4997',
        backgroundColor: 'transparent',
        textAlign: 'center',
        marginVertical: 5,
    },
    text: {
        color: '#73221C',
        backgroundColor: 'transparent',
        textAlign: 'center',
        fontSize: 18,
        paddingHorizontal: 10,
        marginBottom: 10,
    },
    btnVerEnSitioR: {

        backgroundColor: '#FF333C',
        borderRadius: 20,
        alignItems: 'center',
        width: '50%',
        padding: '3%',
        color: 'blue',
        borderWidth: 1,
        marginTop: '5%',
        borderColor: '#4982e6',

    },

    profile: {
        flex: 1
    },
    imagen: {
        width: 75,
        height: 75,
        marginLeft: '20%'

    },
    contImage: {
        flex: 1,
        marginTop: 3,
        justifyContent: 'center',
        borderWidth: 2,
        width: '90%',
        height:  height- 200,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
    },
    map: {
        width: width-20,
        height: '50%'
    },

    btnVerEnSitio: {
        backgroundColor: '#F5F6F7',
        borderRadius: 20,
        width: '100%',
        padding: '3%',
        color: 'blue',
        borderWidth: 1,
        marginTop: '5%',
        borderColor: '#4982e6',
    },

    button: {
        alignSelf: 'center',

        fontSize: 16,
        color: '#fff',
        backgroundColor: '#617792',
        paddingVertical: 10,
        paddingHorizontal: 15,
        textAlign: "center",
        height: 50,
        width: 350,
        padding: 50,
        borderRadius: 24
    },

    profile: {
        flex: 1
    },
    profileCard: {

        marginTop: 65,
        borderTopLeftRadius: 6,
        borderTopRightRadius: 6,
        backgroundColor: 1,
        shadowColor: "black",
        shadowOffset: { 200: 0, 200: 0 },
        shadowRadius: 8,
        shadowOpacity: 0.2,
    },
    btnVerEnSitioL2: {
        backgroundColor: '#32DC36',
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        padding: '3%',
        color: 'blue',
        borderWidth: 1,
        marginTop: '5%',
        borderColor: '#4982e6',
        flexDirection: 'row',

    },
    titulo: {
        marginTop: -70
    },

    avatar: {
        width: 270,
        height: 170,
        borderWidth: 0,
        marginTop:'5%'

    },
    nameInfo: {
        marginTop: -50
    },

});
