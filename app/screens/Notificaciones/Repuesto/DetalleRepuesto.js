import React, { Component } from 'react';
import { StyleSheet, Alert, ScrollView, TouchableOpacity, Dimensions, Image, Animated, View } from 'react-native';
import mecanicoApi from '../../../api/mecanico';
import LoadingProgress from '../../../common/LoadingProgress';
import { Block, Text, theme } from "galio-framework";
import AsyncStorage from '@react-native-community/async-storage';
import Constants from '../../../common/Constants';
import Geolocation from '@react-native-community/geolocation';
import MapView, { Callout, Marker, ProviderPropType } from 'react-native-maps';
import { Overlay, Header } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import LinearGradient from 'react-native-linear-gradient'; // Only if no expo
import { List, ListItem } from 'react-native-elements'
import {
    SCLAlert,
    SCLAlertButton
} from 'react-native-scl-alert'
var { height, width } = Dimensions.get('window');

export default class DetalleRepuesto extends Component {
    constructor(props) {
        super(props);
        this.mecanicoApi = new mecanicoApi();

        this.state = {
            initSplash: true,
            loginSpl: true,
            loading: true,
            reservas: [],
            estado: 'Acpetado',
            show: false,
            localtype: this.props.route.params.localtype,
            poi: {
                latitude: '',
                longitude: '',
            },
            region: {
                latitude: 0,
                longitude: 0,
                latitudeDelta: 0,
                longitudeDelta: 0.04,
            },

        };
        this.RotateValueHolder = new Animated.Value(0);
        this.getLocationUser();
    
    }
    getLocationUser = async () => {
        await Geolocation.getCurrentPosition(
            position => {
                const { latitude, longitude } = position.coords;
                this.state.poi.latitude = position.coords.latitude;
                this.state.poi.longitude = position.coords.longitude;
                this.setState({
                    region: {
                        longitude: longitude,
                        latitude: latitude,
                        latitudeDelta: 0.029213524352655895,
                        longitudeDelta: (width / height) * 0.029213524352655895,
                    },
                });
                this.getReservas(position.coords.latitude, position.coords.longitude);
            }
        );
    };


    getReservas = async (latitude, longitude) => {
        this.setState({ loading: true });
        this.mecanicoApi.GetRepuestos(latitude, longitude, await AsyncStorage.getItem(Constants.STORE_ID_USER)).then(res => {
            if (res.status) {
                this.setState({ show: true, loading: false, })

            }
            if (res.status) {
                this.setState({ show: true, loading: false, })

            }
            this.setState({
                reservas: res,
            });
            if (res != undefined) {
                this.setState({ loading: false });

            }
        });
    }
    handleClose = () => {
        this.setState({ show: false })
        this.props.navigation.replace('Menu');

    }
    estado = async (estado, cliente) => {

        try {
            this.setState({ loading: true });
            cliente.estado = estado;
            if (estado == 2) {
                this.state.estado = "Rechazado";
                cliente.content = "Mecánico " + cliente.nombremecanico + " no disponible";

            } else {
                this.state.estado = "Acpetada";
                cliente.content = "El mecánico " + cliente.nombremecanico + " ha aceptado";

            }
            this.mecanicoApi.respuestaLocal(cliente).then(res => {
                if (res.idReservaMecanico) {
                    this.getReservas();

                    Alert.alert('Emergencia', this.state.estado, [{ text: 'Cerrar' }]);
                    this.setState({ loading: false });

                } else {
                    if (res.status && res.status == 'error') {
                        Alert.alert('Error', res.message, [{ text: 'Cerrar' }]);
                        this.setState({ loading: false });
                    }
                }
            });

        } catch (err) {
            Alert.alert('Error', err.message, [{ text: 'Cerrar' }]);
        }


    }

    render() {
        if (this.state.loading) {
            return <LoadingProgress />;
        }
        if (!this.state.loading) {
            if (!this.state.reservas.status) {
                return (
                    <View>
                        <Header
                            leftComponent={<TouchableOpacity onPress={() => {
                                this.props.navigation.goBack();
                            }}>
                                <Icon name="arrow-left" type="Ionicons" size={30} color="#fff" />
                            </TouchableOpacity>}
                            centerComponent={{ text: 'REPUESTOS', style: { color: '#fff' } }}

                            rightComponent={<TouchableOpacity onPress={() => {
                                this.props.navigation.navigate('Menu');

                            }}>
                                <Icon name="home" type="Ionicons" size={30} color="#fff" />
                            </TouchableOpacity>}
                        />
                        <View style={{ flexDirection: 'row', alignItems: 'center', marginLeft: '30%' }}>
                            <View>
                                <Image
                        source={require('../../../assets/png/lg.png')}
                        style={styles.imagen} />
                                <View>
                                    <Text style={{ fontSize: 18, color: '#617792', fontWeight: 'bold' }}> MECÁNICA EXPRESS</Text>
                                </View>
                            </View>

                        </View>

                        <ScrollView style={{ height: '75%', width: '100%' }}>
                            <View>
                                {this.state.reservas.map(novedad => (
                                    <ListItem
                                        style={{
                                            paddingTop: '1%',
                                        }}
                                        badge={novedad.distancia >= 1 ? (
                                            {
                                                value: novedad.distancia.toFixed(0)+' Km',
                                                textStyle: { color: 'white', fontWeight: 'bold', fontSize: 15 }, containerStyle: { color: 'orange' }
                                            }
                                        ) : (
                                                {
                                                    value: (novedad.distancia * 1000 ).toFixed(0)+ ' m',
                                                    textStyle: { color: 'white', fontWeight: 'bold', fontSize: 15 }, containerStyle: { color: 'orange' }
                                                }
                                            )}
                                        friction={90} //
                                        tension={100} // These props are passed to the parent component (here TouchableScale)
                                        activeScale={0.95} //
                                        linearGradientProps={novedad.estado==0 ?(
                                            {
                                                colors: ['#FFE7E7', '#FF0000'],
                                                start: { x: 1, y: 0 },
                                                end: { x: 0.2, y: 0 },
                                            }
                                        ):(
                                            {
                                                colors: ['#FFFFFF', '#00FF09'],
                                                start: { x: 1, y: 0 },
                                                end: { x: 0.2, y: 0 },
                                            }
                                        )}
                                        ViewComponent={LinearGradient} // Only if no expo
                                        leftAvatar={{ rounded: true, source: { uri: Constants.BASEURI + novedad.imagenRepuesto } }}
                                        onPress={() =>this.props.navigation.navigate('NotificacionesRepuestos', {reservas:novedad })}
                                        title={novedad.detalle +' ('+ novedad.marca+')'}
                                        titleStyle={{ color: 'white', fontWeight: 'bold' }}
                                        subtitle={'Cliente: '+novedad.nombreUsuario}
                                        subtitleStyle={{ color: 'white' }}
                                        bottomDivider
                                        chevron={{ color: '#617792' }}
                                    />

                                ))}
                            </View>
                        </ScrollView>

                    </View>

                )
            } else {
                return (
                    <View style={styles.container}>
                        <SCLAlert
                            theme="danger"
                            show={this.state.show}
                            title="Repuestos"
                            onRequestClose={this.handleClose}
                            cancellable={true}
                            subtitle="Actualmente no tienes repuestos por atender"
                        >
                            <SCLAlertButton theme="danger" onPress={this.handleClose}>Atras</SCLAlertButton>
                        </SCLAlert>
                    </View>
                );

            }
        }
    }
    renderBtn(cliente) {
        if (cliente.estado == 0) {
            return (
                <View style={styles.containerdtn}>
                    <TouchableOpacity style={styles.btnVerEnSitioL}
                        onPress={() => this.estado(1, cliente)}>
                        <Text style={{ textAlign: 'center' }}>
                            ACEPTAR
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.btnVerEnSitioR}
                        onPress={() => this.estado(2, cliente)}>
                        <Text style={{ textAlign: 'center' }}>
                            RECHAZAR
                        </Text>
                    </TouchableOpacity>

                </View>
            );
        }
        if (cliente.estado == 1) {
            return (
                <View >
                    <Text style={{ fontSize: 15, color: '#35DC32', fontWeight: 'bold', textAlign: 'center' }}> En espera de confirmacion de {cliente.nommbreusuario}
                    </Text>

                </View>
            );
        }
        if (cliente.estado == 3) {
            return (
                <View >
                    <Text style={{ fontSize: 15, color: '#3243DC', fontWeight: 'bold', textAlign: 'center' }}> {cliente.nommbreusuario} confirmo el mecanico
                    </Text>
                    <Text style={{ fontSize: 15, color: '#3243DC', fontWeight: 'bold', textAlign: 'center' }}>  Dirigete al destino
                    </Text>
                </View>
            );
        }
    }
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        width: '100%'
    },
    containerdtn: {
        bottom: 0,
        alignItems: 'center',
        flexDirection: 'row'

    },
    btnVerEnSitioL: {
        backgroundColor: '#32DC36',
        borderRadius: 20,
        alignItems: 'center',
        width: '50%',
        padding: '3%',
        color: 'blue',
        borderWidth: 1,
        marginTop: '5%',
        borderColor: '#4982e6',

    },
    btnVerEnSitioR: {

        backgroundColor: '#FF333C',
        borderRadius: 20,
        alignItems: 'center',
        width: '50%',
        padding: '3%',
        color: 'blue',
        borderWidth: 1,
        marginTop: '5%',
        borderColor: '#4982e6',

    },


    imagen: {
        width: 75,
        height: 75,
        marginLeft: '20%'

    },
    contImage: {
        flex: 1,
        marginTop: 3,
        justifyContent: 'center',
        width: '100%',
        height: 560,
        borderWidth: 2,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
    },
    map: {
        width: '90%',
        marginTop: '15%',
        height: '60%',
    },
    btnVerEnSitio: {
        backgroundColor: '#32DC36',
        borderRadius: 20,
        alignItems: 'center',
        width: '100%',
        height: '40%',
        alignContent: 'center',
        padding: '3%',
        color: 'blue',
        borderWidth: 1,
        borderColor: '#4982e6',
    },

    btnVerEnSitiol: {
        backgroundColor: '#F5F6F7',
        borderRadius: 20,
        alignItems: 'center',
        width: '70%',
        padding: '3%',
        color: 'blue',
        borderWidth: 1,
        marginTop: '5%',
        borderColor: '#4982e6',
    },


    button: {
        alignSelf: 'center',

        fontSize: 16,
        color: '#fff',
        backgroundColor: '#617792',
        paddingVertical: 10,
        paddingHorizontal: 15,
        textAlign: "center",
        height: 50,
        width: 350,
        padding: 50,
        borderRadius: 24
    },

    profile: {
        flex: 1
    },

    profileCard: {
        position: "relative",

        marginTop: 65,
        borderTopLeftRadius: 6,
        borderTopRightRadius: 6,
        backgroundColor: 1,
        shadowColor: "black",
        shadowOffset: { 200: 0, 200: 0 },
        shadowRadius: 8,
        shadowOpacity: 0.2,
    },

    titulo: {
        marginTop: -70
    },
    titulomapas: {
        marginTop: -190
    },

    avatar: {
        width: '50%',
        height: '50%',
        marginTop: '-20%',
    },
    nameInfo: {
        marginTop: -50
    },
    divider: {
        width: "90%",
        borderWidth: 1,
        borderColor: "#E9ECEF"
    },
});
