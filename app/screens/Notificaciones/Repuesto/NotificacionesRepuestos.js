
import React, { Component } from 'react';
import { StyleSheet, Alert, ScrollView, TouchableOpacity, Dimensions, Image, Animated, View } from 'react-native';
import mecanicoApi from '../../../api/mecanico';
import LoadingProgress from '../../../common/LoadingProgress';
import { Block, Text, theme } from "galio-framework";
import AsyncStorage from '@react-native-community/async-storage';
import Constants from '../../../common/Constants';
import Geolocation from '@react-native-community/geolocation';
import MapView, { Callout, Marker, ProviderPropType } from 'react-native-maps';
import { Overlay, Header } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import {
    SCLAlert,
    SCLAlertButton
} from 'react-native-scl-alert'
var { height, width } = Dimensions.get('window');

export default class NotificacionesRepuestos extends Component {
    constructor(props) {
        super(props);
        this.mecanicoApi = new mecanicoApi();

        this.state = {
            initSplash: true,
            loginSpl: true,
            loading: false,
            estado: 'Acpetado',
            show: false,
            visible: false,
            foto1: 'http://mecanico.balinetsoft.com/images/lg.png',
            foto2: 'http://mecanico.balinetsoft.com/images/lg.png',
            foto3: 'http://mecanico.balinetsoft.com/images/lg.png',
            reservas: this.props.route.params.reservas,
            contador: 0,

            poi: {
                latitude: '',
                longitude: '',
            },
            region: {
                latitude: 0,
                longitude: 0,
                latitudeDelta: 0,
                longitudeDelta: 0.04,
            },

        };
        this.RotateValueHolder = new Animated.Value(0);
    }



    handleClose = () => {
        this.setState({ show: false })
        this.props.navigation.replace('Menu');

    }
    cancelar(novedad) {
        if (novedad.imagenRepuesto) {
            this.setState({ foto1: Constants.BASEURI + this.state.reservas.imagenRepuesto })

        }
        if (novedad.imagenRepuesto1) {
            this.setState({ foto2: Constants.BASEURI + this.state.reservas.imagenRepuesto1 })

        }
        if (novedad.imagenRepuesto2) {
            this.setState({ foto3: Constants.BASEURI + this.state.reservas.imagenRepuesto2 })

        }
        this.setState({ visible: !this.state.visible })
    }
    cancelar2() {

        this.setState({ visible: !this.state.visible })
    }
    estado = async (estado, cliente) => {

        try {
            this.setState({ loading: true });
            cliente.estado = estado;
            if (estado == 2) {
                this.state.estado = "Rechazado";
                cliente.content = "Mecánico " + cliente.nombremecanico + " no disponible";

            } else {
                this.state.estado = "Acpetada";
                cliente.content = "El mecánico " + cliente.nombremecanico + " ha aceptado";

            }
            this.mecanicoApi.respuestaLocal(cliente).then(res => {
                if (res.idReservaMecanico) {
                    this.getReservas();

                    Alert.alert('Emergencia', this.state.estado, [{ text: 'Cerrar' }]);
                    this.setState({ loading: false });

                } else {
                    if (res.status && res.status == 'error') {
                        Alert.alert('Error', res.message, [{ text: 'Cerrar' }]);
                        this.setState({ loading: false });
                    }
                }
            });

        } catch (err) {
            Alert.alert('Error', err.message, [{ text: 'Cerrar' }]);
        }


    }

    render() {


        return (
            <View>

                <Overlay isVisible={this.state.visible}>
                    <View style={{ alignItems: 'center', width: '100%', height: '86%', alignContent: 'center' }} >

                        <Image
                            source={{ uri: this.state.foto1 }}
                            style={styles.imagen2} />
                        <Text style={{ fontSize: 15, color: '#617792', fontWeight: 'bold', textAlign: 'center' }}>Foto 1</Text>

                        <Image
                            source={{ uri: this.state.foto2 }}
                            style={styles.imagen2} />
                        <Text style={{ fontSize: 15, color: '#617792', fontWeight: 'bold', textAlign: 'center' }}>Foto 2</Text>
                        <Image
                            source={{ uri: this.state.foto3 }}
                            style={styles.imagen2} />
                        <Text style={{ fontSize: 15, color: '#617792', fontWeight: 'bold', textAlign: 'center' }}>Foto 3</Text>

                        <View style={{ alignItems: 'center', width: '90%', height: '10%', alignContent: 'center', flexDirection: 'row' }} >
                            <TouchableOpacity style={styles.btnVerEnSitio3}
                                onPress={() => this.cancelar2()} >
                                <Text style={{ fontSize: 14, textAlign: 'center', color: '#1F4997' }}>
                                    Salir</Text>
                            </TouchableOpacity>

                        </View>



                    </View>
                </Overlay>

                <Header
                    leftComponent={<TouchableOpacity onPress={() => {
                        this.props.navigation.goBack();
                    }}>
                        <Icon name="arrow-left" type="Ionicons" size={30} color="#fff" />
                    </TouchableOpacity>}
                    centerComponent={{ text: 'REPUESTOS', style: { color: '#fff' } }}

                    rightComponent={<TouchableOpacity onPress={() => {
                        this.props.navigation.navigate('Menu');

                    }}>
                        <Icon name="home" type="Ionicons" size={30} color="#fff" />
                    </TouchableOpacity>}
                    containerStyle={{
                        justifyContent: 'space-around',
                        backgroundColor: '#FF9100'
                    }}
                />
                <View style={{ flexDirection: 'row', alignItems: 'center', marginLeft: '30%' }}>
                    <View>
                        <Image
                            source={require('../../../assets/png/lg.png')}
                            style={styles.imagen} />
                        <View>
                            <Text style={{ fontSize: 18, color: '#617792', fontWeight: 'bold' }}> MECÁNICA EXPRESS</Text>
                        </View>
                    </View>

                </View>

                <ScrollView style={{ height: '70%', width: '100%' }}>

                    <View style={styles.container}>
                        <View style={styles.contImage}>
                            <View style={{ width: '100%', textAlign: 'center', alignItems: 'center' }}>

                                <Text style={{ fontSize: 20, color: '#617792', fontWeight: 'bold' }}>{this.state.reservas.detalle}</Text>
                                <Image
                                    source={{ uri: Constants.BASEURI + this.state.reservas.imagenRepuesto }}
                                    style={styles.avatar}
                                />
                            </View>
                            <MapView
                                style={styles.map}
                                mapType={"standard"}
                                initialCamera={{
                                    center: {
                                        latitude: this.state.reservas.poi[0].latitude,
                                        longitude: this.state.reservas.poi[0].longitude,
                                    },
                                    pitch: 45,
                                    heading: 90,
                                    altitude: 1000,
                                    zoom: 17,
                                }}
                                loadingEnabled
                                loadingIndicatorColor="#666666"
                                loadingBackgroundColor="#eeeeee"
                                showsIndoors
                                showsIndoorLevelPicker
                                zoomEnabled={true}
                                pitchEnabled={true}
                                showsUserLocation={true}
                                followsUserLocation={true}
                                showsCompass={true}
                                showsBuildings={true}
                                showsTraffic={true}
                                showsIndoors={true}
                                initialRegion={this.state.region}
                                zoomEnabled={true}>

                                <Marker coordinate={this.state.reservas.poi[0]}>
                                    <Callout>
                                        <View>
                                            <Text>{this.state.reservas.nombreUsuario}</Text>
                                        </View>
                                    </Callout>
                                </Marker>
                            </MapView>
                            <View style={{ width: '100%', textAlign: 'center', alignItems: 'center' }}>

                                <Text style={{ fontSize: 15, color: '#617792', fontWeight: 'bold' }}>{this.state.reservas.fecha}</Text>
                                <Text style={{ fontSize: 15, color: '#617792', fontWeight: 'bold' }}>{this.state.reservas.hora}</Text>
                                <Text size={16}>Fecha de la solicitud</Text>
                            </View>

                            <TouchableOpacity style={styles.btnVerEnSitio2}
                                onPress={() => this.cancelar(this.state.reservas)} >
                                <Text style={{ fontSize: 14, textAlign: 'center', color: '#1F4997' }}>
                                    Ver mas  Fotos del repuesto</Text>
                            </TouchableOpacity>

                            <View style={{ flexDirection: 'row', alignItems: 'center', width: '100%' }}>

                                <View style={{ alignItems: 'center', width: '30%' }}>
                                    <Text bold
                                        size={16}
                                        color="#525F7F"
                                        style={{ marginBottom: 4 }}
                                    >
                                        {this.state.reservas.distancia} Km
                                                            </Text>
                                    <Text size={16}>Distancia</Text>
                                </View>
                                <View style={{ alignItems: 'center', width: '30%' }}>
                                    <Text
                                        bold
                                        color="#525F7F"
                                        size={16}
                                        style={{ marginBottom: 4 }}
                                    >
                                        {this.state.reservas.nombreUsuario}

                                    </Text>

                                    <Text size={16}>Usuario</Text>
                                </View>
                                <View style={{ alignItems: 'center', width: '40%' }}>
                                    {this.state.reservas.acpetadas.length == 0 ? (
                                        <TouchableOpacity style={styles.btnVerEnSitio}
                                            onPress={() =>
                                                this.props.navigation.navigate('Cotizar', { repuesto: this.state.reservas })

                                            }>
                                            <Text style={{ fontSize: 14, textAlign: 'center', color: '#1F4997' }}>
                                                COTIZAR</Text>
                                        </TouchableOpacity>
                                    ) : (

                                            <TouchableOpacity style={styles.btnVerEnSitiol}
                                                onPress={() =>
                                                    this.props.navigation.navigate('DetalleCotizar', { detalle: this.state.reservas })

                                                }>
                                                <Text style={{ fontSize: 14, textAlign: 'center', color: '#1F4997' }}>
                                                    VER DETALLE
                        </Text>
                                            </TouchableOpacity>
                                        )}

                                </View>
                            </View>
                        </View>

                    </View>

                </ScrollView>

            </View>

        )


    }
    renderBtn(cliente) {
        if (cliente.estado == 0) {
            return (
                <View style={styles.containerdtn}>
                    <TouchableOpacity style={styles.btnVerEnSitioL}
                        onPress={() => this.estado(1, cliente)}>
                        <Text style={{ textAlign: 'center' }}>
                            ACEPTAR
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.btnVerEnSitioR}
                        onPress={() => this.estado(2, cliente)}>
                        <Text style={{ textAlign: 'center' }}>
                            RECHAZAR
                        </Text>
                    </TouchableOpacity>

                </View>
            );
        }
        if (cliente.estado == 1) {
            return (
                <View >
                    <Text style={{ fontSize: 15, color: '#35DC32', fontWeight: 'bold', textAlign: 'center' }}> En espera de confirmacion de {cliente.nommbreusuario}
                    </Text>

                </View>
            );
        }
        if (cliente.estado == 3) {
            return (
                <View >
                    <Text style={{ fontSize: 15, color: '#3243DC', fontWeight: 'bold', textAlign: 'center' }}> {cliente.nommbreusuario} confirmo el mecanico
                    </Text>
                    <Text style={{ fontSize: 15, color: '#3243DC', fontWeight: 'bold', textAlign: 'center' }}>  Dirigete al destino
                    </Text>
                </View>
            );
        }
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
    },
    containerdtn: {
        bottom: 0,
        alignItems: 'center',
        flexDirection: 'row'

    },
    btnVerEnSitioL: {
        backgroundColor: '#32DC36',
        borderRadius: 20,
        alignItems: 'center',
        width: '50%',
        padding: '3%',
        color: 'blue',
        borderWidth: 1,
        borderColor: '#4982e6',

    },
    btnVerEnSitioR: {

        backgroundColor: '#FF333C',
        borderRadius: 20,
        alignItems: 'center',
        width: '50%',
        padding: '3%',
        color: 'blue',
        borderWidth: 1,
        marginTop: '5%',
        borderColor: '#4982e6',

    },


    imagen: {
        width: 75,
        height: 75,
        marginLeft: '20%'

    },
    imagen2: {
        width: 150,
        height: 150,

    },
    contImage: {
        borderWidth: 2,
        width: '90%',
        height: height - 20,
        margin: '1%',
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
    },
    map: {
        width: '100%',
        height: '40%'
    },
    btnVerEnSitio: {
        backgroundColor: '#32DC36',
        borderRadius: 20,
        alignItems: 'center',
        width: '100%',
        height: '25%',
        alignContent: 'center',
        color: 'blue',
        borderWidth: 1,
        borderColor: '#4982e6',
        padding: '5%'
    },
    btnVerEnSitio2: {
        borderRadius: 20,
        alignItems: 'center',
        width: '100%',
        height: '8%',
        alignContent: 'center',
        padding: '5%',
        color: 'blue',
        borderWidth: 1,
        borderColor: '#4982e6',
    },
    btnVerEnSitio3: {
        borderRadius: 20,
        alignItems: 'center',
        width: '100%',
        alignContent: 'center',
        padding: '3%',
        color: 'blue',
        borderWidth: 1,
        borderColor: '#4982e6',
    },
    btnVerEnSitiol: {
        backgroundColor: '#F5F6F7',
        borderRadius: 20,
        alignItems: 'center',
        width: '70%',
        padding: '3%',
        color: 'blue',
        borderWidth: 1,
        borderColor: '#4982e6',
    },


    button: {
        alignSelf: 'center',

        fontSize: 16,
        color: '#fff',
        backgroundColor: '#617792',
        paddingVertical: 10,
        paddingHorizontal: 15,
        textAlign: "center",
        height: 50,
        width: 350,
        padding: 50,
        borderRadius: 24
    },

    profile: {
        flex: 1
    },

    profileCard: {
        borderTopLeftRadius: 6,
        borderTopRightRadius: 6,
        backgroundColor: 1,
        shadowColor: "black",
        shadowOffset: { 200: 0, 200: 0 },
        shadowRadius: 8,
        shadowOpacity: 0.2,
        width: '100%'
    },

    titulo: {
    },
    titulomapas: {
    },

    avatar: {
        width: 150,
        height: 150,
    },

    divider: {
        width: "90%",
        borderWidth: 1,
        borderColor: "#E9ECEF"
    },
});
