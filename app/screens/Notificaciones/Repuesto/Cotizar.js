
import React, { Component } from 'react';
import { Text, Dimensions, Image, ScrollView, Alert, StyleSheet, TextInput, View, TouchableOpacity, ImageBackground, ActivityIndicator } from 'react-native';
import mecanicoApi from '../../../api/mecanico.js';
import ImagePicker from 'react-native-image-picker';
import { CheckBox } from 'react-native-elements';

import Constants from '../../../common/Constants';
import LoadingProgress from '../../../common/LoadingProgress';


import AsyncStorage from '@react-native-community/async-storage';
import { Overlay, Header } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import {
    SCLAlert,
    SCLAlertButton
} from 'react-native-scl-alert'


export default class Cotizar extends Component {
    constructor(props) {
        super(props);
        this.mecanicoApi = new mecanicoApi();
        this.state = {
            imageFileData: '',
            loading: false,
            checked: false,
            loadingBtn: false,
            repuesto: this.props.route.params.repuesto,
            validador: false,
            showfailid: false,
            local: [],

            showok: false,
            message:'No debe existir campos vacios',
            userFeatures: {

                idCliente: '',
                idLocal: '',
                idReservaRepuesto: 0,

                nombre: '',
                marca: '',
                precio: 0,
                descuento: 0,
                original: 0,
                calidad: '',
                procedencia: '',
                stock: 0,

                tocken: '',
                headings: 'Repuesto',
                content: 'Necesito una cotización',


            },

        };
        this.getLocal();

    }
    getLocal = async () => {
        this.mecanicoApi.getLocal(await AsyncStorage.getItem(Constants.STORE_ID_USER)).then(res => {
            this.setState({
                local: res,
            });
            if (res != undefined) {
                this.setState({
                    loading: false,
                });
            }
        });
    }
    onChangeText = (key, val) => {
        this.setState({ [key]: val });
    };
    terminos() {
        this.setState({ checked: !this.state.checked })


    }
    updateStatusFeature(param, value) {
        let userFeatConst = this.state.userFeatures;
        userFeatConst[param] = value;
        this.setState({ userFeatures: userFeatConst });
    }
    validarprecio(value) {
        var regex = /^[1-9]\d*(((,\d{3}){1})?(\.\d{0,2})?)$/;
        if(regex.test(value)){
            return true ;
        }else{
            this.setState({ showfailid: true,message:'Debes in gresar un precio válido' })
            return false ;

        }
    }

    validadorCampo() {
        if (this.state.userFeatures.nombre != '' && this.state.userFeatures.imggrande != '') {
            if(this.validarprecio(this.state.userFeatures.precio)){
                if(this.state.userFeatures.descuento>=100||this.state.userFeatures.descuento<0){
                    this.setState({ showfailid: true,message:'Ingresa un descuento válido(0% al 100%)' })
                    return false;
                }else{
                    return true;
                }
            }else{
                return false;
            }
        } else {
            this.setState({ showfailid: true,message:'No debe existir campos vacios' })
            return false;
        }
    }
    
    
    handleOpenFaild = () => {
        this.setState({ showfailid: true })
    }

    handleCloseFaild = () => {
        this.setState({ showfailid: false })

    }

    handleOpenOk = () => {
        this.setState({ showok: true })
        this.props.navigation.navigate('MenuNotificaciones')

    }

    handleCloseOk = () => {
        this.setState({ showok: false })
        this.props.navigation.navigate('MenuNotificaciones')

    }

    cotizar = async () => {
        this.state.userFeatures.imggrande = this.state.imageFileData;
        this.state.validador == true;
        try {
            if (this.validadorCampo()) {
                this.setState({ loading: true });
                if (this.state.checked == true) {
                    this.state.userFeatures.original = 1;
                }
                if (this.state.checked == false) {
                    this.state.userFeatures.original = 0;
                }
                this.state.userFeatures.url = this.state.imageFileData;
                this.state.userFeatures.tocken = this.state.repuesto.tocken;
                this.state.userFeatures.headings = 'Cotización Repuesto';
                this.state.userFeatures.content = 'EL local '+this.state.local.nombre+' realizó una cotizacion';
                this.state.userFeatures.idCliente = this.state.repuesto.idUsuaio;
                this.state.userFeatures.idLocal = await AsyncStorage.getItem(Constants.STORE_ID_USER);
                this.state.userFeatures.idReservaRepuesto = this.state.repuesto.idReservaRepuesto;
                this.mecanicoApi.createRespuesta(this.state.userFeatures).then(res => {
                    if (res.idLocal) {
                        this.setState({ loading: false });
                        this.setState({ showok: true })

                    } else {
                        if (res.status && res.status == 'error') {
                            Alert.alert('Error', res.message, [{ text: 'Cerrar' }]);
                            this.setState({ loading: false });
                        }
                    }
                });
            }
        } catch (err) {
            Alert.alert('Error', err.message, [{ text: 'Cerrar' }]);
        }
    };

    render() {
        if (this.state.loading) {
            return <LoadingProgress />;
        }
        if (!this.state.loading) {
            return (
                <View>
                    <Header
                        leftComponent={<TouchableOpacity onPress={() => {
                            this.props.navigation.navigate('NotificacionesRepuestos')
                        }}>
                            <Icon name="arrow-left" type="Ionicons" size={30} color="#fff" />
                        </TouchableOpacity>}
                        centerComponent={{ text: 'COTIZAR', style: { color: '#fff' } }}

                        rightComponent={<TouchableOpacity onPress={() => {
                            this.props.navigation.navigate('Menu');

                        }}>
                            <Icon name="home" type="Ionicons" size={30} color="#fff" />
                        </TouchableOpacity>}
                                 containerStyle={{
                                    justifyContent: 'space-around',
                                    backgroundColor: '#FF9100'
                                }}
                    />
                    <ScrollView>
                        <View style={styles.container}>
                            <View style={{ alignItems: 'center' }}>
                                <Image
                                    source={require('../../../assets/png/lg.png')}
                                    style={styles.imagen} />
                                <View>
                                    <Text style={{ fontSize: 17, color: '#617792', fontWeight: 'bold', textAlign: 'center' }}>MECÁNICA EXPRESS</Text>
                                </View>
                            </View>

                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <View style={styles.containe}>
                                    <Text style={{ fontSize: 17, color: '#617792', fontWeight: 'bold', textAlign: 'center' }}>FOTO DEL REPUESTO</Text>
                                    <TouchableOpacity
                                        style={styles.fotoContainerfree}
                                        onPress={() => this.chooseImage()}>
                                        {this.renderFileData()}
                                    </TouchableOpacity>

                                </View>
                            </View>
                            <View style={styles.ellanoteama}>
                                <TextInput
                                    placeholder="Nombre"
                                    onChangeText={val => this.updateStatusFeature('nombre', val)}
                                    style={styles.inputSearch}
                                    value={this.state.userFeatures.nombre}
                                />
                                <TextInput
                                    placeholder="Marca"
                                    onChangeText={val => this.updateStatusFeature('marca', val)}
                                    style={styles.inputSearch}
                                    value={this.state.userFeatures.marca}
                                />
                                <TextInput
                                    placeholder="Precio"
                                    onChangeText={val => this.updateStatusFeature('precio', parseFloat(val))
                                    }
                                    keyboardType={'numeric'}
                                    style={styles.inputSearch}
                                    value={this.state.userFeatures.precio}
                                />
                                <TextInput
                                    placeholder="Descuento %"
                                    onChangeText={val => {
                                        val == 0 ? (
                                            this.updateStatusFeature('descuento', 0)
                                        ) : (
                                                this.updateStatusFeature('descuento', parseFloat(val).toFixed(0))
                                            )
                                    }}
                                    keyboardType={'numeric'}
                                    style={styles.inputSearch}
                                    value={this.state.userFeatures.descuento}
                                />

                                <TextInput
                                    placeholder="Stock"
                                    onChangeText={val => {
                                        val == 0 ? (
                                            this.updateStatusFeature('stock', 0)
                                        ) : (
                                                this.updateStatusFeature('stock', parseFloat(val).toFixed(0))
                                            )
                                    }}
                                    keyboardType={'numeric'}
                                    style={styles.inputSearch}
                                    value={this.state.userFeatures.stock}
                                />
                                <TextInput
                                    placeholder="Calidad"
                                    onChangeText={val => this.updateStatusFeature('calidad', val)}
                                    style={styles.inputSearch}
                                    value={this.state.userFeatures.calidad}
                                />
                                <TextInput
                                    placeholder="Procedencia"
                                    onChangeText={val => this.updateStatusFeature('procedencia', val)}
                                    style={styles.inputSearch}
                                    value={this.state.userFeatures.procedencia}
                                />
                                <View style={{ flexDirection: 'row' }}>

                                    <Text style={{ alignItems: 'center', fontWeight: 'bold', marginTop: '5%' }}>Repuesto Original</Text>

                                    <CheckBox
                                        right
                                        checkedIcon="dot-circle-o"
                                        uncheckedIcon="circle-o"
                                        checkedColor="#502A18"
                                        checked={this.state.checked}
                                        onPress={() => this.terminos()}
                                    />
                                </View>
                                {this.renderBtn()}
                                {this.renderValidadorError()}
                                {this.renderValidadorOk()}



                            </View>
                        </View>
                    </ScrollView>
                </View>
            );
        }
    }
    renderValidadorError() {
        return (
            <View style={styles.container}>
                <SCLAlert
                    theme="danger"
                    show={this.state.showfailid}
                    title="Cotización"
                    onRequestClose={this.handleCloseFaild}
                    cancellable={true}
                    subtitle={this.state.message}
                >
                    <SCLAlertButton theme="danger" onPress={this.handleCloseFaild}>OK</SCLAlertButton>
                </SCLAlert>
            </View>
        );
    }
    renderValidadorOk() {
        return (
            <View style={styles.container}>
                <SCLAlert
                    theme="success"
                    show={this.state.showok}
                    title="Cotización"
                    onRequestClose={this.handleOpenOk}
                    cancellable={true}
                    subtitle="Cotización exitosa"
                >
                    <SCLAlertButton theme="success" onPress={this.handleOpenOk}>OK</SCLAlertButton>
                </SCLAlert>
            </View>
        );
    }
    renderBtn() {
        if (!this.state.loadingBtn) {
            return (
                <View>
                    <TouchableOpacity
                        onPress={() => { this.cotizar() }}
                        style={styles.btnStyle}>
                        <Text style={{ fontSize: 14, textAlign: 'center', color: '#1F4997' }}>
                            ENVIAR COTIZACIÓN
                        </Text>
                    </TouchableOpacity>

                </View>
            );
        } else {
            return (
                <TouchableOpacity style={styles.btnStyle}>
                    <ActivityIndicator size="small" color={'#1F4997'} />
                </TouchableOpacity>
            );
        }
    }

    chooseImage(index) {
        let options = {
            title: 'Seleccionar imagen',
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
            cancelButtonTitle: 'Cancelar',
            takePhotoButtonTitle: 'Tomar Foto',
            chooseFromLibraryButtonTitle: 'Elegir Foto de la Galería',
            mediaType: 'photo',
            index: index,
        };
        ImagePicker.showImagePicker(options, response => {
            if (response.didCancel) {
            } else if (response.error) {
            } else if (response.customButton) {
                alert(response.customButton);
            } else {
                let ministatus = 'data:image/jpg;base64,' + response.data;
                this.setState({
                    imageFileData: ministatus,
                });
            }
        });
    }


    renderFileData() {
        if (this.state.userFeatures.path) {
            return (
                <Image
                    source={{ uri: Constants.BASEURI + this.state.userFeatures.path }}
                    style={styles.imagesfrelance}

                />
            );
        }
        if (this.state.imageFileData) {
            return (
                <Image
                    source={{
                        uri: this.state.imageFileData,
                    }}
                    style={styles.imagesfrelance}
                />
            );
        } else {
            return (
                <View>
                    <Image
                        source={require('../../../assets/png/camera.png')}
                        style={{
                            width: 50,
                            marginLeft: '40%',
                            marginTop: '40%',

                            height: 50
                        }} />
                </View>
            );
        }
    }
}

const rnpickerStyles = StyleSheet.create({
    labelStyle: { textAlign: 'left', minWidth: 60 },
    labelStyle2: { textAlign: 'center', minWidth: 60 },
    pickerContainer: {
        borderBottomWidth: 1,
        borderBottomColor: '#FFFF',
        flex: 1,
    },
});

const styles = StyleSheet.create({
    btnVerEnSitio: {
        backgroundColor: '#F5F6F7',
        borderRadius: 20,
        alignItems: 'center',
        width: '70%',
        padding: '3%',
        color: 'blue',
        borderWidth: 1,
        marginTop: '5%',
        borderColor: '#4982e6',
    },
    map: {
        width: 180,
        height: 180,
    },

    btnVerEnSitio3: {
        backgroundColor: '#F5F6F7',
        borderRadius: 20,
        alignItems: 'center',
        width: '90%',
        padding: '3%',
        color: 'blue',
        borderWidth: 1,
        marginTop: '5%',
        borderColor: '#4982e6',
    },
    fotoContainerfree: {
        width: 300,
        height: 300,
        borderColor: 'black',
        borderWidth: 1,

    },
    imagesfrelance: {
        width: 300,
        height: 300,
        borderColor: 'black',
        borderWidth: 1,

    },
    container: {
        flex: 1,
        alignItems: 'center',
        height: '100%',
        padding: '3%',
        backgroundColor: '#fff',
    },
    btnVerEnSitio2: {
        backgroundColor: '#F5F6F7',
        borderRadius: 20,
        alignItems: 'center',
        width: '70%',
        padding: '3%',
        color: 'blue',
        borderWidth: 1,
        marginTop: '-10%',
        borderColor: '#4982e6',
    },
    descriptionTag: {
        color: '#502A18',
    },
    btnTextStyle: {
        color: '#FFFFFF',
        fontSize: 22,
        alignItems: 'center',
        textAlign: 'center',
    },
    btnStyle: {
        alignItems: 'center',
        borderWidth: 1,
        borderColor: '#4982e6',
        borderRadius: 20,
        justifyContent: 'center',
        backgroundColor: '#F5F6F7',
        height: 50,
        marginTop: 20,
        marginBottom: 80,

    },
    images: {
        width: 150,
        height: 150,
        borderColor: 'black',
        borderWidth: 1,
    },
    fotoContainer: {
        borderColor: '#424244',
        borderWidth: 1,
        borderStyle: 'dotted',
        backgroundColor: '#F9FCFD',
    },

    ellanoteama: {
        minWidth: '80%',
        backgroundColor: '#fff',
    },
    createAccount: {
        fontSize: 30,
        textAlign: 'left',
        marginVertical: 15,
    },
    inputSearch: {
        borderBottomWidth: 1,
    },
    imagen: {
        width: 70,
        height: 70,


    },
    imagen2: {
        width: 155,
        height: 130,


    },
});
