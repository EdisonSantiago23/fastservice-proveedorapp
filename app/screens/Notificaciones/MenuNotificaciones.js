import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  View,
  ScrollView,
  TouchableOpacity,
  ImageBackground,
  Image,
  StatusBar,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Badge, Header} from 'react-native-elements';
import AsyncStorage from '@react-native-community/async-storage';
import Constants from '../../../app/common/Constants';
import mecanicoApi from '../../api/mecanico.js';
import LoadingProgress from '../../common/LoadingProgress';

export default class MenuNotificaciones extends Component {
  constructor(props) {
    super(props);
    this.mecanicoApi = new mecanicoApi();

    this.state = {
      initSplash: true,
      loginSpl: true,
      loading: true,
      localtype: this.props.route.params.localtype,
      contador: '',
    };
    this.contadorgeneral();
    const unsubscribe = this.props.navigation.addListener('focus', () => {
      this.contadorgeneral();
    });
  }
  contadorgeneral = async () => {
    this.mecanicoApi
      .contadorgeneral(
        await AsyncStorage.getItem(Constants.STORE_ID_USER),
        this.state.localtype,
      )
      .then(res => {
        this.setState({
          contador: res[0],
        });
        this.setState({loading: false});
      });
  };
  render() {
    if (this.state.loading) {
      return <LoadingProgress />;
    }
    if (!this.state.loading) {
      return (
        <View style={{height: '100%', width: '100%'}}>
          <StatusBar backgroundColor="#FF9100" barStyle="light-content" />
          <Header
            leftComponent={
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate('Menu');
                }}>
                <Icon
                  name="arrow-left"
                  type="Ionicons"
                  size={30}
                  color="#fff"
                />
              </TouchableOpacity>
            }
            centerComponent={{text: 'NOTIFICACIONES', style: {color: '#fff'}}}
            rightComponent={
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate('Menu');
                }}>
                <Icon name="home" type="Ionicons" size={30} color="#fff" />
              </TouchableOpacity>
            }
            containerStyle={{
              justifyContent: 'space-around',
              backgroundColor: '#FF9100',
            }}
          />
          <View style={{alignItems: 'center'}}>
            <Image
              source={require('../../assets/png/lg.png')}
              style={styles.imagen}
            />
            <Text
              style={{
                fontSize: 20,
                color: '#617792',
                fontWeight: 'bold',
                textAlign: 'center',
              }}>
              MECÁNICA EXPRESS
            </Text>
          </View>
          <View style={{width: '100%', height: '70%', alignItems: 'center'}}>
            <ScrollView style={{height: '100%', width: '90%'}}>
              <View style={styles.contImage}>
                <ImageBackground
                  source={require('../../assets/jpg/im5.jpeg')}
                  imageStyle={{borderRadius: 20}}
                  style={{width: '100%', height: '100%'}}>
                  <TouchableOpacity
                    onPress={() =>
                      this.props.navigation.navigate('DetalleEmergencia', {
                        localtype: this.state.localtype,
                      })
                    }
                    style={styles.btnImage}>
                    <Text
                      style={{fontSize: 30, fontWeight: 'bold', color: '#FFF'}}>
                      EMERGENCIAS
                    </Text>

                    {this.state.contador.contadoremergencia > 0 ? (
                      <Badge
                        status="error"
                        value={this.state.contador.contadoremergencia}
                        width={30}
                        containerStyle={{
                          position: 'absolute',
                          top: -4,
                          right: -4,
                        }}
                      />
                    ) : null}
                  </TouchableOpacity>
                </ImageBackground>
              </View>
              <View style={styles.contImage}>
                <ImageBackground
                  source={require('../../assets/jpg/im2.jpeg')}
                  imageStyle={{borderRadius: 20}}
                  style={{width: '100%', height: '100%', borderRadius: 25}}>
                  <TouchableOpacity
                    onPress={() =>
                      this.props.navigation.navigate('DetalleRepuesto', {
                        localtype: this.state.localtype,
                      })
                    }
                    style={styles.btnImage}>
                    <Text
                      style={{fontSize: 30, fontWeight: 'bold', color: '#FFF'}}>
                      REPUESTOS
                    </Text>

                    {this.state.contador.contadorrepuesto > 0 ? (
                      <Badge
                        status="error"
                        value={this.state.contador.contadorrepuesto}
                        width={30}
                        containerStyle={{
                          position: 'absolute',
                          top: -4,
                          right: -4,
                        }}
                      />
                    ) : null}
                  </TouchableOpacity>
                </ImageBackground>
              </View>
              <View style={styles.contImage}>
                <ImageBackground
                  source={require('../../assets/jpg/im3.jpeg')}
                  imageStyle={{borderRadius: 20}}
                  style={{width: '100%', height: '100%'}}>
                  <TouchableOpacity
                    onPress={() =>
                      this.props.navigation.navigate('DetalleMantenimiento', {
                        localtype: this.state.localtype,
                      })
                    }
                    style={styles.btnImage}>
                    <Text
                      style={{fontSize: 30, fontWeight: 'bold', color: '#FFF'}}>
                      MANTENIMIENTO
                    </Text>

                    {this.state.contador.contadormantenimiento > 0 ? (
                      <Badge
                        status="error"
                        value={this.state.contador.contadormantenimiento}
                        width={30}
                        containerStyle={{
                          position: 'absolute',
                          top: -4,
                          right: -4,
                        }}
                      />
                    ) : null}
                  </TouchableOpacity>
                </ImageBackground>
              </View>
              <View style={styles.contImage}>
                <ImageBackground
                  source={require('../../assets/jpg/im3.jpeg')}
                  imageStyle={{borderRadius: 20}}
                  style={{width: '100%', height: '100%'}}>
                  <TouchableOpacity
                    onPress={() =>
                      this.props.navigation.navigate('NotificacionesMotoNew', {
                        localtype: this.state.localtype,
                      })
                    }
                    style={styles.btnImage}>
                    <Text
                      style={{fontSize: 30, fontWeight: 'bold', color: '#FFF'}}>
                      Servicios Autorizados
                    </Text>

                    {this.state.contador.contadorsa > 0 ? (
                      <Badge
                        status="error"
                        value={this.state.contador.contadorsa}
                        width={30}
                        containerStyle={{
                          position: 'absolute',
                          top: -4,
                          right: -4,
                        }}
                      />
                    ) : null}
                  </TouchableOpacity>
                </ImageBackground>
              </View>
            </ScrollView>
          </View>
        </View>
      );
    }
  }
  _renderItem = ({item, dimensions}) => (
    <View>
      <Image style={styles.image} source={item.image} />
    </View>
  );
}

const styles = StyleSheet.create({
  image: {
    width: 400,
    height: 200,
  },
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  contImage: {
    flex: 1,
    marginTop: 5,
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 2,
    height: 180,
    width: '100%',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
  },
  btnImage: {
    flex: 1,
    justifyContent: 'center',
    paddingBottom: 10,
    alignItems: 'center',
  },
  imagen: {
    width: 100,
    height: 100,
  },
});
