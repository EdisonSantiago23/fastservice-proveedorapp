
import React, { Component } from 'react';
import { StyleSheet, Alert, ScrollView, TouchableOpacity, Dimensions, Image, Animated, View } from 'react-native';
import mecanicoApi from '../../../api/mecanico';
import LoadingProgress from '../../../common/LoadingProgress';
import { Block, Text, theme } from "galio-framework";
import AsyncStorage from '@react-native-community/async-storage';
import Constants from '../../../common/Constants';
import Geolocation from '@react-native-community/geolocation';
import MapView, { Callout, Marker, ProviderPropType } from 'react-native-maps';
import Icon from 'react-native-vector-icons/FontAwesome';
var { height, width } = Dimensions.get('window');
import Feather from 'react-native-vector-icons/Feather';

import {
    SCLAlert,
    SCLAlertButton
} from 'react-native-scl-alert'

import { Overlay, Header } from 'react-native-elements';
export default class NotificacionesMotoNew extends Component {
    constructor(props) {
        super(props);
        this.mecanicoApi = new mecanicoApi();

        this.state = {
            initSplash: true,
            loginSpl: true,
            loading: true,
            reservas: [],
            estado: 'Acpetado',
            show: false,
            localtype: this.props.route.params.localtype,

            poi: {
                latitude: '',
                longitude: '',
            },
            region: {
                latitude: 0,
                longitude: 0,
                latitudeDelta: 0,
                longitudeDelta: 0.04,
            },

        };
        this.RotateValueHolder = new Animated.Value(0);
        this.getLocalidad();

    }
    getLocalidad= async () => {
        await Geolocation.getCurrentPosition(
            position => {
                const { latitude, longitude } = position.coords;
                this.state.poi.latitude = position.coords.latitude;
                this.state.poi.longitude = position.coords.longitude;
                this.setState({
                    region: {
                        longitude: longitude,
                        latitude: latitude,
                        latitudeDelta: 0.029213524352655895,
                        longitudeDelta: (width / height) * 0.029213524352655895,
                    },
                });
                this.getReservas();
            }
        );

    }
    getReservas = async () => {
        this.mecanicoApi.getRecervaproveedorMotos(await AsyncStorage.getItem(Constants.STORE_ID_USER)).then(res => {
            if (res.status) {
                this.setState({ show: true, loading: false, })

            }
            this.setState({
                reservas: res,
            });
            if (res != undefined) {

                this.setState({
                    loading: false,
                });
            }
        });
    }

    estado = async (estado, cliente) => {

        try {
            this.setState({ loading: true });
            cliente.estado = estado;
            if (estado == 2) {
                this.state.estado = "Rechazado";
                cliente.content = "Mecánico " + cliente.nombremecanico + " no disponible";

            } else {
                this.state.estado = "Acpetada";
                cliente.content = "El mecánico " + cliente.nombremecanico + " ha aceptado";

            }
            this.mecanicoApi.respuestaLocal(cliente).then(res => {
                if (res.idReservaMecanico) {
                    this.getReservas();

                    Alert.alert('Emergencia', this.state.estado, [{ text: 'Cerrar' }]);
                    this.setState({ loading: false });

                } else {
                    if (res.status && res.status == 'error') {
                        Alert.alert('Error', res.message, [{ text: 'Cerrar' }]);
                        this.setState({ loading: false });
                    }
                }
            });

        } catch (err) {
            Alert.alert('Error', err.message, [{ text: 'Cerrar' }]);
        }


    }
    handleOpen = () => {
        this.setState({ show: true })
    }

    handleClose = () => {
        this.setState({ show: false })
        this.props.navigation.replace('Menu');
        
    }
    render() {
        if (this.state.loading) {
            return <LoadingProgress />;
        }
        if (!this.state.loading) {
            if (!this.state.reservas.status) {
                return (
                    <View>

                        <Header
                            leftComponent={<TouchableOpacity onPress={() => {
                                this.props.navigation.navigate('MenuNotificaciones');
                            }}>
                                <Icon name="arrow-left" type="Ionicons" size={30} color="#fff" />
                            </TouchableOpacity>}
                            centerComponent={{ text: 'SERVICIO AUTORIZADO', style: { color: '#fff' } }}

                            rightComponent={<TouchableOpacity onPress={() => {
                                this.props.navigation.navigate('Menu');

                            }}>
                                <Icon name="home" type="Ionicons" size={30} color="#fff" />
                            </TouchableOpacity>}
                        />
                        <View style={{ flexDirection: 'row', alignItems: 'center', marginLeft: '30%' }}>
                            <View>
                                <Image
                                    source={require('../../../assets/png/lg.png')}
                                    style={styles.imagen} />
                                <View>
                                    <Text style={{ fontSize: 18, color: '#617792', fontWeight: 'bold' }}> MECÁNICA EXPRESS</Text>
                                </View>
                            </View>

                        </View>
                        <ScrollView style={{ height: '75%', width: '100%' }}>

                            <View style={styles.container}>
                                        {this.state.reservas.map(novedad => (
                                            <View style={styles.contImage}>
                                                <Block flex style={styles.profileCard}>

                                                    <Block middle style={styles.titulo}>
                                                        <Text style={{ fontSize: 20, color: '#617792', fontWeight: 'bold' }}>{novedad.nombreemergencia} km</Text>

                                                    </Block>
                                                    {this.renderBtn(novedad)}

                                                    <Block middle style={styles.titulo}>

                                                        <MapView
                                                               style={styles.map}
                                                               mapType={"standard"}
                                                               initialCamera={{
                                                                   center: {
                                                                       latitude: novedad.poi[0].latitude,
                                                                       longitude: novedad.poi[0].longitude,
                                                                   },
                                                                   pitch: 45,
                                                                   heading: 90,
                                                                   altitude: 1000,
                                                                   zoom: 17,
                                                               }}
                                                               loadingEnabled
                                                               loadingIndicatorColor="#666666"
                                                               loadingBackgroundColor="#eeeeee"
                                                               showsIndoors
                                                               showsIndoorLevelPicker
                                                               zoomEnabled={true}
                                                               pitchEnabled={true}
                                                               showsUserLocation={true}
                                                               followsUserLocation={true}
                                                               showsCompass={true}
                                                               showsBuildings={true}
                                                               showsTraffic={true}
                                                               showsIndoors={true}
                                                               initialRegion={this.state.region}
                                                               zoomEnabled={true}>
                                                            <Marker coordinate={novedad.poi[0]}>
                                                                <Callout>
                                                                    <View>
                                                                        <Text>Mi local</Text>
                                                                    </View>
                                                                </Callout>
                                                            </Marker>
                                                        </MapView>

                                                        <Block middle>
                                                            <Text
                                                                bold
                                                                color="#525F7F"
                                                                size={16}
                                                                style={{ marginBottom: 4 }}
                                                            >
                                                                {novedad.estado == 0 ? (
                                                                    <Text bold
                                                                        size={16}
                                                                        color="#32BEDC"
                                                                        style={{ marginBottom: 4 }}>Pendiente</Text>

                                                                ) : (

                                                                        novedad.estado == 1 ? (
                                                                            <Text bold
                                                                                size={16}
                                                                                color="#32BEDC"
                                                                                style={{ marginBottom: 4 }}>Aceptado</Text>

                                                                        ) : (
                                                                                novedad.estado == 2 ? (
                                                                                    <Text bold
                                                                                        size={16}
                                                                                        color="#FF333C"
                                                                                        style={{ marginBottom: 4 }}>Rechazado</Text>

                                                                                ) : (
                                                                                        <Text bold
                                                                                            size={16}
                                                                                            color="#3243DC"
                                                                                            style={{ marginBottom: 4 }}>Confirmado</Text>
                                                                                    )

                                                                            )


                                                                    )}

                                                            </Text>
                                                            <Text size={16}>Estado</Text>
                                                        </Block>
                                                        <Text style={{ fontSize: 15, color: '#617792', fontWeight: 'bold' }}>{novedad.fecha}</Text>
                                                    <Text style={{ fontSize: 15, color: '#617792', fontWeight: 'bold' }}>{novedad.hora}</Text>
                                                    <Text size={16}>Fecha de la solicitud</Text>
                                                    </Block>



                                                    <Block style={styles.info}>
                                                        <Block middle style={styles.nameInfo}>
                                                            <Text bold size={28} color="#32325D">
                                                                {novedad.nombre}</Text>


                                                        </Block>

                                                        <Block row space="between">
                                                            <Block middle>
                                                                <Image
                                                                    source={{ uri: Constants.BASEURI + novedad.imagenmecanico }}
                                                                    style={styles.avatar}
                                                                />
                                                            </Block>

                                                            <Block middle>
                                                                <Text bold
                                                                    size={16}
                                                                    color="#525F7F"
                                                                    style={{ marginBottom: 4 }}
                                                                >
                                                                    {novedad.nombremecanico}
                                                                </Text>
                                                                <Text size={16}>Mecánico</Text>
                                                            </Block>
                                                            <Block middle>
                                                                <Text bold
                                                                    size={16}
                                                                    color="#525F7F"
                                                                    style={{ marginBottom: 4 }}
                                                                >
                                                                    ${novedad.precioemergencia}
                                                                </Text>
                                                                <Text size={16}>Precio</Text>
                                                            </Block>
                                                            <Block middle>
                                                                <Text
                                                                    bold
                                                                    color="#525F7F"
                                                                    size={16}
                                                                    style={{ marginBottom: 4 }}
                                                                >
                                                                    {novedad.nommbreusuario}

                                                                </Text>

                                                                <Text size={16}>Usuario</Text>
                                                            </Block>

                                                        </Block>
                                                    </Block>

                                                </Block>

                                            </View>

                                        ))}
                                         </View>

                        </ScrollView>

                    </View>
                );
            } else {
                return (
                    <View style={styles.container}>
                        <SCLAlert
                            theme="danger"
                            show={this.state.show}
                            title="Servicio Autorizado"
                            onRequestClose={this.handleClose}
                            cancellable={true}

                            subtitle="Actualmente no tienes servicios por atender"
                        >
                            <SCLAlertButton theme="danger" onPress={this.handleClose}>Atras</SCLAlertButton>
                        </SCLAlert>
                    </View>
                );

            }

        }
    }
    renderBtn(cliente) {
        if (cliente.estado == 0) {
            return (
                <View style={styles.containerdtn}>
                    <TouchableOpacity style={styles.btnVerEnSitioL}
                        onPress={() => this.estado(1, cliente)}>
                        <Text style={{ textAlign: 'center' }}>
                            ACEPTAR
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.btnVerEnSitioR}
                        onPress={() => this.estado(2, cliente)}>
                        <Text style={{ textAlign: 'center' }}>
                            RECHAZAR
                        </Text>
                    </TouchableOpacity>

                </View>
            );
        }
        if (cliente.estado == 1) {
                return (
                    <View style={{ alignContent: 'center', textAlign: 'center' }} >
                        <Text style={{ fontSize: 15, color: '#35DC32', fontWeight: 'bold', textAlign: 'center' }}> En espera de confirmacion de {cliente.nommbreusuario}
                        </Text>
                       
                    </View>
                );
            
     
        }
        if (cliente.estado == 3) {
            return (
                <View >
                    <Text style={{ fontSize: 15, color: '#3243DC', fontWeight: 'bold', textAlign: 'center' }}>En espera que el cliente {cliente.nommbreusuario}  pague el servicio
                    </Text>              
                </View>
            );
        }
        if (cliente.estado == 4) {
            return (
                <View >
                    <Text style={{ fontSize: 15, color: '#3243DC', fontWeight: 'bold', textAlign: 'center' }}> {cliente.nommbreusuario} aceptó el servicio, dirígete a su ubicación, el pago es en efectivo 
                    </Text>
                    <Text style={{ fontSize: 15, color: '#3243DC', fontWeight: 'bold', textAlign: 'center' }}>
                    </Text>
                    <View style={{ alignContent: 'center', textAlign: 'center' }} >
                        {this.state.localtype == 0 ? (
                         <TouchableOpacity style={styles.btnVerEnSitioL2}
                         onPress={() => {
                             const shareOptions = {
                                 title: 'Compartir producto vía',
                                 message: 'El cliente:' + cliente.nommbreusuario + ' se encuentra en ',
                                 url:
                                     'https://maps.google.com/?q=' + cliente.poi[0].latitude + ',' + cliente.poi[0].longitude
                             };
                             Share.open(shareOptions);
                         }}>
                         <Feather
                             name="share-2"
                             size={20}/>
                         <Text>Compartir Ubicación</Text>
                     </TouchableOpacity>
                        ) : (
                            <TouchableOpacity style={styles.btnVerEnSitioL2}
                                onPress={() => {
                                    Linking.openURL('https://maps.google.com/?q=' + cliente.poi[0].latitude + ',' + cliente.poi[0].longitude)
                                }}>
                                <Feather
                                    name="share-2"
                                    size={20}/>
                                <Text>Google Maps</Text>
                            </TouchableOpacity>
                        )}
                    </View>
                </View>
            );
        }
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
   },
    containerdtn: {
        bottom: 0,
        alignItems: 'center',
        flexDirection: 'row'

    },
    btnVerEnSitioL: {
        backgroundColor: '#32DC36',
        borderRadius: 20,
        alignItems: 'center',
        width: '50%',
        padding: '3%',
        color: 'blue',
        borderWidth: 1,
        marginTop: '5%',
        borderColor: '#4982e6',

    },
    btnVerEnSitioL2: {
        backgroundColor: '#32DC36',
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        padding: '3%',
        color: 'blue',
        borderWidth: 1,
        borderColor: '#4982e6',
        flexDirection: 'row',

    },
    btnVerEnSitioR: {

        backgroundColor: '#FF333C',
        borderRadius: 20,
        alignItems: 'center',
        width: '50%',
        padding: '3%',
        color: 'blue',
        borderWidth: 1,
        marginTop: '5%',
        borderColor: '#4982e6',

    },

    profile: {
        flex: 1
    },
    imagen: {
        width: 75,
        height: 75,
        marginLeft: '20%'

    },
    contImage: {
        flex: 1,
        marginTop: 3,
        justifyContent: 'center',
        borderWidth: 2,
        width: '97%',
        height: height - 150,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,

    },
    map: {
        width: '95%',
        marginTop: '15%',
        height: '50%',
    },
    headerContainer: {
        flexDirection: 'row',
        padding: 5,
        alignItems: 'flex-start',
    },
    btnVerEnSitio: {
        backgroundColor: '#F5F6F7',
        borderRadius: 20,
        width: '35%',
        padding: '3%',
        color: 'blue',
        borderWidth: 1,
        marginTop: '5%',
        borderColor: '#4982e6',
    },
    carousel: {
        position: 'absolute',
        bottom: 0,
        marginBottom: 48
    },
    cardContainer: {
        alignSelf: 'center',
        backgroundColor: '#617792',
        height: 630,
        width: 350,
        padding: 50,
        borderRadius: 24
    },
    cardImage: {
        height: 120,
        width: 300,
        bottom: 0,
        position: 'absolute',
        borderBottomLeftRadius: 24,
        borderBottomRightRadius: 24
    },
    button: {
        alignSelf: 'center',

        fontSize: 16,
        color: '#fff',
        backgroundColor: '#617792',
        paddingVertical: 10,
        paddingHorizontal: 15,
        textAlign: "center",
        height: 50,
        width: 350,
        padding: 50,
        borderRadius: 24
    },
    cardTitle: {
        color: 'white',
        fontSize: 22,
        alignSelf: 'center'
    },
    profile: {
        flex: 1
    },
    profileContainer: {
        width: '100%',
        height: '50%',
        color: '#617792',
        padding: 0,
        zIndex: 1
    },
    profileBackground: {
        width: '100%',
        height: 350 / 2
    },
    profileCard: {

        marginTop: 65,
        borderTopLeftRadius: 6,
        borderTopRightRadius: 6,
        backgroundColor: 1,
        shadowColor: "black",
        shadowOffset: { 200: 0, 200: 0 },
        shadowRadius: 8,
        shadowOpacity: 0.2,
    },
    info: {

    },
    titulo: {
        marginTop: -70
    },
    avatarContainer: {
        position: "relative",
        marginTop: -25
    },
    avatar: {
        width: 70,
        height: 70,
        borderRadius: 62,
        borderWidth: 0
    },
    nameInfo: {
        marginTop: -50
    },
    divider: {
        width: "90%",
        borderWidth: 1,
        borderColor: "#E9ECEF"
    },
    thumb: {
        borderRadius: 4,
        marginVertical: 4,
        alignSelf: "center",
        width: 300,
        height: 300
    }
});
