import React, {Component} from 'react';
import {
  StyleSheet,
  Linking,
  ScrollView,
  TouchableOpacity,
  Dimensions,
  Image,
  Animated,
  View,
} from 'react-native';
import mecanicoApi from '../../../api/mecanico';
import LoadingProgress from '../../../common/LoadingProgress';
import {Block, Text, theme} from 'galio-framework';
import Constants from '../../../common/Constants';
import Geolocation from '@react-native-community/geolocation';
import MapView, {Callout, Marker} from 'react-native-maps';
import Icon from 'react-native-vector-icons/FontAwesome';
import Share from 'react-native-share';
import Feather from 'react-native-vector-icons/Feather';
import {showDangerMsg, showSuccesMsg} from '../../../common/ShowMessages';

import {SCLAlert, SCLAlertButton} from 'react-native-scl-alert';
var {height, width} = Dimensions.get('window');
import { Header} from 'react-native-elements';
export default class NotificacionesEmergencias extends Component {
  constructor(props) {
    super(props);
    this.mecanicoApi = new mecanicoApi();

    this.state = {
      initSplash: true,
      loginSpl: true,
      loading: false,
      reservas: [],
      estado: 'Acpetado',
      show: false,
      localtype: this.props.route.params.localtype,
      reservas: this.props.route.params.reservas,

      poi: {
        latitude: '',
        longitude: '',
      },
      region: {
        latitude: 0,
        longitude: 0,
        latitudeDelta: 0,
        longitudeDelta: 0.04,
      },
    };
    this.RotateValueHolder = new Animated.Value(0);
    this.getLocalidad();
  }
  getLocalidad = async () => {
    await Geolocation.getCurrentPosition(position => {
      const {latitude, longitude} = position.coords;
      this.state.poi.latitude = position.coords.latitude;
      this.state.poi.longitude = position.coords.longitude;
      this.setState({
        region: {
          longitude: longitude,
          latitude: latitude,
          latitudeDelta: 0.029213524352655895,
          longitudeDelta: (width / height) * 0.029213524352655895,
        },
      });
    });
  };

  getKilometros = function(lat1, lon1, lat2, lon2) {
    rad = function(x) {
      return (x * Math.PI) / 180;
    };
    var R = 6378.137; //Radio de la tierra en km
    var dLat = rad(lat2 - lat1);
    var dLong = rad(lon2 - lon1);
    var a =
      Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.cos(rad(lat1)) *
        Math.cos(rad(lat2)) *
        Math.sin(dLong / 2) *
        Math.sin(dLong / 2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c;
    return d.toFixed(0); //Retorna tres decimales
  };
  estado = async (estado, cliente) => {
    var distanc = 0;
    var tiempo =
      this.getKilometros(
        this.state.region.latitude,
        this.state.region.longitude,
        cliente.poi[0].latitude,
        cliente.poi[0].longitude,
      ) * 2.06;
    if (tiempo < 60) {
      distanc = tiempo + ' minutos';
    } else {
      distanc = tiempo / 60 + ' horas';
    }

    try {
      this.setState({loading: true});
      cliente.estado = estado;
      if (estado == 2) {
        this.state.estado = 'Rechazado';
        cliente.content =
          'Mecánico ' + cliente.nombremecanico + ' no disponible';
      } else {
        this.state.estado = 'Acpetada';
        cliente.content =
          'El mecánico ' +
          cliente.nombremecanico +
          ' ha aceptado tu solicitud,                  celular: ' +
          cliente.celular +
          ', llega en:' +
          distanc;
      }
      this.mecanicoApi.respuestaLocal(cliente).then(res => {
        if (res.idReservaMecanico) {
          this.props.navigation.replace('Menu');
          showSuccesMsg(this.state.estado,'Emergencia',);
          this.setState({loading: false});
        } else {
          if (res.status && res.status == 'error') {
            showDangerMsg(res.message,'Error');
            this.setState({loading: false});
          }
        }
      });
    } catch (err) {
        showDangerMsg(err.message,'Error');
    }
  };
  finalizado = async (cliente, estado) => {
    try {
      this.setState({loading: true});
      cliente.estado = estado;

      cliente.content =
        'EL mecanico ' + cliente.nombremecanico + ' Finalizó el servicio ';
      this.mecanicoApi.respuestaLocal(cliente).then(res => {
        if (res) {
          this.setState({loading: false});
          this.props.navigation.replace('Menu');
        } else {
          if (res.status && res.status == 'error') {
            showDangerMsg(res.message,'Error');
            this.setState({loading: false});
          }
        }
      });
    } catch (err) {
        showDangerMsg(err.message,'Error');
    }
  };
  handleOpen = () => {
    this.setState({show: true});
    this.props.navigation.replace('Menu');
  };

  handleClose = () => {
    this.setState({show: false});
    this.props.navigation.replace('Menu');
  };
  render() {
    if (this.state.loading) {
      return <LoadingProgress />;
    }
    if (!this.state.loading) {
      if (!this.state.reservas.status) {
        return (
          <View>
            <Header
              leftComponent={
                <TouchableOpacity
                  onPress={() => {
                    this.props.navigation.navigate('MenuNotificaciones');
                  }}>
                  <Icon
                    name="arrow-left"
                    type="Ionicons"
                    size={30}
                    color="#fff"
                  />
                </TouchableOpacity>
              }
              centerComponent={{text: 'EMERGENCIAS', style: {color: '#fff'}}}
              containerStyle={{
                justifyContent: 'space-around',
                backgroundColor: '#FF9100',
              }}
              rightComponent={
                <TouchableOpacity
                  onPress={() => {
                    this.props.navigation.navigate('Menu');
                  }}>
                  <Icon name="home" type="Ionicons" size={30} color="#fff" />
                </TouchableOpacity>
              }
            />
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                marginLeft: '30%',
                height: '20%',
              }}>
              <View>
                <Image
                  source={require('../../../assets/png/lg.png')}
                  style={styles.imagen}
                />
                <View>
                  <Text
                    style={{
                      fontSize: 18,
                      color: '#617792',
                      fontWeight: 'bold',
                    }}>
                    {' '}
                    MECÁNICA EXPRESS
                  </Text>
                </View>
              </View>
            </View>
            <ScrollView style={{height: '65%', width: '100%'}}>
              <View style={styles.container}>
                <View style={styles.contImage}>
                  <Block flex style={styles.profileCard}>
                    <Block middle style={styles.titulo}>
                      <Text
                        style={{
                          fontSize: 20,
                          color: '#617792',
                          fontWeight: 'bold',
                        }}>
                        {this.state.reservas.nombreemergencia}
                      </Text>
                    </Block>
                    {this.renderBtn(this.state.reservas)}
                    <Block middle style={styles.titulo}>
                      <MapView
                        style={styles.map}
                        mapType={'standard'}
                        initialCamera={{
                          center: {
                            latitude: this.state.reservas.poi[0].latitude,
                            longitude: this.state.reservas.poi[0].longitude,
                          },
                          pitch: 45,
                          heading: 90,
                          altitude: 1000,
                          zoom: 17,
                        }}
                        loadingEnabled
                        loadingIndicatorColor="#666666"
                        loadingBackgroundColor="#eeeeee"
                        showsIndoors
                        showsIndoorLevelPicker
                        zoomEnabled={true}
                        pitchEnabled={true}
                        showsUserLocation={true}
                        followsUserLocation={true}
                        showsCompass={true}
                        showsBuildings={true}
                        showsTraffic={true}
                        showsIndoors={true}
                        initialRegion={this.state.region}
                        zoomEnabled={true}>
                        <Marker coordinate={this.state.reservas.poi[0]}>
                          <Callout>
                            <View>
                              <Text>{this.state.reservas.nommbreusuario}</Text>
                            </View>
                          </Callout>
                        </Marker>
                      </MapView>
                      <View style={{marginTop: 15}} />
                      <Block middle>
                        <Text
                          bold
                          color="#525F7F"
                          size={16}
                          style={{marginBottom: 4}}>
                          {this.state.reservas.estado == 0 ? (
                            <Text
                              bold
                              size={16}
                              color="#32BEDC"
                              style={{marginBottom: 4}}>
                              Pendiente
                            </Text>
                          ) : this.state.reservas.estado == 1 ? (
                            <Text
                              bold
                              size={16}
                              color="#32BEDC"
                              style={{marginBottom: 4}}>
                              Aceptado
                            </Text>
                          ) : this.state.reservas.estado == 2 ? (
                            <Text
                              bold
                              size={16}
                              color="#FF333C"
                              style={{marginBottom: 4}}>
                              Rechazado
                            </Text>
                          ) : (
                            <Text
                              bold
                              size={16}
                              color="#3243DC"
                              style={{marginBottom: 4}}>
                              Confirmado
                            </Text>
                          )}
                        </Text>
                        <Text size={16}>Estado</Text>
                      </Block>
                      <Text
                        style={{
                          fontSize: 15,
                          color: '#617792',
                          fontWeight: 'bold',
                        }}>
                        {this.state.reservas.fecha}
                      </Text>
                      <Text
                        style={{
                          fontSize: 15,
                          color: '#617792',
                          fontWeight: 'bold',
                        }}>
                        {this.state.reservas.hora}
                      </Text>
                      <Text size={16}>Fecha de la solicitud</Text>
                    </Block>
                    <Block style={styles.info}>
                      <Block row space="between">
                        <Block middle>
                          <Image
                            source={{
                              uri:
                                Constants.BASEURI +
                                this.state.reservas.imagenmecanico,
                            }}
                            style={styles.avatar}
                          />
                        </Block>

                        <Block middle>
                          <Text
                            bold
                            size={16}
                            color="#525F7F"
                            style={{marginBottom: 4}}>
                            {this.state.reservas.nombremecanico}
                          </Text>
                          <Text size={16}>Mecánico</Text>
                        </Block>
                        <Block middle>
                          <Text
                            bold
                            size={16}
                            color="#525F7F"
                            style={{marginBottom: 4}}>
                            ${this.state.reservas.precioemergencia}
                          </Text>
                          <Text size={16}>Precio</Text>
                        </Block>
                        <Block middle>
                          <Text
                            bold
                            color="#525F7F"
                            size={16}
                            style={{marginBottom: 4}}>
                            {this.state.reservas.nommbreusuario}
                          </Text>

                          <Text size={16}>Usuario</Text>
                        </Block>
                      </Block>
                    </Block>
                    {this.renderBtnFin(this.state.reservas)}
                  </Block>
                </View>
              </View>
            </ScrollView>
          </View>
        );
      } else {
        return (
          <View style={styles.container}>
            <SCLAlert
              theme="danger"
              show={this.state.show}
              title="Emergencias"
              onRequestClose={this.handleClose}
              cancellable={true}
              subtitle="Actualmente no tienes emergencias por atender">
              <SCLAlertButton theme="danger" onPress={this.handleClose}>
                Atras
              </SCLAlertButton>
            </SCLAlert>
          </View>
        );
      }
    }
  }

  renderBtn(cliente) {
    if (cliente.estado == 0) {
      return (
        <View style={styles.containerdtn}>
          <TouchableOpacity
            style={styles.btnVerEnSitioL}
            onPress={() => this.estado(1, cliente)}>
            <Text style={{textAlign: 'center'}}>ACEPTAR</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.btnVerEnSitioR}
            onPress={() => this.estado(2, cliente)}>
            <Text style={{textAlign: 'center'}}>RECHAZAR</Text>
          </TouchableOpacity>
        </View>
      );
    }
    if (cliente.estado == 1) {
      return (
        <View style={{alignContent: 'center', textAlign: 'center'}}>
          <Text
            style={{
              fontSize: 15,
              color: '#35DC32',
              fontWeight: 'bold',
              textAlign: 'center',
            }}>
            {' '}
            En espera de confirmacion de {cliente.nommbreusuario}
          </Text>
        </View>
      );
    }
    if (cliente.estado == 3) {
      return (
        <View>
          <Text
            style={{
              fontSize: 15,
              color: '#3243DC',
              fontWeight: 'bold',
              textAlign: 'center',
            }}>
            En espera que el cliente {cliente.nommbreusuario} pague el servicio
          </Text>
        </View>
      );
    }
    if (cliente.estado == 4) {
      return (
        <View>
          <Text
            style={{
              fontSize: 15,
              color: '#3243DC',
              fontWeight: 'bold',
              textAlign: 'center',
            }}>
            {' '}
            {cliente.nommbreusuario} aceptó el servicio, dirígete a su
            ubicación, el pago es en efectivo
          </Text>
          <Text
            style={{
              fontSize: 15,
              color: '#3243DC',
              fontWeight: 'bold',
              textAlign: 'center',
            }}
          />
          <View style={{alignContent: 'center', textAlign: 'center'}}>
            {this.state.localtype == 0 ? (
              <View style={{flexDirection: 'row'}}>
                <TouchableOpacity
                  style={styles.btnVerEnSitioL2}
                  onPress={() => {
                    const shareOptions = {
                      title: 'Compartir producto vía',
                      message:
                        'El cliente:' +
                        cliente.nommbreusuario +
                        ' se encuentra en ',
                      url:
                        'https://maps.google.com/?q=' +
                        cliente.poi[0].latitude +
                        ',' +
                        cliente.poi[0].longitude,
                    };
                    Share.open(shareOptions);
                  }}>
                  <Feather name="share-2" size={20} />
                  <Text>Compartir Ubicación</Text>
                </TouchableOpacity>
              </View>
            ) : (
              <View style={{flexDirection: 'row'}}>
                <TouchableOpacity
                  style={styles.btnVerEnSitioL2}
                  onPress={() => {
                    Linking.openURL(
                      'https://maps.google.com/?q=' +
                        cliente.poi[0].latitude +
                        ',' +
                        cliente.poi[0].longitude,
                    );
                  }}>
                  <Feather name="share-2" size={20} />
                  <Text>Google Maps</Text>
                </TouchableOpacity>
              </View>
            )}
          </View>
        </View>
      );
    }
  }

  renderBtnFin(cliente) {
    if (cliente.estado == 4) {
      return (
        <View>
          <View style={{flexDirection: 'row'}}>
            <TouchableOpacity
              style={styles.btnVerEnSitio2}
              onPress={_ => this.finalizado(cliente, 5)}>
              <Text
                style={{fontSize: 14, textAlign: 'center', color: '#1F4997'}}>
                FINALIZAR
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },
  containerdtn: {
    bottom: 0,
    alignItems: 'center',
    flexDirection: 'row',
  },
  btnVerEnSitioL: {
    backgroundColor: '#32DC36',
    borderRadius: 20,
    alignItems: 'center',
    width: '50%',
    padding: '3%',
    color: 'blue',
    borderWidth: 1,
    marginTop: '5%',
    borderColor: '#4982e6',
  },

  btnVerEnSitioL2: {
    backgroundColor: '#52F705',
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    padding: '3%',
    color: 'blue',
    borderWidth: 1,
    borderColor: '#4982e6',
    flexDirection: 'row',
  },
  btnVerEnSitioR: {
    backgroundColor: '#FF333C',
    borderRadius: 20,
    alignItems: 'center',
    width: '50%',
    padding: '3%',
    color: 'blue',
    borderWidth: 1,
    marginTop: '5%',
    borderColor: '#4982e6',
  },

  profile: {
    flex: 1,
  },
  imagen: {
    width: 75,
    height: 75,
    marginLeft: '20%',
  },
  contImage: {
    flex: 1,
    marginTop: 3,
    justifyContent: 'center',
    borderWidth: 2,
    width: '97%',
    height: height + 200,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
  },
  map: {
    width: '95%',
    marginTop: '15%',
    height: '50%',
  },
  headerContainer: {
    flexDirection: 'row',
    padding: 5,
    alignItems: 'flex-start',
  },
  btnVerEnSitio2: {
    backgroundColor: '#F5F6F7',
    borderRadius: 20,
    width: '100%',
    padding: '3%',
    color: 'blue',
    borderWidth: 1,
    marginTop: '5%',
    borderColor: '#4982e6',
  },
  btnVerEnSitio: {
    backgroundColor: '#F5F6F7',
    borderRadius: 20,
    width: '35%',
    padding: '3%',
    color: 'blue',
    borderWidth: 1,
    marginTop: '5%',
    borderColor: '#4982e6',
  },
  carousel: {
    position: 'absolute',
    bottom: 0,
    marginBottom: 48,
  },
  cardContainer: {
    alignSelf: 'center',
    backgroundColor: '#617792',
    height: 630,
    width: 350,
    padding: 50,
    borderRadius: 24,
  },
  cardImage: {
    height: 120,
    width: 300,
    bottom: 0,
    position: 'absolute',
    borderBottomLeftRadius: 24,
    borderBottomRightRadius: 24,
  },
  button: {
    alignSelf: 'center',

    fontSize: 16,
    color: '#fff',
    backgroundColor: '#617792',
    paddingVertical: 10,
    paddingHorizontal: 15,
    textAlign: 'center',
    height: 50,
    width: 350,
    padding: 50,
    borderRadius: 24,
  },
  cardTitle: {
    color: 'white',
    fontSize: 22,
    alignSelf: 'center',
  },
  profile: {
    flex: 1,
  },
  profileContainer: {
    width: '100%',
    height: '50%',
    color: '#617792',
    padding: 0,
    zIndex: 1,
  },
  profileBackground: {
    width: '100%',
    height: 350 / 2,
  },
  profileCard: {
    marginTop: 65,
    borderTopLeftRadius: 6,
    borderTopRightRadius: 6,
    backgroundColor: 1,
    shadowColor: 'black',
    shadowOffset: {200: 0, 200: 0},
    shadowRadius: 8,
    shadowOpacity: 0.2,
  },
  info: {
    paddingHorizontal: 20,
    marginTop: -10,
  },
  titulo: {
    marginTop: -70,
  },
  avatarContainer: {
    position: 'relative',
    marginTop: -25,
  },
  avatar: {
    width: 70,
    height: 70,
    borderRadius: 62,
    borderWidth: 0,
  },
  nameInfo: {
    marginTop: -50,
  },
  divider: {
    width: '90%',
    borderWidth: 1,
    borderColor: '#E9ECEF',
  },
  thumb: {
    borderRadius: 4,
    marginVertical: 4,
    alignSelf: 'center',
    width: 300,
    height: 300,
  },
});
