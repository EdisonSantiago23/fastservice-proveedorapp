import React, {Component} from 'react';
import {
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  Image,
  Animated,
  View,
} from 'react-native';
import mecanicoApi from '../../api/mecanico.js';
import {Block, Text} from 'galio-framework';
import {AirbnbRating} from 'react-native-ratings';
import LoadingProgress from '../../common/LoadingProgress';
var base = 'http://mecanico.balinetsoft.com';
import {Header} from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';

var moment = require('moment'); // require

export default class MenuMecanico extends Component {
  constructor(props) {
    super(props);
    this.mecanicoApi = new mecanicoApi();

    this.state = {
      initSplash: true,
      loginSpl: true,
      loading: true,
      mecanicos: [],
      isnew: this.props.route.params.isnew,

      idLocal: this.props.route.params.idLocal,
    };
    this.RotateValueHolder = new Animated.Value(0);
    this.GetMecanicos(this.state.idLocal);
  }
  updatemecanico = async cliente => {
    try {
      this.setState({loading: true});
      cliente.estado = estado;

      this.mecanicoApi.updatemecanico(cliente).then(res => {
        if (res.idReservaMecanico) {
          this.GetMecanicos(this.state.idLocal);
          Alert.alert('Emergencia', this.state.estado, [{text: 'Cerrar'}]);
          this.setState({loading: false});
        } else {
          if (res.status && res.status == 'error') {
            Alert.alert('Error', res.message, [{text: 'Cerrar'}]);
            this.setState({loading: false});
          }
        }
      });
    } catch (err) {
      Alert.alert('Error', err.message, [{text: 'Cerrar'}]);
    }
  };

  GetMecanicos(idLocal) {
    this.mecanicoApi.GetMecanicos(idLocal).then(res => {
      this.setState({
        mecanicos: res,
      });
      if (res != undefined) {
        this.setState({
          loading: false,
        });
      }
    });
  }
  render() {
    if (this.state.loading) {
      return <LoadingProgress />;
    }
    if (!this.state.loading) {
      return (
        <View>
          <Header
            leftComponent={
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.goBack();
                }}>
                <Icon
                  name="arrow-left"
                  type="Ionicons"
                  size={30}
                  color="#fff"
                />
              </TouchableOpacity>
            }
            centerComponent={{text: 'MIS MECÁNICOS', style: {color: '#fff'}}}
            rightComponent={
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate('Menu');
                }}>
                <Icon name="home" type="Ionicons" size={30} color="#fff" />
              </TouchableOpacity>
            }
            containerStyle={{
              justifyContent: 'space-around',
              backgroundColor: '#FF9100',
            }}
          />

          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              marginLeft: '10%',
            }}>
            <View>
              <Image
                source={require('../../assets/png/lg.png')}
                style={styles.imagen}
              />
              <View>
                <Text
                  style={{fontSize: 17, color: '#617792', fontWeight: 'bold'}}>
                  {' '}
                  MECÁNICA EXPRESS
                </Text>
              </View>
            </View>

            <TouchableOpacity
              style={styles.btnVerEnSitio}
              onPress={() =>
                this.props.navigation.navigate('RegistroMecanico', {
                  idLocal: this.state.idLocal,
                })
              }>
              <Text
                style={{fontSize: 14, textAlign: 'center', color: '#1F4997'}}>
                AGREGAR
              </Text>
            </TouchableOpacity>
          </View>
          <ScrollView style={{width: '100%', height: '65%', marginTop: '5%'}}>
            <View style={styles.container}>
              {!this.state.mecanicos.status ? (
                <View>
                  {this.state.mecanicos.map(mecanico => (
                    <View style={styles.contImage}>
                      <Block flex style={styles.profileCard}>
                        <Block middle style={styles.avatarContainer}>
                          <Text
                            style={{
                              textAlign: 'center',
                              color: '#525F7F',
                              fontSize: 16,
                            }}>
                            Mecánico calificado para el servicio de nuestros
                            clientes
                          </Text>
                          <Image
                            source={{uri: base + mecanico.imagen}}
                            style={styles.avatar}
                          />
                        </Block>
                        <Block style={styles.info}>
                          <Block
                            middle
                            row
                            space="evenly"
                            style={{marginTop: 20, paddingBottom: 24}}
                          />
                          <Block middle style={styles.nameInfo}>
                            <Text bold size={28} color="#32325D">
                              {mecanico.nombre}
                            </Text>
                            <TouchableOpacity
                              style={styles.btnVerEnSitio}
                              onPress={() =>
                                this.props.navigation.navigate(
                                  'ActualizarMecanico',
                                  {mecanico: mecanico},
                                )
                              }>
                              <Text
                                style={{
                                  fontSize: 14,
                                  textAlign: 'center',
                                  color: '#1F4997',
                                }}>
                                Editar
                              </Text>
                            </TouchableOpacity>
                          </Block>

                          <AirbnbRating
                            count={5}
                            isDisabled={true}
                            style={{marginTop: '5%'}}
                            reviews={[
                              'Malo',
                              'Regular',
                              'Normal',
                              'Bueno',
                              'Excelente',
                            ]}
                            defaultRating={mecanico.calificacion}
                            size={36}
                          />
                          <Block middle style={styles.nameInfo}>
                            <Text bold size={18} style={{marginTop: '10%'}}>
                              Calificación
                            </Text>
                          </Block>
                          <Block row space="between">
                            <Block middle>
                              <Text
                                bold
                                size={16}
                                color="#525F7F"
                                style={{marginBottom: 4}}>
                                {mecanico.experiencia}
                              </Text>
                              <Text size={16}>Experiencia</Text>
                            </Block>
                            <Block middle>
                              <Text
                                bold
                                color="#525F7F"
                                size={16}
                                style={{marginBottom: 4}}>
                                {mecanico.calificacion}
                              </Text>

                              <Text size={16}>Calificación</Text>
                            </Block>
                            <Block middle>
                              <Text
                                bold
                                color="#525F7F"
                                size={16}
                                style={{marginBottom: 4}}>
                                {moment().diff(
                                  mecanico.fechanacimiento,
                                  'years',
                                )}
                              </Text>
                              <Text size={16}>Edad</Text>
                            </Block>
                          </Block>
                        </Block>
                        <Block flex>
                          <Block
                            middle
                            style={{marginTop: 30, marginBottom: 16}}>
                            <Block style={styles.divider} />
                          </Block>

                          <Block
                            row
                            style={{
                              paddingVertical: 14,
                              alignItems: 'baseline',
                            }}>
                            <Block middle>
                              <Text
                                bold
                                size={16}
                                color="#525F7F"
                                style={{textAlign: 'center', marginTop: -80}}>
                                Capacitado en:
                              </Text>
                            </Block>
                            <Text
                              style={{
                                textAlign: 'center',
                                marginTop: -100,
                                color: '#525F7F',
                                fontSize: 16,
                              }}>
                              {mecanico.capacitaciones}
                            </Text>
                          </Block>
                          <Block
                            style={{
                              paddingVertical: 30,
                              alignItems: 'baseline',
                            }}>
                            <Block
                              style={{
                                paddingVertical: 14,
                                alignItems: 'baseline',
                              }}>
                              <Block middle>
                                <Text
                                  bold
                                  size={16}
                                  color="#525F7F"
                                  style={{textAlign: 'center', marginTop: -50}}>
                                  Especialista en:
                                </Text>
                              </Block>
                            </Block>
                            <Block
                              style={{
                                paddingVertical: 14,
                                alignItems: 'baseline',
                              }}>
                              <Block middle>
                                <Text
                                  size={16}
                                  color="#525F7F"
                                  style={{textAlign: 'center', marginTop: -40}}>
                                  {mecanico.especialiasta}
                                </Text>
                              </Block>
                            </Block>
                          </Block>
                        </Block>
                      </Block>
                    </View>
                  ))}
                </View>
              ) : (
                <View>
                  <Block middle style={styles.nameInfo}>
                    <Text bold size={28} color="#32325D">
                      Sin Mecánicos
                    </Text>
                  </Block>
                  <Block middle style={styles.nameInfo}>
                    <Text bold size={28} color="#32325D">
                      Registrados
                    </Text>
                  </Block>
                </View>
              )}
            </View>
          </ScrollView>
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    padding: '3%',
  },
  profile: {
    // marginBottom: -HeaderHeight * 2,
    flex: 1,
  },
  imagen: {
    width: 70,
    height: 70,
    marginLeft: '25%',
    marginTop: '5%',
  },
  contImage: {
    marginTop: 3,
    justifyContent: 'center',
    width: '100%',
    borderWidth: 2,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
  headerContainer: {
    flexDirection: 'row',
    padding: 5,
    alignItems: 'flex-start',
  },
  btnVerEnSitio: {
    backgroundColor: '#F5F6F7',
    borderRadius: 20,
    width: '35%',
    padding: '3%',
    color: 'blue',
    borderWidth: 1,
    marginTop: '17%',
    marginLeft: '10%',
    borderColor: '#4982e6',
  },
  carousel: {
    position: 'absolute',
    bottom: 0,
    marginBottom: 48,
  },
  cardContainer: {
    alignSelf: 'center',
    backgroundColor: '#617792',
    height: 600,
    width: 350,
    padding: 50,
    borderRadius: 24,
  },
  cardImage: {
    height: 120,
    width: 300,
    bottom: 0,
    position: 'absolute',
    borderBottomLeftRadius: 24,
    borderBottomRightRadius: 24,
  },
  button: {
    alignSelf: 'center',

    fontSize: 16,
    color: '#fff',
    backgroundColor: '#617792',
    paddingVertical: 10,
    paddingHorizontal: 15,
    textAlign: 'center',
    height: 50,
    width: 350,
    padding: 50,
    borderRadius: 24,
  },
  cardTitle: {
    color: 'white',
    fontSize: 22,
    alignSelf: 'center',
  },
  profile: {
    flex: 1,
  },
  profileContainer: {
    width: '100%',
    height: '50%',
    color: '#617792',
    padding: 0,
    zIndex: 1,
  },
  profileBackground: {
    width: '100%',
    height: 350 / 2,
  },
  profileCard: {
    // position: "relative",
    padding: 1,
    marginHorizontal: 1,
    marginTop: 65,
    borderTopLeftRadius: 6,
    borderTopRightRadius: 6,
    backgroundColor: 1,
    shadowColor: 'black',
    shadowOffset: {200: 0, 200: 0},
    shadowRadius: 8,
    shadowOpacity: 0.2,
    zIndex: 2,
    height: '100%',
  },
  info: {
    paddingHorizontal: 40,
  },
  avatarContainer: {
    position: 'relative',
    marginTop: -60,
  },
  avatar: {
    width: 124,
    height: 124,
    borderRadius: 62,
    borderWidth: 0,
    marginTop: 10,
  },
  nameInfo: {
    marginTop: -15,
  },
  divider: {
    width: '90%',
    borderWidth: 1,
    borderColor: '#E9ECEF',
  },
  thumb: {
    borderRadius: 4,
    marginVertical: 4,
    alignSelf: 'center',
    width: 300,
    height: 300,
  },
});
