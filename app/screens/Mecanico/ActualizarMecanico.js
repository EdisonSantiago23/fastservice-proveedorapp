import React, {Component} from 'react';
import {
  Text,
  Image,
  ScrollView,
  StyleSheet,
  View,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import mecanicoApi from '../../api/mecanico.js';
import ImagePicker from 'react-native-image-picker';
import LoadingProgress from '../../common/LoadingProgress';
import {TextInput} from 'react-native-paper';
import {showDangerMsg, showSuccesMsg} from '../../common/ShowMessages';
import {Header} from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';

export default class ActualizarMecanico extends Component {
  constructor(props) {
    super(props);
    this.mecanicoApi = new mecanicoApi();
    this.state = {
      imageFileData: '',
      loading: false,
      userFeatures: this.props.route.params.mecanico,
      date: new Date(),
      mode: 'date',
      horario: 0,
      fecha: '',
      mfecha: '',
    };
    this.onDateChange = this.onDateChange.bind(this);
  }

  onDateChange(date) {
    this.state.userFeatures.fechanacimiento = date;
  }
  onChangeText = (key, val) => {
    this.setState({[key]: val});
  };

  updateStatusFeature(param, value) {
    let userFeatConst = this.state.userFeatures;
    userFeatConst[param] = value;
    this.setState({userFeatures: userFeatConst});
  }

  validadorCampo() {
    if (
      this.state.userFeatures.nombre != '' &&
      this.state.userFeatures.experiencia != '' &&
      this.state.userFeatures.especialiasta != ''
    ) {
      if (this.state.userFeatures.experiencia > 0) {
        return true;
      } else {
        showDangerMsg('No debe tener campos negativos','Campos erróneos');
        return false;
      }
    } else {
      showDangerMsg('No debe existir campos vacios','Campos erróneos');
      return false;
    }
  }

  createMecanico = async () => {
    try {
      if (this.validadorCampo()) {
        this.setState({loading: true});
        this.mecanicoApi.updatemecanico(this.state.userFeatures).then(res => {
          if (res.idmecanico) {
            showSuccesMsg('Mecánico actualizado correctamente','Mecánico',);

            this.setState({loading: false});
            this.props.navigation.navigate('Menu');
          } else {
            if (res.status && res.status == 'error') {
                showDangerMsg(res.message,'Error');
                this.setState({loading: false});
            }
          }
        });
      }
    } catch (err) {
        showDangerMsg(err.message,'Error');
    }
  };

  render() {
    if (this.state.loading) {
      return <LoadingProgress />;
    }
    if (!this.state.loading) {
      return (
        <View>
          <Header
            leftComponent={
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.goBack();
                }}>
                <Icon
                  name="arrow-left"
                  type="Ionicons"
                  size={30}
                  color="#fff"
                />
              </TouchableOpacity>
            }
            centerComponent={{
              text: 'ACTUALIZAR MECÁNICO',
              style: {color: '#fff'},
            }}
            rightComponent={
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate('Menu');
                }}>
                <Icon name="home" type="Ionicons" size={30} color="#fff" />
              </TouchableOpacity>
            }
            containerStyle={{
              justifyContent: 'space-around',
              backgroundColor: '#FF9100',
            }}
          />
          <ScrollView>
            <View style={styles.container}>
              <View style={{alignItems: 'center'}}>
                <Image
                  source={require('../../assets/png/lg.png')}
                  style={styles.imagen}
                />
                <View>
                  <Text
                    style={{
                      fontSize: 17,
                      color: '#617792',
                      fontWeight: 'bold',
                      textAlign: 'center',
                    }}>
                    MECÁNICA EXPRESS
                  </Text>
                </View>
              </View>

              <View style={styles.container3}>
                <TextInput
                  mode="outlined"
                  ref="zipCode"
                  label="Nombre"
                  theme={{
                    colors: {
                      primary: 'purple',
                      background: 'white',
                    },
                  }}
                  value={this.state.userFeatures.nombre}
                  onChangeText={val => this.updateStatusFeature('nombre', val)}
                  returnKeyType="done"
                />
                <TextInput
                  mode="outlined"
                  ref="zipCode"
                  label="Años de experiencia"
                  keyboardType={'numeric'}
                  theme={{
                    colors: {
                      primary: 'purple',
                      background: 'white',
                    },
                  }}
                  value={this.state.userFeatures.experiencia}
                  onChangeText={val =>
                    this.updateStatusFeature('experiencia', val)
                  }
                  returnKeyType="done"
                />
                <TextInput
                  mode="outlined"
                  ref="zipCode"
                  label="Celular"
                  theme={{
                    colors: {
                      primary: 'purple',
                      background: 'white',
                    },
                  }}
                  value={this.state.userFeatures.celular}
                  keyboardType={'numeric'}
                  maxLength={10}
                  onChangeText={val => this.updateStatusFeature('celular', val)}
                  returnKeyType="done"
                />
                <TextInput
                  mode="outlined"
                  ref="zipCode"
                  label="Especialiasta"
                  theme={{
                    colors: {
                      primary: 'purple',
                      background: 'white',
                    },
                  }}
                  value={this.state.userFeatures.especialiasta}
                  onChangeText={val =>
                    this.updateStatusFeature('especialiasta', val)
                  }
                  returnKeyType="done"
                />
                <TextInput
                  mode="outlined"
                  ref="zipCode"
                  label="Capacitaciones en"
                  theme={{
                    colors: {
                      primary: 'purple',
                      background: 'white',
                    },
                  }}
                  value={this.state.userFeatures.capacitaciones}
                  onChangeText={val =>
                    this.updateStatusFeature('capacitaciones', val)
                  }
                  returnKeyType="done"
                />
              </View>
              <TouchableOpacity
                style={styles.btnVerEnSitio}
                onPress={() => this.createMecanico()}>
                <Text
                  style={{fontSize: 14, textAlign: 'center', color: '#1F4997'}}>
                  Actualizar
                </Text>
              </TouchableOpacity>
            </View>
          </ScrollView>
        </View>
      );
    }
  }

  onChangeFecha = (event, selectedDate) => {
    const aux = selectedDate.toString().substring(4, 16);
    this.setState({
      fecha: selectedDate,
      mfecha: aux,
      horario: 0,
    });
  };
  renderBtn() {
    if (!this.state.loadingBtn) {
      return (
        <View>
          <TouchableOpacity
            onPress={() => {
              this.createMecanico();
            }}
            style={styles.btnStyle}>
            <Text style={{fontSize: 14, textAlign: 'center', color: '#1F4997'}}>
              GUARDAR
            </Text>
          </TouchableOpacity>
        </View>
      );
    } else {
      return (
        <TouchableOpacity style={styles.btnStyle}>
          <ActivityIndicator size="small" color={'#1F4997'} />
        </TouchableOpacity>
      );
    }
  }

  chooseImage(index) {
    let options = {
      title: 'Seleccionar imagen',
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
      cancelButtonTitle: 'Cancelar',
      takePhotoButtonTitle: 'Tomar Foto',
      chooseFromLibraryButtonTitle: 'Elegir Foto de la Galería',
      mediaType: 'photo',
      index: index,
    };
    ImagePicker.showImagePicker(options, response => {
      if (response.didCancel) {
      } else if (response.error) {
      } else if (response.customButton) {
      } else {
        let ministatus = 'data:image/jpg;base64,' + response.data;
        this.setState({
          imageFileData: ministatus,
        });
      }
    });
  }

  renderFileData() {
    if (this.state.userFeatures.path) {
      return (
        <Image
          source={{uri: Constants.BASEURI + this.state.userFeatures.path}}
          style={[
            !this.state.tipo ? styles.imagesfrelance : styles.imagesfrelance,
          ]}
        />
      );
    }
    if (this.state.imageFileData) {
      return (
        <Image
          source={{
            uri: this.state.imageFileData,
          }}
          style={styles.imagesfrelance}
        />
      );
    } else {
      return (
        <View>
          <Image
            source={require('../../assets/png/camera.png')}
            style={{
              width: 50,
              marginLeft: '30%',
              marginTop: '25%',

              height: 50,
            }}
          />
        </View>
      );
    }
  }
}

const rnpickerStyles = StyleSheet.create({
  labelStyle: {textAlign: 'left', minWidth: 60},
  labelStyle2: {textAlign: 'center', minWidth: 60},
  pickerContainer: {
    borderBottomWidth: 1,
    borderBottomColor: '#FFFF',
    flex: 1,
  },
});

const styles = StyleSheet.create({
  btnVerEnSitio: {
    backgroundColor: '#F5F6F7',
    borderRadius: 20,
    alignItems: 'center',
    width: '70%',
    padding: '3%',
    color: 'blue',
    borderWidth: 1,
    marginTop: '5%',
    borderColor: '#4982e6',
  },
  map: {
    width: 180,
    height: 180,
  },

  btnVerEnSitio3: {
    backgroundColor: '#F5F6F7',
    borderRadius: 20,
    alignItems: 'center',
    width: '90%',
    padding: '3%',
    color: 'blue',
    borderWidth: 1,
    marginTop: '5%',
    borderColor: '#4982e6',
  },
  fotoContainerfree: {
    width: 130,
    height: 130,
    borderColor: 'black',
    borderWidth: 1,
  },
  imagesfrelance: {
    width: 130,
    height: 130,
    borderColor: 'black',
    borderWidth: 1,
  },
  container: {
    flex: 1,
    alignItems: 'center',
    height: '100%',
    padding: '3%',
    backgroundColor: '#fff',
  },
  btnVerEnSitio2: {
    backgroundColor: '#F5F6F7',
    borderRadius: 20,
    alignItems: 'center',
    width: '70%',
    padding: '3%',
    color: 'blue',
    borderWidth: 1,
    marginTop: '-10%',
    borderColor: '#4982e6',
  },
  descriptionTag: {
    color: '#502A18',
  },
  btnTextStyle: {
    color: '#FFFFFF',
    fontSize: 22,
    alignItems: 'center',
    textAlign: 'center',
  },
  btnStyle: {
    alignItems: 'center',
    borderWidth: 1,
    borderColor: '#4982e6',
    borderRadius: 20,
    justifyContent: 'center',
    backgroundColor: '#F5F6F7',
    height: 50,
    marginTop: 20,
    marginBottom: 80,
  },
  images: {
    width: 150,
    height: 150,
    borderColor: 'black',
    borderWidth: 1,
  },
  fotoContainer: {
    borderColor: '#424244',
    borderWidth: 1,
    borderStyle: 'dotted',
    backgroundColor: '#F9FCFD',
  },

  container3: {
    width: '100%',
    flexDirection: 'column',
    justifyContent: 'space-around',
    alignItems: 'stretch',
  },
  createAccount: {
    fontSize: 30,
    textAlign: 'left',
    marginVertical: 15,
  },
  inputSearch: {
    borderBottomWidth: 1,
  },
  imagen: {
    width: 70,
    height: 70,
  },
  imagen2: {
    width: 155,
    height: 130,
  },
});
