import React, {Component} from 'react';
import {
  Text,
  Image,
  ScrollView,
  Alert,
  StyleSheet,
  TextInput,
  View,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import mecanicoApi from '../../api/mecanico.js';
import ImagePicker from 'react-native-image-picker';
import Feather from 'react-native-vector-icons/Feather';
import DateTimePicker from '@react-native-community/datetimepicker';
import { Header} from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';

export default class RegistroMecanico extends Component {
  constructor(props) {
    super(props);
    this.mecanicoApi = new mecanicoApi();
    this.state = {
      imageFileData: '',
      loading: true,
      idLocal: this.props.route.params.idLocal,
      date: new Date(),
      mode: 'date',
      horario: 0,
      fecha: '',
      mfecha: '',

      userFeatures: {
        idmecanico: 0,
        cedula: 0,
        nombre: '',
        celular: '',
        fechanacimiento: '',
        experiencia: 0,
        especialiasta: '',
        calificacion: 5,
        capacitaciones: '',
        local_idLocal: this.props.route.params.idLocal,
        url: '',
        estado: 0,
      },
    };
    this.onDateChange = this.onDateChange.bind(this);
  }

  onDateChange(date) {
    this.state.userFeatures.fechanacimiento = date;
  }
  onChangeText = (key, val) => {
    this.setState({[key]: val});
  };

  updateStatusFeature(param, value) {
    let userFeatConst = this.state.userFeatures;
    userFeatConst[param] = value;
    this.setState({userFeatures: userFeatConst});
  }

  validadorCampo() {
    if (
      this.state.userFeatures.nombre != '' &&
      this.state.userFeatures.cedula != '' &&
      this.state.userFeatures.experiencia != '' &&
      this.state.userFeatures.especialiasta != '' &&
      this.state.userFeatures.calificacion != '' &&
      this.state.userFeatures.fechanacimiento != '' &&
      this.state.userFeatures.url != ''
    ) {
      return true;
    } else {
      Alert.alert('Campos erróneos', 'No debe existir campos vacios', [
        {text: 'Cerrar'},
      ]);

      return false;
    }
  }
  validadorCampoFecha() {
    if (
      this.state.userFeatures.nombre != '' &&
      this.state.userFeatures.cedula != '' &&
      this.state.userFeatures.experiencia != '' &&
      this.state.userFeatures.especialiasta != '' &&
      this.state.userFeatures.calificacion != '' &&
      this.state.userFeatures.fechanacimiento != '' &&
      this.state.userFeatures.url != ''
    ) {
      return true;
    } else {
      Alert.alert('Campos erróneos', 'No debe existir campos vacios', [
        {text: 'Cerrar'},
      ]);

      return false;
    }
  }
  validateCedula = cedula => {
    var cedula = cedula;
    //Preguntamos si la cedula consta de 10 digitos
    if (cedula.length == 10) {
      //Obtenemos el digito de la region que sonlos dos primeros digitos
      var digito_region = cedula.substring(0, 2);

      //Pregunto si la region existe ecuador se divide en 24 regiones
      if (digito_region >= 1 && digito_region <= 24) {
        // Extraigo el ultimo digito
        var ultimo_digito = cedula.substring(9, 10);

        //Agrupo todos los pares y los sumo
        var pares =
          parseInt(cedula.substring(1, 2)) +
          parseInt(cedula.substring(3, 4)) +
          parseInt(cedula.substring(5, 6)) +
          parseInt(cedula.substring(7, 8));

        //Agrupo los impares, los multiplico por un factor de 2, si la resultante es > que 9 le restamos el 9 a la resultante
        var numero1 = cedula.substring(0, 1);
        var numero1 = numero1 * 2;
        if (numero1 > 9) {
          var numero1 = numero1 - 9;
        }

        var numero3 = cedula.substring(2, 3);
        var numero3 = numero3 * 2;
        if (numero3 > 9) {
          var numero3 = numero3 - 9;
        }

        var numero5 = cedula.substring(4, 5);
        var numero5 = numero5 * 2;
        if (numero5 > 9) {
          var numero5 = numero5 - 9;
        }

        var numero7 = cedula.substring(6, 7);
        var numero7 = numero7 * 2;
        if (numero7 > 9) {
          var numero7 = numero7 - 9;
        }

        var numero9 = cedula.substring(8, 9);
        var numero9 = numero9 * 2;
        if (numero9 > 9) {
          var numero9 = numero9 - 9;
        }

        var impares = numero1 + numero3 + numero5 + numero7 + numero9;

        //Suma total
        var suma_total = pares + impares;

        //extraemos el primero digito
        var primer_digito_suma = String(suma_total).substring(0, 1);

        //Obtenemos la decena inmediata
        var decena = (parseInt(primer_digito_suma) + 1) * 10;

        //Obtenemos la resta de la decena inmediata - la suma_total esto nos da el digito validador
        var digito_validador = decena - suma_total;

        //Si el digito validador es = a 10 toma el valor de 0
        if (digito_validador == 10) var digito_validador = 0;

        //Validamos que el digito validador sea igual al de la cedula
        if (digito_validador == ultimo_digito) {
          return true;
        } else {
          Alert.alert(
            'Cédula errónea',
            'la cedula:' + cedula + ' es incorrecta',
            [{text: 'Cerrar'}],
          );
          return false;
        }
      } else {
        // imprimimos en consola si la region no pertenece
        Alert.alert(
          'Cédula errónea',
          'Esta cedula no pertenece a ninguna region',
          [{text: 'Cerrar'}],
        );
        return false;
      }
    } else {
      //imprimimos en consola si la cedula tiene mas o menos de 10 digitos
      Alert.alert('Cédula errónea', 'Esta cedula tiene menos de 10 Digitos', [
        {text: 'Cerrar'},
      ]);
      return false;
    }
  };
  createMecanico = async () => {
    this.state.userFeatures.url = this.state.imageFileData;
    this.state.userFeatures.fechanacimiento = this.state.fecha;
    try {
      if (
        this.validadorCampo() &&
        this.validateCedula(this.state.userFeatures.cedula)
      ) {
        this.setState({loadingBtn: true});
        this.mecanicoApi.createMecanico(this.state.userFeatures).then(res => {
          if (res.idmecanico) {
            Alert.alert('Mecánico', 'Registro exitoso', [{text: 'Cerrar'}]);
            this.setState({loadingBtn: false});
            this.props.navigation.navigate('Menu');
          } else {
            if (res.status && res.status == 'error') {
              Alert.alert('Error', res.message, [{text: 'Cerrar'}]);
              this.setState({loadingBtn: false});
            }
          }
        });
      }
    } catch (err) {
      Alert.alert('Error', err.message, [{text: 'Cerrar'}]);
    }
  };

  render() {
    return (
      <View>
        <Header
          leftComponent={
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.goBack();
              }}>
              <Icon name="arrow-left" type="Ionicons" size={30} color="#fff" />
            </TouchableOpacity>
          }
          centerComponent={{
            text: 'REGISTRO DE MECÁNICO',
            style: {color: '#fff'},
          }}
          rightComponent={
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate('Menu');
              }}>
              <Icon name="home" type="Ionicons" size={30} color="#fff" />
            </TouchableOpacity>
          }
          containerStyle={{
            justifyContent: 'space-around',
            backgroundColor: '#FF9100',
          }}
        />
        <ScrollView>
          <View style={styles.container}>
            <View style={{alignItems: 'center'}}>
              <Image
                source={require('../../assets/png/lg.png')}
                style={styles.imagen}
              />
              <View>
                <Text
                  style={{
                    fontSize: 17,
                    color: '#617792',
                    fontWeight: 'bold',
                    textAlign: 'center',
                  }}>
                  MECÁNICA EXPRESS
                </Text>
              </View>
            </View>

            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <View style={styles.containe}>
                <Text
                  style={{
                    fontSize: 15,
                    color: '#617792',
                    marginTop: '10%',
                    fontWeight: 'bold',
                    textAlign: 'center',
                  }}>
                  REGISTRO
                </Text>
                <TouchableOpacity
                  style={styles.fotoContainerfree}
                  onPress={() => this.chooseImage()}>
                  {this.renderFileData()}
                </TouchableOpacity>
                <Text
                  style={{
                    fontSize: 17,
                    color: '#617792',
                    fontWeight: 'bold',
                    textAlign: 'center',
                  }}>
                  Foto
                </Text>
              </View>
            </View>
            <View style={styles.ellanoteama}>
              <TextInput
                placeholder="Nombre"
                onChangeText={val => this.updateStatusFeature('nombre', val)}
                style={styles.inputSearch}
                value={this.state.userFeatures.nombre}
              />
              <TextInput
                placeholder="Años de experiencia"
                onChangeText={val =>
                  this.updateStatusFeature('experiencia', val)
                }
                style={styles.inputSearch}
                keyboardType={'phone-pad'}
                maxLength={2}
                value={this.state.userFeatures.experiencia}
              />
              <TextInput
                placeholder="Cédula"
                onChangeText={val => this.updateStatusFeature('cedula', val)}
                style={styles.inputSearch}
                keyboardType={'phone-pad'}
                maxLength={10}
                value={this.state.userFeatures.cedula}
              />
              <TextInput
                placeholder="Celular"
                onChangeText={val => this.updateStatusFeature('celular', val)}
                style={styles.inputSearch}
                keyboardType={'phone-pad'}
                maxLength={10}
                value={this.state.userFeatures.celular}
              />
              <TextInput
                placeholder="Especialiasta"
                onChangeText={val =>
                  this.updateStatusFeature('especialiasta', val)
                }
                style={styles.inputSearch}
                value={this.state.userFeatures.especialiasta}
              />
              <TextInput
                placeholder="Capacitaciones"
                onChangeText={val =>
                  this.updateStatusFeature('capacitaciones', val)
                }
                style={styles.inputSearch}
                value={this.state.userFeatures.capacitaciones}
              />
              <Text>Fecha de nacimiento:</Text>
              <View style={{flexDirection: 'row'}}>
                <TouchableOpacity
                  onPress={() => this.setState({horario: 1})}
                  style={styles.btnVerEnSitio}>
                  <View style={{flexDirection: 'row'}}>
                    <Feather name="calendar" color="green" size={20} />
                    <Text
                      style={{
                        fontSize: 15,
                        fontWeight: 'bold',
                        textAlign: 'center',
                      }}>
                      Fecha
                    </Text>
                  </View>
                </TouchableOpacity>
                <Text
                  style={{
                    fontSize: 15,
                    fontWeight: 'bold',
                    textAlign: 'center',
                    marginTop: '7%',
                  }}>
                  {this.state.mfecha}
                </Text>
              </View>
              {this.hora()}

              {this.renderBtn()}
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
  hora() {
    if (this.state.horario == 1) {
      return (
        <View>
          <DateTimePicker
            testID="dateTimePicker"
            timeZoneOffsetInMinutes={0}
            minuteInterval={this.state.interval}
            value={this.state.date}
            mode="date"
            is24Hour
            display="default"
            onChange={this.onChangeFecha}
            style={styles.iOsPicker}
          />
        </View>
      );
    }
  }
  onChangeFecha = (event, selectedDate) => {
    const aux = selectedDate.toString().substring(4, 16);
    this.setState({
      fecha: selectedDate,
      mfecha: aux,
      horario: 0,
    });
  };
  renderBtn() {
    if (!this.state.loadingBtn) {
      return (
        <View>
          <TouchableOpacity
            onPress={() => {
              this.createMecanico();
            }}
            style={styles.btnStyle}>
            <Text style={{fontSize: 14, textAlign: 'center', color: '#1F4997'}}>
              GUARDAR
            </Text>
          </TouchableOpacity>
        </View>
      );
    } else {
      return (
        <TouchableOpacity style={styles.btnStyle}>
          <ActivityIndicator size="small" color={'#1F4997'} />
        </TouchableOpacity>
      );
    }
  }

  chooseImage(index) {
    let options = {
      title: 'Seleccionar imagen',
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
      cancelButtonTitle: 'Cancelar',
      takePhotoButtonTitle: 'Tomar Foto',
      chooseFromLibraryButtonTitle: 'Elegir Foto de la Galería',
      mediaType: 'photo',
      index: index,
    };
    ImagePicker.showImagePicker(options, response => {
      if (response.didCancel) {
      } else if (response.error) {
      } else if (response.customButton) {
        alert(response.customButton);
      } else {
        let ministatus = 'data:image/jpg;base64,' + response.data;
        this.setState({
          imageFileData: ministatus,
        });
      }
    });
  }

  renderFileData() {
    if (this.state.userFeatures.path) {
      return (
        <Image
          source={{uri: Constants.BASEURI + this.state.userFeatures.path}}
          style={[
            !this.state.tipo ? styles.imagesfrelance : styles.imagesfrelance,
          ]}
        />
      );
    }
    if (this.state.imageFileData) {
      return (
        <Image
          source={{
            uri: this.state.imageFileData,
          }}
          style={styles.imagesfrelance}
        />
      );
    } else {
      return (
        <View>
          <Image
            source={require('../../assets/png/camera.png')}
            style={{
              width: 50,
              marginLeft: '30%',
              marginTop: '25%',

              height: 50,
            }}
          />
        </View>
      );
    }
  }
}

const rnpickerStyles = StyleSheet.create({
  labelStyle: {textAlign: 'left', minWidth: 60},
  labelStyle2: {textAlign: 'center', minWidth: 60},
  pickerContainer: {
    borderBottomWidth: 1,
    borderBottomColor: '#FFFF',
    flex: 1,
  },
});

const styles = StyleSheet.create({
  btnVerEnSitio: {
    backgroundColor: '#F5F6F7',
    borderRadius: 20,
    alignItems: 'center',
    width: '70%',
    padding: '3%',
    color: 'blue',
    borderWidth: 1,
    marginTop: '5%',
    borderColor: '#4982e6',
  },
  map: {
    width: 180,
    height: 180,
  },

  btnVerEnSitio3: {
    backgroundColor: '#F5F6F7',
    borderRadius: 20,
    alignItems: 'center',
    width: '90%',
    padding: '3%',
    color: 'blue',
    borderWidth: 1,
    marginTop: '5%',
    borderColor: '#4982e6',
  },
  fotoContainerfree: {
    width: 130,
    height: 130,
    borderColor: 'black',
    borderWidth: 1,
  },
  imagesfrelance: {
    width: 130,
    height: 130,
    borderColor: 'black',
    borderWidth: 1,
  },
  container: {
    flex: 1,
    alignItems: 'center',
    height: '100%',
    padding: '3%',
    backgroundColor: '#fff',
  },
  btnVerEnSitio2: {
    backgroundColor: '#F5F6F7',
    borderRadius: 20,
    alignItems: 'center',
    width: '70%',
    padding: '3%',
    color: 'blue',
    borderWidth: 1,
    marginTop: '-10%',
    borderColor: '#4982e6',
  },
  descriptionTag: {
    color: '#502A18',
  },
  btnTextStyle: {
    color: '#FFFFFF',
    fontSize: 22,
    alignItems: 'center',
    textAlign: 'center',
  },
  btnStyle: {
    alignItems: 'center',
    borderWidth: 1,
    borderColor: '#4982e6',
    borderRadius: 20,
    justifyContent: 'center',
    backgroundColor: '#F5F6F7',
    height: 50,
    marginTop: 20,
    marginBottom: 80,
  },
  images: {
    width: 150,
    height: 150,
    borderColor: 'black',
    borderWidth: 1,
  },
  fotoContainer: {
    borderColor: '#424244',
    borderWidth: 1,
    borderStyle: 'dotted',
    backgroundColor: '#F9FCFD',
  },

  ellanoteama: {
    minWidth: '80%',
    backgroundColor: '#fff',
  },
  createAccount: {
    fontSize: 30,
    textAlign: 'left',
    marginVertical: 15,
  },
  inputSearch: {
    borderBottomWidth: 1,
  },
  imagen: {
    width: 70,
    height: 70,
  },
  imagen2: {
    width: 155,
    height: 130,
  },
});
