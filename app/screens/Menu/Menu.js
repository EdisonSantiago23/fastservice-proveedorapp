import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  View,
  TouchableOpacity,
  StatusBar,
  Image,
  Animated,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Badge, Header} from 'react-native-elements';
import mecanicoApi from '../../api/mecanico.js';
import AsyncStorage from '@react-native-community/async-storage';
import Constants from '../../../app/common/Constants';
import OneSignal from 'react-native-onesignal'; // Import package from node modules
import Feather from 'react-native-vector-icons/Feather';
import LoadingProgress from '../../common/LoadingProgress';
import {SCLAlert, SCLAlertButton} from 'react-native-scl-alert';
export default class Menu extends Component {
  constructor(props) {
    super(props);
    this.mecanicoApi = new mecanicoApi();
    OneSignal.setLogLevel(6, 0);

    // Replace 'YOUR_ONESIGNAL_APP_ID' with your OneSignal App ID.
    OneSignal.init('f153a90b-c14b-44a2-9de8-6a95a26f132f', {
      kOSSettingsKeyAutoPrompt: false,
      kOSSettingsKeyInAppLaunchURL: false,
      kOSSettingsKeyInFocusDisplayOption: 2,
    });
    OneSignal.inFocusDisplaying(2); // Controls what should happen if a notification is received while the app is open. 2 means that the notification will go directly to the device's notification center.

    // The promptForPushNotifications function code will show the iOS push notification prompt. We recommend removing the following code and instead using an In-App Message to prompt for notification permission (See step below)
    //  OneSignal.promptForPushNotificationsWithUserResponse(myiOSPromptCallback);

    OneSignal.addEventListener('received', this.onReceived);
    OneSignal.addEventListener('opened', this.onOpened);
    OneSignal.addEventListener('ids', this.onIds);
    this.state = {
      initSplash: true,
      loginSpl: true,
      local: [],
      visible: false,
      contador: [],
      loading: true,
    };
    this.RotateValueHolder = new Animated.Value(0);
    this.salir = this.salir.bind(this);
    this.cancelar = this.cancelar.bind(this);
    const unsubscribe = this.props.navigation.addListener('focus', () => {
      this.contadorgeneral();
    });
    this.getLocal();
  }

  contadorgeneral = async () => {
    this.mecanicoApi
      .contadorgeneral(
        await AsyncStorage.getItem(Constants.STORE_ID_USER),
        this.state.local.localtype,
      )
      .then(res => {
        this.setState({
          contador: res[0],
        });
        this.setState({loading: false});
      });
  };
  getLocal = async () => {
    this.setState({loading: true});
    this.mecanicoApi
      .getLocal(await AsyncStorage.getItem(Constants.STORE_ID_USER))
      .then(res => {
        this.setState({
          local: res,
        });
        this.setState({loading: false});
        this.contadorgeneral();
      });
  };
  componentWillUnmount() {
    OneSignal.removeEventListener('received', this.onReceived);
    OneSignal.removeEventListener('opened', this.onOpened);
    OneSignal.removeEventListener('ids', this.onIds);
  }

  onReceived(notification) {}

  onOpened(openResult) {}

  onIds = async device => {
    var tokenlocal = JSON.parse(
      await AsyncStorage.getItem(Constants.STORE_ID_TOKEN),
    );

    this.state.tocken = device.userId;
    if (tokenlocal != device.userId) {
      AsyncStorage.removeItem(Constants.STORE_ID_TOKEN);
      var user = await AsyncStorage.getItem(Constants.STORE_ID_USER);
      this.mecanicoApi.updatetoken(user, device.userId).then(res => {
        if (res) {
          AsyncStorage.setItem(
            Constants.STORE_ID_TOKEN,
            JSON.stringify(res.tocken),
          );
        }
      });
    }
  };
  salir() {
    this.setState({visible: !this.state.visible});
    AsyncStorage.removeItem(Constants.STORE_ID_USER);
    AsyncStorage.removeItem(Constants.STORE_ID_TOKEN);
    this.props.navigation.replace('Login');
  }
  cancelar() {
    this.setState({visible: !this.state.visible});
  }
  render() {
    if (this.state.loading) {
      return <LoadingProgress />;
    }
    if (!this.state.loading) {
      const {panel2, panel2x, touchable} = style;
      return (
        <View style={style.container}>
          <StatusBar backgroundColor="#FF9100" barStyle="light-content" />

          <SCLAlert
            theme="warning"
            show={this.state.visible}
            title="MECÁNICA EXPRESS"
            onRequestClose={this.cancelar}
            cancellable={true}
            subtitle="Deseas salir de la app?">
            <SCLAlertButton theme="danger" onPress={this.salir}>
              Aceptar
            </SCLAlertButton>
            <SCLAlertButton theme="info" onPress={this.cancelar}>
              Cancelar
            </SCLAlertButton>
          </SCLAlert>
          <Header
            centerComponent={{
              text: this.state.local.nombre,
              style: {color: '#fff', fontSize: 25, fontWeight: 'bold'},
            }}
            rightComponent={
              <TouchableOpacity
                onPress={() => {
                  this.setState({visible: !this.state.visible});
                }}>
                <Icon name="power-off" type="Ionicons" size={30} color="#fff" />
              </TouchableOpacity>
            }
            containerStyle={{
              justifyContent: 'space-around',
              backgroundColor: '#FF9100',
            }}
          />

          <View style={[{flex: 1}, style.elementsContainer]}>
            <View style={{flex: 2}}>
              <TouchableOpacity
                style={[
                  panel2x,
                  {
                    marginTop: 4,
                    marginRight: 4,
                    alignItems: 'center',
                    marginBottom: 4,
                    marginLeft: 8,
                  },
                ]}
                onPress={() => this.props.navigation.navigate('Informacion')}>
                <Text
                  style={{fontSize: 20, fontWeight: 'bold', color: '#617792'}}>
                  MECÁNICA EXPRESS
                </Text>

                <Image
                  style={{height: '40%', width: '100%', resizeMode: 'contain'}}
                  source={require('../../assets/png/lg.png')}
                  resizeMode="contain"
                  resizeMethod="resize"
                />

                <Text
                  style={{
                    fontSize: 20,
                    color: '#617792',
                    fontWeight: 'bold',
                    alignItems: 'center',
                  }}>
                  INFORMACIÓN
                </Text>
              </TouchableOpacity>
            </View>

            <View style={{flex: 3}}>
              <View style={panel2}>
                <TouchableOpacity
                  style={[
                    panel2x,
                    {
                      marginTop: 4,
                      marginRight: 4,
                      marginBottom: 4,
                      marginLeft: 8,
                    },
                  ]}
                  onPress={() =>
                    this.props.navigation.navigate('MenuContabilidad', {
                      localtype: this.state.local,
                    })
                  }>
                  <Image
                    style={{
                      height: '40%',
                      width: '100%',
                      resizeMode: 'contain',
                    }}
                    source={require('../../assets/png/home/emergencia.png')}
                    resizeMode="contain"
                    resizeMethod="resize"
                  />
                  <Text
                    style={{
                      fontSize: 20,
                      color: '#617792',
                      fontWeight: 'bold',
                      textAlign: 'center',
                    }}>
                    MI CONTABILIDAD
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={[
                    panel2x,
                    {
                      marginTop: 4,
                      marginRight: 8,
                      marginBottom: 4,
                      marginLeft: 4,
                    },
                  ]}
                  onPress={() =>
                    this.props.navigation.navigate('MenuNotificaciones', {
                      localtype: this.state.local.localtype,
                    })
                  }>
                  <Image
                    style={{
                      height: '50%',
                      width: '100%',
                      resizeMode: 'contain',
                      marginTop: '10%',
                    }}
                    resizeMode="contain"
                    resizeMethod="resize"
                    source={require('../../assets/png/home/mantenimiento.png')}
                  />
                  {this.state.contador.contadorgeneral > 0 ? (
                    <Badge
                      status="error"
                      style={{scaleX: 0.7, scaleY: 0.7}}
                      value={this.state.contador.contadorgeneral}
                      width={30}
                      containerStyle={{
                        position: 'absolute',
                        top: -4,
                        right: -4,
                      }}
                    />
                  ) : null}
                  <Text
                    style={{
                      fontSize: 20,
                      color: '#617792',
                      fontWeight: 'bold',
                      textAlign: 'center',
                    }}>
                    MIS NOTIFICACIONES
                  </Text>
                </TouchableOpacity>
              </View>
              {this.state.local.localtype == 0 ? (
                <View style={panel2}>
                  <TouchableOpacity
                    style={[
                      panel2x,
                      {
                        marginTop: 4,
                        marginRight: 4,
                        marginBottom: 4,
                        marginLeft: 8,
                      },
                    ]}
                    onPress={() =>
                      this.props.navigation.navigate('MenuMecanico', {
                        idLocal: this.state.local.idLocal,
                      })
                    }>
                    <Image
                      style={{
                        height: '40%',
                        width: '100%',
                        resizeMode: 'contain',
                      }}
                      resizeMode="contain"
                      resizeMethod="resize"
                      source={require('../../assets/png/home/repuesto.png')}
                    />
                    <Text
                      style={{
                        fontSize: 20,
                        color: '#617792',
                        fontWeight: 'bold',
                        textAlign: 'center',
                      }}>
                      MIS MECÁNICOS
                    </Text>
                  </TouchableOpacity>

                  <TouchableOpacity
                    style={[
                      panel2x,
                      {
                        marginTop: 4,
                        marginRight: 8,
                        marginBottom: 4,
                        marginLeft: 4,
                      },
                    ]}
                    onPress={() =>
                      this.props.navigation.navigate('MisNovedades', {
                        idLocal: this.state.local.idLocal,
                      })
                    }>
                    <Image
                      style={{
                        height: '50%',
                        width: '100%',
                        resizeMode: 'contain',
                      }}
                      resizeMode="contain"
                      resizeMethod="resize"
                      source={require('../../assets/png/home/nuevamoto.png')}
                    />
                    <Text
                      style={{
                        fontSize: 20,
                        color: '#617792',
                        fontWeight: 'bold',
                        textAlign: 'center',
                      }}>
                      MIS NOVEDADES
                    </Text>
                  </TouchableOpacity>
                </View>
              ) : (
                <View
                  style={{
                    flex: 1,
                    backgroundColor: '#F2F9FA',
                    marginTop: 4,
                    marginRight: 8,
                    marginBottom: 8,
                    marginLeft: 8,
                  }}>
                  <TouchableOpacity
                    style={touchable}
                    onPress={() =>
                      this.props.navigation.navigate('MisNovedades', {
                        idLocal: this.state.local.idLocal,
                      })
                    }>
                    <View
                      style={[
                        panel2,
                        {marginLeft: 4, marginRight: 4, marginBottom: 4},
                      ]}>
                      <View style={[panel2x, {alignItems: 'center'}]}>
                        <Image
                          style={{
                            height: '50%',
                            width: '100%',
                            resizeMode: 'contain',
                          }}
                          resizeMode="contain"
                          resizeMethod="resize"
                          source={require('../../assets/png/home/nuevamoto.png')}
                        />
                        <Text
                          style={{
                            fontSize: 20,
                            color: '#617792',
                            fontWeight: 'bold',
                            textAlign: 'center',
                          }}>
                          MIS NOVEDADES
                        </Text>
                      </View>
                    </View>
                  </TouchableOpacity>
                </View>
              )}
            </View>

            <View
              style={{
                flex: 1,
                backgroundColor: '#F2F9FA',
                marginTop: 4,
                marginRight: 8,
                marginBottom: 8,
                marginLeft: 8,
              }}>
              <TouchableOpacity
                style={touchable}
                onPress={() => this.props.navigation.navigate('MenuCitas')}>
                <View
                  style={[
                    panel2,
                    {marginLeft: 4, marginRight: 4, marginBottom: 4},
                  ]}>
                  <View style={[panel2x, {alignItems: 'center'}]}>
                    <Feather name="calendar" color="#FF9100" size={50} />
                    <Text
                      style={{
                        fontSize: 20,
                        color: '#617792',
                        fontWeight: 'bold',
                      }}>
                      MIS CITAS
                    </Text>
                  </View>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      );
    }
  }
}

const style = StyleSheet.create({
  container: {
    flex: 1,
  },
  btnVerEnSitio: {
    backgroundColor: '#F5F6F7',
    borderRadius: 20,
    alignItems: 'center',
    width: '40%',
    padding: '3%',
    color: 'blue',
    borderWidth: 1,
    marginTop: '30%',
    borderColor: '#4982e6',
  },
  headerStyle: {
    fontSize: 36,
    textAlign: 'center',
    fontWeight: '100',
    marginBottom: 24,
  },
  elementsContainer: {
    backgroundColor: '#fff',
    margin: 1,
  },

  panel1: {
    flex: 1,
    justifyContent: 'center',
  },
  panel2: {
    flex: 1,
    flexDirection: 'row',
  },
  panel2x: {
    flex: 1,
    justifyContent: 'center',
    elevation: 6,
    borderRadius: 10,
    backgroundColor: '#ffffff',
    padding: 10,
    marginHorizontal: 10,
    marginVertical: 10,
  },
  textTitle: {
    fontSize: 15,
  },
  touchable: {
    flex: 1,
  },
  colorFondo: {
    backgroundColor: '#617792',
    alignItems: 'center',
  },
});
