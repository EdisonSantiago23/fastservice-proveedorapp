import React, {Component} from 'react';
import {
  StyleSheet,
  Dimensions,
  StatusBar,
  ScrollView,
  Image,
  TouchableOpacity,
} from 'react-native';
import {Block, Text, theme} from 'galio-framework';
import {View} from 'react-native-animatable';
const {width, height} = Dimensions.get('screen');
const StatusHeight = StatusBar.currentHeight;
const HeaderHeight = theme.SIZES.BASE * 3.5 + (StatusHeight || 0);
const thumbMeasure = (width - 48 - 32) / 3;
import { Header} from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';

export default class Informacion extends Component {
  constructor(props) {
    super(props);
    this.state = {
      initSplash: true,
      loginSpl: true,
    };
  }

  render() {
    return (
      <View
        style={styles.profileContainer}
        imageStyle={styles.profileBackground}>
        <StatusBar backgroundColor="#FF9100" barStyle="light-content" />

        <Header
          leftComponent={
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate('Menu');
              }}>
              <Icon name="arrow-left" type="Ionicons" size={30} color="#fff" />
            </TouchableOpacity>
          }
          centerComponent={{text: 'INFORMACIÓN', style: {color: '#fff'}}}
          rightComponent={
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate('Menu');
              }}>
              <Icon name="home" type="Ionicons" size={30} color="#fff" />
            </TouchableOpacity>
          }
          containerStyle={{
            justifyContent: 'space-around',
            backgroundColor: '#FF9100',
          }}
        />
        <Block middle style={styles.avatarContainer}>
          <Image
            source={require('../../assets/png/lg.png')}
            style={styles.avatar}
          />
          <Text
            size={20}
            color="#525F7F"
            style={{textAlign: 'center', marginTop: 10}}>
            MECÁNICA EXPRESS
          </Text>
        </Block>
        <Block flex style={styles.profileCard}>
          <ScrollView style={{width: '100%', height: '90%'}}>
            <Block flex>
              <Block middle style={{marginTop: 1, marginBottom: 16}}>
                <Block style={styles.divider} />
              </Block>
              <Block middle style={styles.nameInfo}>
                <Text
                  bold
                  size={20}
                  color="#32325D"
                  style={{textAlign: 'justify'}}>
                  Somos una empresa con amplia experiencia, dedicada a la
                  reparación y comercialización de repuestos y accesorios para
                  motocicletas, cuadraciclos y equipo agrícola, utilizando
                  siempre los implementos de las marcas más reconocidas del
                  mercado, tanto para uso industrial como doméstico.
                </Text>
              </Block>
              <Block middle style={{marginTop: 30, marginBottom: 16}}>
                <Block style={styles.divider} />
              </Block>
            </Block>
          </ScrollView>

          <TouchableOpacity
            style={styles.btnVerEnSitio}
            onPress={() => {
              this.props.navigation.navigate('MenuAuxilioMotos');
            }}>
            <Text style={{fontSize: 14, textAlign: 'center', color: '#1F4997'}}>
              Atras
            </Text>
          </TouchableOpacity>
        </Block>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
  },
  profile: {
    marginTop: Platform.OS === 'android' ? -HeaderHeight : 0,
    // marginBottom: -HeaderHeight * 2,
    flex: 1,
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
  headerContainer: {
    flexDirection: 'row',
    padding: 5,
    alignItems: 'flex-start',
  },
  btnVerEnSitio: {
    backgroundColor: '#F5F6F7',
    borderRadius: 20,
    width: '100%',
    padding: '3%',
    color: 'blue',
    borderWidth: 1,
    borderColor: '#4982e6',
  },
  carousel: {
    position: 'absolute',
    bottom: 0,
    marginBottom: 48,
  },
  cardContainer: {
    alignSelf: 'center',
    backgroundColor: '#617792',
    height: 600,
    width: 350,
    padding: 50,
    borderRadius: 24,
  },
  cardImage: {
    height: 120,
    width: 300,
    bottom: 0,
    position: 'absolute',
    borderBottomLeftRadius: 24,
    borderBottomRightRadius: 24,
  },
  button: {
    alignSelf: 'center',

    fontSize: 16,
    color: '#fff',
    backgroundColor: '#617792',
    paddingVertical: 10,
    paddingHorizontal: 15,
    textAlign: 'center',
    height: 50,
    width: 350,
    padding: 50,
    borderRadius: 24,
  },
  cardTitle: {
    color: 'white',
    fontSize: 22,
    alignSelf: 'center',
  },
  profile: {
    marginTop: Platform.OS === 'android' ? -HeaderHeight : 0,
    // marginBottom: -HeaderHeight * 2,
    flex: 1,
  },
  profileContainer: {
    width: '100%',
    height: '100%',
    color: '#617792',
    padding: 0,
    zIndex: 1,
  },
  profileBackground: {
    width: width,
    height: height / 2,
  },
  profileCard: {
    // position: "relative",
    padding: theme.SIZES.BASE,
    marginHorizontal: theme.SIZES.BASE,
    marginTop: 10,
    borderTopLeftRadius: 6,
    borderTopRightRadius: 6,
    backgroundColor: theme.COLORS.WHITE,
    shadowColor: 'black',
    shadowOffset: {width: 0, height: 0},
    shadowRadius: 8,
    shadowOpacity: 0.2,
    zIndex: 2,
  },
  info: {
    paddingHorizontal: 40,
  },
  avatarContainer: {
    position: 'relative',
    marginTop: '5%',
  },
  avatar: {
    width: 100,
    height: 100,
    borderRadius: 62,
    borderWidth: 0,
  },
  nameInfo: {
    marginTop: 35,
    textAlign: 'center',
  },
  divider: {
    width: '90%',
    borderWidth: 1,
    borderColor: '#E9ECEF',
  },
  thumb: {
    borderRadius: 4,
    marginVertical: 4,
    alignSelf: 'center',
    width: thumbMeasure,
    height: thumbMeasure,
  },
});
