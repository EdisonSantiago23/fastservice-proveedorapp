import { Alert } from 'react-native';

export default class mecanico {
  version = 'v1';
  constructor() { }
  static getQueryURL = version => {
    //let baseUrl = 'http://192.168.1.6:80/mecanico/public/api/';
    let baseUrl = 'http://mecanico.balinetsoft.com/api/';
    let url = baseUrl;
    url += version;
    return url;
  };

  urlFetch = (url, options) => {
    return fetch(url, options)
      .then(res => res.json())
      .then(res => {
        if (res.status === 'exception') {
          Alert.alert('Error', 'Error inesperado', [{ text: 'Cerrar' }]);
          return Promise.resolve(res);
        }
        if (res.status === 'error') {
          return Promise.resolve(res);
        } else {
          if (res.data) {
            return Promise.resolve(res.data);
          } else {
            return res;
          }
        }
      })
      .catch(function (error) {
        let errorTittle = 'Error';
        if (error.message == 'Network request failed') {
          error.message =
            'No se ha podido conectar con el servidor, revisa tu conexión a internet';
          errorTittle = 'Error de conexión';
        }
        Alert.alert(errorTittle, error.message, [{ text: 'Cerrar' }]);
        return { data: '', status: 'fail', error: error.message };
      });
  };

  setFormBody = params => {
    var formBody = [];
    for (var property in params) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(params[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    formBody = formBody.join('&');
    return formBody;
  };

  loginUser = params => {
    let url = mecanico.getQueryURL(this.version);
    let formBody = this.setFormBody(params);
    let options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: formBody,
    };
    url += '/login';
    return this.urlFetch(url, options);
  };

  createNovedades = params => {
    let url = mecanico.getQueryURL(this.version);
    let options = {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(params),
    };
    url += '/novedadesnew';
    return this.urlFetch(url, options);
  };
  recotizar = params => {
    let url = mecanico.getQueryURL(this.version);
    let options = {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(params),
    };
    url += '/recotizar';
    return this.urlFetch(url, options);
  };

  createLocal = params => {
    let url = mecanico.getQueryURL(this.version);
    let options = {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(params),
    };
    url += '/createLocal';
    return this.urlFetch(url, options);
  };

  createRespuesta = params => {
    let url = mecanico.getQueryURL(this.version);
    let options = {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(params),
    };
    url += '/respuestarepuesto';
    return this.urlFetch(url, options);
  };



  createMecanico = params => {
    let url = mecanico.getQueryURL(this.version);
    let options = {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(params),
    };
    url += '/createMecanico';
    return this.urlFetch(url, options);
  };

  respuestareserva = params => {
    let url = mecanico.getQueryURL(this.version);
    let options = {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(params),
    };
    url += '/respuestareserva';
    return this.urlFetch(url, options);
  };

  respuestaCliente = params => {
    let url = mecanico.getQueryURL(this.version);
    let options = {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(params),
    };
    url += '/respuestacliente';
    return this.urlFetch(url, options);
  };


 
  respuestaLocal = params => {
    let url = mecanico.getQueryURL(this.version);
    let options = {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(params),
    };
    url += '/respuestalocal';
    return this.urlFetch(url, options);
  };


  updatemecanico = params => {
    let url = mecanico.getQueryURL(this.version);
    let options = {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(params),
    };
    url += '/updatemecanico';
    return this.urlFetch(url, options);
  };


  getMyCites = idLocal => {
    let url = mecanico.getQueryURL(this.version);
    let options = {};
    url += '/miscitas/' + idLocal;
    return this.urlFetch(url, options);
  };

  getcitasproveedor = idLocal => {
    let url = mecanico.getQueryURL(this.version);
    let options = {};
    url += '/getcitasproveedor/' + idLocal;
    return this.urlFetch(url, options);
  };

  getReservasLocal = (idLocal,localtype) => {
    let url = mecanico.getQueryURL(this.version);
    let options = {};
    url += '/getreservaslocal/' + idLocal+'/'+localtype;
    return this.urlFetch(url, options);
  };
  getRecervaproveedorMotos = idLocal => {
    let url = mecanico.getQueryURL(this.version);
    let options = {};
    url += '/getRecervaproveedorMotos/' + idLocal;

    return this.urlFetch(url, options);
  };

  getReservasLocalMantenimiento = idLocal => {
    let url = mecanico.getQueryURL(this.version);
    let options = {};
    url += '/getreservasMantenimiento/' + idLocal;
   return this.urlFetch(url, options);
  };
  updatetoken = (idlocal, tocken) => {
    let url = mecanico.getQueryURL(this.version);
    let options = {};
    url += '/updatetoken/' + idlocal + '/' + tocken
    return this.urlFetch(url, options);
  };

  getProvincias = () => {
    let url = mecanico.getQueryURL(this.version);
    let options = {};
    url += '/provincias';
    return this.urlFetch(url, options);
  };
  getMisNovedades = idLocal => {
    let url = mecanico.getQueryURL(this.version);
    let options = {};
    url += '/getnovedades/' + idLocal;
    return this.urlFetch(url, options);
  };

  getLocal = idLocal => {
    let url = mecanico.getQueryURL(this.version);
    let options = {};
    url += '/getLocalById/' + idLocal;
    return this.urlFetch(url, options);
  };

  getTipoNovedades = () => {
    let url = mecanico.getQueryURL(this.version);
    let options = {};
    url += '/tiponovedades';
    return this.urlFetch(url, options);
  };

  getCiudades = provinciaId => {
    let url = mecanico.getQueryURL(this.version);
    let options = {};
    url += '/ciudades/' + provinciaId;
    return this.urlFetch(url, options);
  };
  GetMecanicos = idLocal => {
    let url = mecanico.getQueryURL(this.version);
    let options = {};
    url += '/mecanicos/' + idLocal;
    return this.urlFetch(url, options);
  };

  getTiposLocal = () => {
    let url = mecanico.getQueryURL(this.version);
    let options = {};
    url += '/tipolocales'
    return this.urlFetch(url, options);
  };


  GetRepuestos = (lat, long, local) => {
    let url = mecanico.getQueryURL(this.version);
    let options = {};
    url += '/getrepuestosall/' + lat + '/' + long + '/' + local
    return this.urlFetch(url, options);
  };
  contadorgeneral = (local,type) => {
    let url = mecanico.getQueryURL(this.version);
    let options = {};
    url += '/contadorgeneral/' + local+'/'+type
    return this.urlFetch(url, options);
  };

  finalizarrepuesto = (idReservaRepuesto) => {
    let url = mecanico.getQueryURL(this.version);
    let options = {};
    url += '/finalizarrepuesto/' + idReservaRepuesto
    return this.urlFetch(url, options);
  };



}

