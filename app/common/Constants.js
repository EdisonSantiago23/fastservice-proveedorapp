export default {
  STORE_ID_USER: 'idUser',
  TIPO: 'tipo',
  STORE_IMG_USER: 'imgUser',
  STORE_ID_TOKEN: 'userId',
  USER_TOKEN: 'token',
  GUEST_TOKEN: 'invitado',
  IS_FIRST_TIME_KEY: 'isFirst',
  BASEURI: 'http://mecanico.balinetsoft.com',
  colorGray: '#aaa',
  colorBrownBlack: '#502917',
  colorBrown: '#9C5C3E',
  colorPink: '#C78D82',
  colorBrownLigth: '#C78D82',
  colorBlue: '#D5D8DD',
};

