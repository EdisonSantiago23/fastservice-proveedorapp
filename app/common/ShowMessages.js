import {showMessage} from 'react-native-flash-message';
export function showDangerMsgWarPayph(message, title) {
  showMessage({
    duration: 5000,
    message: title ? title : '',
    description: message,
    backgroundColor: '#FBDC00',
  });
}
export function showSuccesMsg(message, title) {
  showMessage({
    duration: 5000,
    message: title ? title : '',
    description: message,
    backgroundColor:  '#092147',
  });
}
export function showDangerMsg(message, title) {
  showMessage({
    duration: 5000,
    message: title ? title : '',
    description: message,
    backgroundColor: '#ff3345',
  });
}
export function showDangerMsgWar(message, title) {
  showMessage({
    duration: 5000,
    message: title ? title : '',
    description: message,
    backgroundColor: '#FBDC00',
  });

}
