import React, { Component } from 'react';
import {
  Animated, View, Dimensions
} from 'react-native';
import Menu from './app/screens/Menu/Menu';
import Novedadesmes from './app/screens/novedadesmes/Novedadesmes';
import RegistroDireccionMapa from './app/screens/Registro/RegistroDireccionMapa';
import RegistroFormularioMapa from './app/screens/Registro/RegistroFormularioMapa';
import Terminos from './app/screens/Registro/Terminos';
import MenuMecanico from './app/screens/Mecanico/MenuMecanico';
import RegistroMecanico from './app/screens/Mecanico/RegistroMecanico';
import ActualizarMecanico from './app/screens/Mecanico/ActualizarMecanico';



import MisNovedades from './app/screens/novedadesmes/MisNovedades';

import MenuNotificaciones from './app/screens/Notificaciones/MenuNotificaciones';



//emergencias
import NotificacionesEmergencias from './app/screens/Notificaciones/Emergencia/NotificacionesEmergencias';
import DetalleEmergencia from './app/screens/Notificaciones/Emergencia/DetalleEmergencia';



//repuestos
import DetalleRepuesto from './app/screens/Notificaciones/Repuesto/DetalleRepuesto';
import NotificacionesRepuestos from './app/screens/Notificaciones/Repuesto/NotificacionesRepuestos';
import Cotizar from './app/screens/Notificaciones/Repuesto/Cotizar';
import DetalleCotizar from './app/screens/Notificaciones/Repuesto/DetalleCotizar';


//mantenimiento
import NotificacionesMantenimiento from './app/screens/Notificaciones/Mantenimiento/NotificacionesMantenimiento';
import DetalleMantenimiento from './app/screens/Notificaciones/Mantenimiento/DetalleMantenimiento';




//servicios
import NotificacionesMotoNew from './app/screens/Notificaciones/Servicio/NotificacionesMotoNew';


import MenuCitas from './app/screens/Citas/MenuCitas';
import Citas from './app/screens/Citas/Citas';
import CitasPendientes from './app/screens/Citas/CitasPendientes';

import Informacion from './app/screens/Informacion/Informacion';




import MenuContabilidad from './app/screens/Contabilidad/MenuContabilidad';
import ContabilidadEmergencias from './app/screens/Contabilidad/ContabilidadEmergencias';
import ContabilidadMantenimiento from './app/screens/Contabilidad/ContabilidadMantenimiento';
import ContabilidadMotoNew from './app/screens/Contabilidad/ContabilidadMotoNew';
import ContabilidadRepuestos from './app/screens/Contabilidad/ContabilidadRepuestos';




import FlashMessage from 'react-native-flash-message';




import Login from './app/screens/Registro/Login';

import { NavigationContainer } from '@react-navigation/native';

import { createStackNavigator } from '@react-navigation/stack';



class ImageLoader extends Component {

  state = {
    opacity: new Animated.Value(0),
  }
  onLoad = () => {
    Animated.timing(this.state.opacity, {
      toValue: 1,
      duration: 4000,
      useNativeDriver: true,

    }).start();
  }
  render() {
    return (
      <Animated.Image
        onLoad={this.onLoad}
        {...this.props}
        style={[
          {
            opacity: this.state.opacity,
            transform: [
              {
                scale: this.state.opacity.interpolate({
                  inputRange: [0, 1],
                  outputRange: [0.85, 1],
                })
              }
            ]
          },
          this.props.style,
        ]}
      />
    )
  }
}

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      initSplash: true,
      loginSpl: true,
      userToken: false,

    };

    setTimeout(
      () =>
        this.setState({ initSplash: false, }),
      4000
    );

  };



  render() {
    if (this.state.initSplash) {
      return (
        <View style={{
          flex: 1,
          justifyContent: 'center',
          paddingBottom: 10,
          alignItems: 'center'
        }}>
          <ImageLoader
            style={{
              height: '90%', width: '60%', resizeMode: 'contain'
            }}
            source={require('./app/assets/png/lg2.png')}
            resizeMode="contain"
            resizeMethod="resize"
          />
        </View>
      );
    }
    return <AppContainer />;

  }
}
export default App;
const Stack = createStackNavigator();

const AppContainer =  () => {
  return (
    <NavigationContainer>
        <>
          <Stack.Navigator>
            <Stack.Screen name="Login" component={Login} options={{headerLeft: null, headerShown: false,  }} />
            <Stack.Screen name="Menu" component={Menu} options={{
              title: "Bienvenido", headerLeft: null, headerShown: false,
            }} />

            <Stack.Screen name="Novedadesmes" component={Novedadesmes} options={{headerLeft: null, headerShown: false,}} />
            <Stack.Screen name="MenuMecanico" component={MenuMecanico} options={{ headerLeft: null, headerShown: false,}} />
            <Stack.Screen name="RegistroMecanico" component={RegistroMecanico} options={{ headerLeft: null, headerShown: false, }} />
            <Stack.Screen name="MisNovedades" component={MisNovedades} options={{headerLeft: null, headerShown: false,  }} />
            <Stack.Screen name="MenuNotificaciones" component={MenuNotificaciones} options={{ headerLeft: null, headerShown: false, }} />
            <Stack.Screen name="NotificacionesEmergencias" component={NotificacionesEmergencias} options={{ headerLeft: null, headerShown: false, }} />
            <Stack.Screen name="NotificacionesMantenimiento" component={NotificacionesMantenimiento} options={{ headerLeft: null, headerShown: false, }} />
            <Stack.Screen name="NotificacionesMotoNew" component={NotificacionesMotoNew} options={{ headerLeft: null, headerShown: false, }} />

            <Stack.Screen name="DetalleRepuesto" component={DetalleRepuesto} options={{ headerLeft: null, headerShown: false, }} />
            <Stack.Screen name="ActualizarMecanico" component={ActualizarMecanico} options={{ headerLeft: null, headerShown: false, }} />
            <Stack.Screen name="DetalleEmergencia" component={DetalleEmergencia} options={{ headerLeft: null, headerShown: false, }} />
            <Stack.Screen name="DetalleMantenimiento" component={DetalleMantenimiento} options={{ headerLeft: null, headerShown: false, }} />

            
            
            <Stack.Screen name="MenuCitas" component={MenuCitas} options={{ headerLeft: null, headerShown: false, }} />
            <Stack.Screen name="Citas" component={Citas} options={{ headerLeft: null, headerShown: false, }} />
            <Stack.Screen name="CitasPendientes" component={CitasPendientes} options={{ headerLeft: null, headerShown: false, }} />

            

            <Stack.Screen name="MenuContabilidad" component={MenuContabilidad} options={{  headerLeft: null, headerShown: false, }} />
            <Stack.Screen name="ContabilidadEmergencias" component={ContabilidadEmergencias} options={{ headerLeft: null, headerShown: false, }} />
            <Stack.Screen name="ContabilidadMantenimiento" component={ContabilidadMantenimiento} options={{  headerLeft: null, headerShown: false, }} />
            <Stack.Screen name="ContabilidadMotoNew" component={ContabilidadMotoNew} options={{  headerLeft: null, headerShown: false, }} />
            <Stack.Screen name="ContabilidadRepuestos" component={ContabilidadRepuestos} options={{  headerLeft: null, headerShown: false, }} />

            <Stack.Screen name="NotificacionesRepuestos" component={NotificacionesRepuestos} options={{ headerLeft: null, headerShown: false, }} />
            <Stack.Screen name="Cotizar" component={Cotizar} options={{ headerLeft: null, headerShown: false,  }} />
            <Stack.Screen name="DetalleCotizar" component={DetalleCotizar} options={{ headerLeft: null, headerShown: false, }} />
            <Stack.Screen name="Informacion" component={Informacion} options={{ headerLeft: null, headerShown: false, }} />

            
            <Stack.Screen name="RegistroDireccionMapa" component={RegistroDireccionMapa} options={{headerLeft: null, headerShown: false, }} />
            <Stack.Screen name="RegistroFormularioMapa" component={RegistroFormularioMapa} options={{ headerLeft: null, headerShown: false, }} />
            <Stack.Screen name="Terminos" component={Terminos} options={{  headerLeft: null, headerShown: false, }} />
          </Stack.Navigator>
        </>
        <FlashMessage position={'top'} />

    </NavigationContainer>
  );
};
